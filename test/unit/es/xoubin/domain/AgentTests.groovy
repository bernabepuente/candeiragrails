package es.xoubin.domain



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Agent)
class AgentTests {

    void testSomething() {
        fail "Implement me"
    }
}
