package es.xoubin.service

import grails.util.GrailsNameUtils

import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils

class UserService {
    /**
     *
     * @param user
     */
    protected void addRoles(user, roleClazz, userRoleClazz, params) {
        String upperAuthorityFieldName = GrailsNameUtils.getClassName(
                SpringSecurityUtils.securityConfig.authority.nameField, null)

        for (String key in params.keySet()) {
            if (key.contains('ROLE') && 'on' == params.get(key)) {
                userRoleClazz.create user, roleClazz."findBy$upperAuthorityFieldName"(key), true
            }
        }
    }

	/**
	 * 
	 * @param user
	 * @return
	 */
	protected Map buildUserModel(user, roleClazz) {

		String authorityFieldName = SpringSecurityUtils.securityConfig.authority.nameField
		String authoritiesPropertyName = SpringSecurityUtils.securityConfig.userLookup.authoritiesPropertyName

		List roles = sortedRoles(roleClazz)
		Set userRoleNames = user[authoritiesPropertyName].collect { it[authorityFieldName] }
		def granted = [:]
		def notGranted = [:]
		for (role in roles) {
			String authority = role[authorityFieldName]
			if (userRoleNames.contains(authority)) {
				granted[(role)] = userRoleNames.contains(authority)
			}
			else {
				notGranted[(role)] = userRoleNames.contains(authority)
			}
		}

		return [user: user, roleMap: granted + notGranted]
	}

	/**
	 * 
	 * @return
	 */
	protected List sortedRoles(roleClazz) {
		roleClazz.list().sort { it.authority }
	}
}
