package es.xoubin.service

import java.util.Map;

import es.xoubin.domain.Agent
import es.xoubin.domain.Customer
import es.xoubin.domain.Property
import es.xoubin.domain.Search;
import es.xoubin.domain.Tipo_Inmueble
import es.xoubin.exception.CustomerException

import org.springframework.context.i18n.LocaleContextHolder

import grails.plugins.springsecurity.SpringSecurityService
import groovy.sql.Sql


class CustomerService {

    def dataSource
	def messageSource
    def springSecurityService

    Map customerList (Map params, Integer max, Integer offset) {
		def term = params.term
		def customerInstanceList
		def total
		def userId =springSecurityService.currentUser.id
		def ref = '%' + params.nameBusqueda + '%'
		def name = '%' + params.nameBusqueda + '%'
		def tel = '%' + params.nameBusqueda + '%'
		
        if (!params.nameBusqueda) {
            customerInstanceList=Customer.executeQuery("select distinct c from Customer as c inner join c.customerPersons as p where c.agent.id = ?", [userId], [sort: params.sort, order: params.order, max: max, offset: offset])
			total=Customer.executeQuery("select distinct c from Customer as c inner join c.customerPersons as p where c.agent.id = ?", [userId], [sort: params.sort, order: params.order]).size()
        } else {
			customerInstanceList=Customer.executeQuery("select distinct c from Customer as c inner join c.customerPersons as p where p.name like ? or (p.name || ' ' || p.surname) like ? or c.referencia like ? or p.movil like ? or p.particular like ? or p.trabajo like ?", [name, name, ref, tel, tel, tel], [sort: params.sort, order: params.order, max: max, offset: offset])
			total=Customer.executeQuery("select distinct c from Customer as c inner join c.customerPersons as p where p.name like ? or (p.name || ' ' || p.surname) like ? or c.referencia like ? or p.movil like ? or p.particular like ? or p.trabajo like ?", [name, name, ref, tel, tel, tel], [sort: params.sort, order: params.order]).size()
        }
		['customerInstanceList':customerInstanceList, 'total': total]
    }
	
	Map searchFromProperty (Property property, Map params, Integer max, Integer offset) {
		def term = params.term
		def customerInstanceList
		def total
		Tipo_Inmueble tipo = property.tipo
		def operacion
		if (property.operacion){
			operacion = property.operacion
		}
		def parcelaMin
		if (property.nSuperficie_terreno){
			parcelaMin = property.nSuperficie_terreno
		}
		def parcelaMax
		if (property.nSuperficie_terreno){
			parcelaMax = property.nSuperficie_terreno
		}
		def construidaMin
		if (property.nSuperficie_construida){
			construidaMin = property.nSuperficie_construida
		}
		def construidaMax
		if (property.nSuperficie_construida){
			construidaMax = property.nSuperficie_construida
		}
		def dormitorios
		if (property.nDormitorios){
			dormitorios = property.nDormitorios
		}
		def banos
		if (property.nBanos){
			banos = property.nBanos
		}
		def garaje
		if (property.nGarajes){
			garaje = property.nGarajes
		}
		def amueblado
		if (property.amueblado){
			amueblado = property.amueblado
		}
		def precio
		if (property.precio_deseado){
			precio = property.precio_deseado.replace('.', '')
			precio = precio.replace(' ', '')
			precio = precio.replace('€', '')
		}
		def parametros=[]
		def sql="select c from Customer as c inner join c.searches as searches where"
		if (tipo){
			sql= sql + " searches.tipoInmueble = ?"
			parametros.add(tipo)
		}
		if (operacion){
			sql= sql + " and searches.operacion = ?"
			parametros.add(operacion)
		}
		if (parcelaMin){
			sql= sql + " and cast(searches.sup_parcela_min as integer) <= ?"
			parametros.add(parcelaMin)
		}
		if (parcelaMax){
			sql= sql + " and cast(searches.sup_parcela_max as integer) >= ?"
			parametros.add(parcelaMax)
		}
		if (construidaMin){
			sql= sql + " and cast(searches.sup_construida_min as integer) <= ?"
			parametros.add(construidaMin)
		}
		if (construidaMax){
			sql= sql + " and cast(searches.sup_construida_max as integer) >= ?"
			parametros.add(construidaMax)
		}
		if (dormitorios){
			sql= sql + " and cast(searches.dormitorios as integer) <= ?"
			parametros.add(dormitorios)
		}
		if (banos){
			sql= sql + " and cast(searches.banos as integer) <= ?"
			parametros.add(banos)
		}
		if (precio){
			sql= sql + " and cast(searches.maximo as integer) <= ?"
			parametros.add(Integer.parseInt(precio))
		}
		if (garaje){
			sql= sql + " and garaje = 1"			
		}
		if (amueblado){
			sql= sql + " and amueblado = 1"
		}
		
		customerInstanceList=Customer.executeQuery(sql, parametros, [sort: params.sort, order: params.order, max: max, offset: offset])
		
		total=customerInstanceList.size()
		
		['customerInstanceList':customerInstanceList, 'total': total]		
	}

    def createCustomer(Map params){
        def customer = new Customer(params)
        //Creamos la referencia nueva
        customer.referencia = createReferencia()
        // Por defecto, le asignamos el actual user logueado
        customer.agent=Agent.findById(springSecurityService.principal.id)
        //Guardamos
        customer.save(flush: true)
        return customer
    }

    def createReferencia(){
        String ref
        def sql = new Sql(dataSource)
        def ultima = sql.firstRow("SELECT referencia from customer order by referencia desc limit 1")
        if (!ultima){
            ref = "C00001"
        }
        else{
            def num = Integer.parseInt(((ultima[0].value).toString()).substring(1)) + 1

            switch (num.toString().length()) {
                case 1:
                    ref = "C0000" + num.toString()
                    break
                case 2:
                    ref = "C000" + num.toString()
                    break
               case 3:
                   ref = "C00" + num.toString()
                   break
               case 4:
                   ref = "C0" + num.toString()
                   break
               case 5:
                   ref = "C" + num.toString()
                   break
            }
        }
        return ref
    }

    def saveCustomer(Map params){
        def customerInstance = new Customer(params)
		
		def agentInstance = Agent.findById(params.cboAgente)
		customerInstance.agent = agentInstance

        if (!customerInstance.save(flush: true)) {
            customerInstance.errors.each {
                System.out.println(it)
            }
            return null
        }
        return customerInstance
    }

    def showCustomer(Long id){
        def customer = Customer.get(id)
        return customer
    }

    def editCustomer(Long id){
        def customerInstance = Customer.get(id)
        if (!customerInstance) {
            log.error(messageSource.getMessage('customer.not.found.message', null, null, LocaleContextHolder.getLocale()))
            throw new CustomerException(msg: messageSource.getMessage('customer.not.found.message', null, null, LocaleContextHolder.getLocale()))
        }
        return customerInstance
    }

    def updateCustomer(Long id, params){
        def customerInstance = Customer.get(id)
        if (!customerInstance) {
            log.error(messageSource.getMessage('customer.not.found.message', null, null, LocaleContextHolder.getLocale()))
            throw new CustomerException(msg: messageSource.getMessage('customer.not.found.message', null, null, LocaleContextHolder.getLocale()))
        }
        customerInstance.properties = params
		
		def agentInstance = Agent.findById(params.cboAgente)
		customerInstance.agent = agentInstance

        if (!customerInstance.save()) {
            customerInstance.errors.each {
                System.out.println(it)
            }
            return null
        }
        return customerInstance
    }

    def deleteCustomer(Long id) {
        def customerInstance = Customer.get(id)
        if (!customerInstance) {
            log.error(messageSource.getMessage('customer.not.found.message', null, null, LocaleContextHolder.getLocale()))
            throw new CustomerException(msg: messageSource.getMessage('customer.not.found.message', null, null, LocaleContextHolder.getLocale()))
        }
        String nameOfDeleted = customerInstance.toString()
        customerInstance.delete(flush: true)
        return nameOfDeleted
    }
}
