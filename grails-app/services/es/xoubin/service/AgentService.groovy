package es.xoubin.service

import es.xoubin.domain.Agent
import es.xoubin.exception.AgentException

import org.springframework.context.i18n.LocaleContextHolder as LCH
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory

class AgentService {

    def messageSource

    Map agentList (Map params) {
        ['agentInstanceList': Agent.findAllByAccountLocked(false, [sort:"name", order:"asc"]), 'agentInstanceTotal': Agent.findAllByAccountLocked(false).size()]
    }

    def createAgent(Map params){
        def agent = new Agent(params)
        return agent
    }

    def saveAgent(Map params){
        def agentInstance = new Agent(params)

        if (!agentInstance.save()) {
            agentInstance.errors.each {
                System.out.println(it)
            }
            return null
        }
        return agentInstance
    }

    def showAgent(Long id){
        def agent = Agent.get(id)
        return agent
    }

    def editAgent(Long id){
        def agentInstance = Agent.get(id)
        if (!agentInstance) {
            log.error(messageSource.getMessage('agent.not.found.message', null, null, LCH.getLocale()))
            throw new AgentException(msg: messageSource.getMessage('agent.not.found.message', null, null, LCH.getLocale()))
        }
        return agentInstance
    }

    def updateAgent(Long id, params){
        def agentInstance = Agent.get(id)
        if (!agentInstance) {
            log.error(messageSource.getMessage('agent.not.found.message', null, null, LCH.getLocale()))
            throw new AgentException(msg: messageSource.getMessage('agent.not.found.message', null, null, LCH.getLocale()))
        }
        agentInstance.properties = params

        if (!agentInstance.save()) {
            agentInstance.errors.each {
                System.out.println(it)
            }
            return null
        }
        return agentInstance
    }

    def deleteAgent(Long id) {
        def agentInstance = Agent.get(id)
        if (!agentInstance) {
            log.error(messageSource.getMessage('agent.not.found.message', null, null, LCH.getLocale()))
            throw new AgentException(msg: messageSource.getMessage('agent.not.found.message', null, null, LCH.getLocale()))
        }
        String nameOfDeleted = agentInstance.toString()
        agentInstance.delete(flush: true)
        return nameOfDeleted
    }
}
