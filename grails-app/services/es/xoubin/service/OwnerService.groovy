package es.xoubin.service

import es.xoubin.domain.Owner
import es.xoubin.exception.OwnerException
import org.springframework.context.i18n.LocaleContextHolder

class OwnerService {

    Map ownerList () {
        ['ownerInstanceList': Owner.list(), 'ownerInstanceTotal': Owner.count()]
    }

    def createOwner(Map params){
        def owner = new Owner(params)
        return owner
    }

    def saveOwner(Map params){
        def ownerInstance = new Owner(params)

        if (!ownerInstance.save(flush: true)) {
            ownerInstance.errors.each {
                System.out.println(it)
            }
            return null
        }
        return ownerInstance
    }

    def showOwner(Long id){
        def owner = Owner.get(id)
        return owner
    }

    def editOwner(Long id){
        def ownerInstance = Owner.get(id)
        if (!ownerInstance) {
            log.error(messageSource.getMessage('owner.not.found.message', null, null, LocaleContextHolder.getLocale()))
            throw new OwnerException(msg: messageSource.getMessage('owner.not.found.message', null, null, LocaleContextHolder.getLocale()))
        }
        return ownerInstance
    }

    def updateOwner(Long id, params){
        def ownerInstance = Owner.get(id)
        if (!ownerInstance) {
            log.error(messageSource.getMessage('owner.not.found.message', null, null, LocaleContextHolder.getLocale()))
            throw new OwnerException(msg: messageSource.getMessage('owner.not.found.message', null, null, LocaleContextHolder.getLocale()))
        }
        ownerInstance.properties = params

        if (!ownerInstance.save()) {
            ownerInstance.errors.each {
                System.out.println(it)
            }
            return null
        }
        return ownerInstance
    }

    def deleteOwner(Long id) {
        def ownerInstance = Owner.get(id)
        if (!ownerInstance) {
            log.error(messageSource.getMessage('owner.not.found.message', null, null, LocaleContextHolder.getLocale()))
            throw new OwnerException(msg: messageSource.getMessage('owner.not.found.message', null, null, LocaleContextHolder.getLocale()))
        }
        String nameOfDeleted = ownerInstance.toString()
        ownerInstance.delete(flush: true)
        return nameOfDeleted
    }
}
