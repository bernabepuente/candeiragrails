package es.xoubin.service

import es.xoubin.domain.Invoice
import es.xoubin.domain.Agent
import es.xoubin.exception.InvoiceException
import grails.plugins.springsecurity.ui.AbstractS2UiController;
import groovy.sql.Sql

import org.springframework.context.i18n.LocaleContextHolder

import java.util.Map;

class InvoiceService extends AbstractS2UiController{

    def dataSource
	def messageSource
    def springSecurityService

    Map invoiceList (Map params, Integer max, Integer offset) {
		def term = params.term
		def invoiceInstanceList
		def total
		def ref = '%' + params.nameBusqueda + '%'
		def sort=params.sort
		def user=springSecurityService.principal.id
        if (!params.nameBusqueda) {
			if ((user==1)||(user==5)||(user==6)){
                invoiceInstanceList = Invoice.executeQuery("select i from Invoice as i order by i.dateInvoice desc", [max: max, offset: offset])
			}
			else{
				invoiceInstanceList = Invoice.executeQuery("select i from Invoice as i where agent=? order by i.dateInvoice desc", [Agent.findById(user)], [max: max, offset: offset])
			}
			total=Invoice.count()
        } else {
			if ((user==1)||(user==5)||(user==6)){
				invoiceInstanceList=Invoice.executeQuery("select i from Invoice as i where i.reference like ? order by i.dateInvoice desc", [ref], [max: max, offset: offset])
			}
			else{
				invoiceInstanceList=Invoice.executeQuery("select i from Invoice as i where i.reference like ? and agent=? order by i.dateInvoice desc", [ref, Agent.findById(user)], [max: max, offset: offset])
			}
			total=invoiceInstanceList.size()
        }
		['invoiceInstanceList':invoiceInstanceList, 'total': total]
    }

    def createInvoice(Map params){
        def invoice = new Invoice(params)
        //Creamos la referencia nueva si no es libre
        //if (params.tipo!=1){
            invoice.reference = createReference()
        //}
        // Por defecto, le asignamos el actual user logueado
        invoice.agent=Agent.findById(springSecurityService.principal.id)
		invoice.dateInvoice=new Date()
        //Guardamos
        invoice.save(flush: true)
        return invoice
    }

    def createReference(){
        def referencia
        def x = 0
        for ( invoice in Invoice.list() ) {
            def ref = invoice.reference
            try {
                def refInt = Integer.parseInt(ref.split('/')[0])
                if (refInt > x) {
                    x = refInt
                }
            }
            catch (Exception) {
            }
        }
        if (x==0){
            referencia = "1/" + (new Date().year).toString().substring(1)
        }
        else{
            referencia = (x + 1).toString() + "/" + (new Date().year).toString().substring(1)
        }
        return referencia
    }

    def saveInvoice(Map params){
        def invoiceInstance = new Invoice(params)

        if (!invoiceInstance.save(flush: true)) {
            invoiceInstance.errors.each {
                System.out.println(it)
            }
            return null
        }
        return invoiceInstance
    }

    def showInvoice(Long id){
        def invoice = Invoice.get(id)
        return invoice
    }

    def editInvoice(Long id){
        def invoiceInstance = Invoice.get(id)
        if (!invoiceInstance) {
            log.error(messageSource.getMessage('invoice.not.found.message', null, null, LocaleContextHolder.getLocale()))
            throw new InvoiceException(msg: messageSource.getMessage('invoice.not.found.message', null, null, LocaleContextHolder.getLocale()))
        }
        return invoiceInstance
    }

    def updateInvoice(Long id, params){
        def invoiceInstance = Invoice.get(id)
        if (!invoiceInstance) {
            log.error(messageSource.getMessage('invoice.not.found.message', null, null, LocaleContextHolder.getLocale()))
            throw new InvoiceException(msg: messageSource.getMessage('invoice.not.found.message', null, null, LocaleContextHolder.getLocale()))
        }
        invoiceInstance.properties = params

        if (!invoiceInstance.save()) {
            invoiceInstance.errors.each {
                System.out.println(it)
            }
            return null
        }
        return invoiceInstance
    }

    def deleteInvoice(Long id) {
        def invoiceInstance = Invoice.get(id)
        if (!invoiceInstance) {
            log.error(messageSource.getMessage('invoice.not.found.message', null, null, LocaleContextHolder.getLocale()))
            throw new InvoiceException(msg: messageSource.getMessage('invoice.not.found.message', null, null, LocaleContextHolder.getLocale()))
        }
        String nameOfDeleted = invoiceInstance.toString()
        invoiceInstance.delete(flush: true)
        return nameOfDeleted
    }
}
