package es.xoubin.service

import es.xoubin.domain.Agent
import es.xoubin.domain.City
import es.xoubin.domain.Person
import es.xoubin.domain.Property
import es.xoubin.domain.Search
import es.xoubin.domain.Tipo_Inmueble
import es.xoubin.domain.Zone
import es.xoubin.exception.PropertyException
import grails.plugins.springsecurity.SpringSecurityService

import org.springframework.context.i18n.LocaleContextHolder as LCH

class PropertyService {

    def messageSource
    SpringSecurityService springSecurityService

    Map propertyList (Map params, Integer max, Integer offset) {
        def rol = springSecurityService.getPrincipal().getAuthorities()
        def userId =springSecurityService.currentUser.id
        def term = params.term
        def propertyInstanceList
        def total
        def ref = '%' + params.term + '%'
        def add = '%' + params.term + '%'
        def tel = '%' + params.term + '%'
        def estado = '%' + params.term + '%'
		
        if (!params.sort){
            params.sort="referencia"
            params.order="asc"
        }
        def query=""
		
        //rol admin ve todos los inmuebles
        if (rol.toString().contains("ROLE_ADMIN")){ 
            if (!params.term) {
                query="select p from Property as p"
                propertyInstanceList=Property.executeQuery("select p from Property as p order by " +  params.sort + " " + params.order, [], [max: max, offset: offset])
                total=Property.executeQuery("select p from Property as p order by " +  params.sort + " " + params.order, []).size()
            } else {
                query="select distinct p from Property as p left join p.owners as o where p.referencia like " + ref + " or p.address like " + add + " or o.phone1 like " + tel + " or o.phone2 like " + tel + " or p.estado like " + estado
                propertyInstanceList=Property.executeQuery("select distinct p from Property as p left join p.owners as o where p.referencia like ? or p.address like ? or o.phone1 like ? or o.phone2 like ? or p.estado like ? order by " +  params.sort + " " + params.order, [ref, add, tel, tel, estado], [max: max, offset: offset])
                total=Property.executeQuery("select distinct p from Property as p left join p.owners as o where p.referencia like ? or p.address like ? or o.phone1 like ? or o.phone2 like ?  or p.estado like ? order by " +  params.sort + " " + params.order, [ref, add, tel, tel, estado]).size()
            }
        }
        //rol ramallosa ve todos los inmuebles de los agentes con el mismo rol
        else if (rol.toString().contains("ROLE_RAMALLOSA")){
            if (!params.term) {
                propertyInstanceList = Property.executeQuery("select p from Property as p where p.agent.id in (2,3,4)  order by " +  params.sort + " " + params.order, [], [max: max, offset: offset])
                total=Property.executeQuery("select p from Property as p where p.agent.id in (2,3,4)  order by " +  params.sort + " " + params.order, []).size()
            } else {
                propertyInstanceList=Property.executeQuery("select distinct p from Property as p left join p.owners as o where (p.referencia like ? or p.address like ?) and p.agent.id in (2,3,4) or o.phone1 like ? or o.phone2 like ?  or p.estado like ? order by " +  params.sort + " " + params.order, [ref, add, tel, tel, estado], [max: max, offset: offset])
                total=Property.executeQuery("select distinct p from Property as p left join p.owners as o where (p.referencia like ? or p.address like ?) and p.agent.id in (2,3,4) or o.phone1 like ? or o.phone2 like ? or p.estado like ? order by " +  params.sort + " " + params.order, [ref, add, tel, tel, estado]).size()
            }
        }
        //rol user sólo ve sus inmuebles.
        else{
            if (!params.term) {
                propertyInstanceList = Property.executeQuery("select p from Property as p where p.agent.id = ?  order by " +  params.sort + " " + params.order, [userId], [max: max, offset: offset])
                total=Property.executeQuery("select p from Property as p where p.agent.id = ?  order by " +  params.sort + " " + params.order, [userId]).size()
            }
            else {
                def lon=params.term.length()
                def num=org.apache.commons.lang.StringUtils.isNumeric(params.term)
                if ((lon==9)&&(num==true)){
                    query="select distinct p from Property as p left join p.owners as o where (p.referencia like " + ref + " or p.address like " + add + ") or o.phone1 like " + tel + " or o.phone2 like " + tel + "  or p.estado like " + estado
                    propertyInstanceList=Property.executeQuery("select distinct p from Property as p left join p.owners as o where (p.referencia like ? or p.address like ?) or o.phone1 like ? or o.phone2 like ?  or p.estado like ? order by " +  params.sort + " " + params.order, [ref, add, tel, tel, estado], [max: max, offset: offset])
                    total=Property.executeQuery("select distinct p from Property as p left join p.owners as o where (p.referencia like ? or p.address like ?) or o.phone1 like ? or o.phone2 like ?  or p.estado like ? order by " +  params.sort + " " + params.order, [ref, add, tel, tel, estado]).size()
                }
                else{
                    query="select distinct p from Property as p left join p.owners as o where (p.referencia like " + ref + " or p.address like " + add + " or o.phone1 like " + tel + " or o.phone2 like " + tel + "  or p.estado like " + estado + ") and p.agent.id = ?"
                    propertyInstanceList=Property.executeQuery("select distinct p from Property as p left join p.owners as o where (p.referencia like ? or p.address like ? or o.phone1 like ? or o.phone2 like ? or p.estado like ?) and p.agent.id = ? order by " +  params.sort + " " + params.order, [ref, add, tel, tel, estado, userId], [max: max, offset: offset])
                    total=Property.executeQuery("select distinct p from Property as p left join p.owners as o where (p.referencia like ? or p.address like ?  or o.phone1 like ? or o.phone2 like ? or p.estado like ?) and p.agent.id = ? order by " +  params.sort + " " + params.order, [ref, add, tel, tel, estado, userId]).size()
                }
            }
        }
        if ((propertyInstanceList.size()==0)&&(params.term))
        {
            File file = new File("query.txt" )
            file.append(new Date().toString() + " /// " + query + '\n')
        }
        ['propertyInstanceList':propertyInstanceList, 'total': total]
    }
	
    Map searchFromCustomer (Search search, Map params, Integer max, Integer offset) {
        def term = params.term
        def propertyInstanceList
        def total
        Tipo_Inmueble tipo = search.tipoInmueble
        City city
        if (search.city){
            city = search.city
        }
        Zone zone
        if (search.zone){
            zone = search.zone
        }
        def operacion
        if (search.operacion){
            operacion = search.operacion
        }
        def parcelaMin 
        if (search.sup_parcela_min){
            parcelaMin = search.sup_parcela_min.toInteger()
        }
        def parcelaMax
        if (search.sup_parcela_max){
            parcelaMax = search.sup_parcela_max.toInteger()
        }
        def construidaMin
        if (search.sup_construida_min){
            construidaMin = search.sup_construida_min.toInteger()
        }
        def construidaMax
        if (search.sup_construida_max){
            construidaMax = search.sup_construida_max.toInteger()
        }
        def amueblado
        if (search.amueblado==true){
            amueblado = 1
        }
        else{
            amueblado = 0
        }
        def garaje
        if (search.garaje){
            garaje = search.garaje
        }
        def dorm
        if (search.dormitorios){
            dorm = search.dormitorios.toInteger()
        }
        Integer banos
        if (search.banos){
            banos = search.banos.toInteger()
        }
        def precio
        if (search.maximo){
            precio = search.maximo.replace('.', '')
            precio = precio.replace(' ', '')
            precio = precio.replace('€', '')
        }
        def parametros=[]
        def sql="select p from Property as p where p.alquilado=false and p.anulado=false and (estado='POSITIVO' or estado='PENDIENTE') "
        if (tipo){
            sql= sql + " and p.tipo = ?"
            parametros.add(tipo)
        }
        if (city){
            sql= sql + " and p.city = ?"
            parametros.add(city)
        }
        if (zone){
            sql= sql + " and p.zone = ?"
            parametros.add(zone)
        }
        if (operacion){
            sql= sql + " and p.operacion = ?"
            parametros.add(operacion)
        }
        if (parcelaMin){
            sql= sql + " and p.nSuperficie_terreno >= ?"
            parametros.add(parcelaMin)
        }
        if (parcelaMax){
            sql= sql + " and p.nSuperficie_terreno <= ?"
            parametros.add(parcelaMax)
        }
        if (construidaMin){
            sql= sql + " and p.nSuperficie_construida >= ?"
            parametros.add(construidaMin)
        }
        if (construidaMax){
            sql= sql + " and p.nSuperficie_construida <= ?"
            parametros.add(construidaMax)
        }
        if (amueblado==1){
            sql= sql + " and p.amueblado = 1"
        }
        else{
            sql= sql + " and p.amueblado = 0"
        }
        if (garaje){
            sql= sql + " and p.nGarajes > 0"
        }
        else{
            sql= sql + " and p.nGarajes = null"
        }
        if (dorm){
            sql= sql + " and p.nDormitorios >= ?"
            parametros.add(dorm)
        }
        if (banos){
            sql= sql + " and p.nBanos >= ?"
            parametros.add(banos)
        }
        if (precio){
            sql= sql + " and p.nPrecio <= ?"
            parametros.add(precio.toInteger())
        }
		
        propertyInstanceList=Property.executeQuery(sql, parametros, [sort: params.sort, order: params.order, max: max, offset: offset])
        total=propertyInstanceList.size()
		
        ['propertyInstanceList':propertyInstanceList, 'total': total]
    }

    Map ListByAgent (Person a) {
        def propertyBusqueda = Property.withCriteria {
            eq('agent', a)
        }
        ['propertyInstanceList':propertyBusqueda, 'propertyInstanceTotal': propertyBusqueda.size()]
    }

    def createProperty(Map params){
        def property = new Property(params)
        if (params.cboAgent){
            property.agent=Agent.findById(params.cboAgent)
        }
        return property
    }

    def saveProperty(Map params){
        def propertyInstance = new Property(params)
        if (params.precio_deseado){
            def precio = params.precio_deseado.replace('.', '')
            precio = precio.replace(' ', '')
            precio = precio.replace('€', '')
            propertyInstance.nPrecio = precio.toInteger()
        }
        def agentInstance = Agent.findById(params.cboAgente)
        propertyInstance.agent = agentInstance
        def tipoInstance = Tipo_Inmueble.findById(params.cboTipo)
        propertyInstance.tipo = tipoInstance

        def cityInstance = City.findById(params.cboCity)
        propertyInstance.city = cityInstance

        def zoneInstance = Zone.findById(params.cboZone)
        propertyInstance.zone =zoneInstance

        if (!propertyInstance.save(flush: true)) {
            propertyInstance.errors.each {
                System.out.println(it)
            }
            return null
        }
        return propertyInstance
    }

    def showProperty(Long id){
        def property = Property.get(id)
        return property
    }

    def editProperty(Long id){
        def propertyInstance = Property.get(id)
        if (!propertyInstance) {
            log.error(messageSource.getMessage('property.not.found.message', null, null, LCH.getLocale()))
            throw new PropertyException(msg: messageSource.getMessage('property.not.found.message', null, null, LCH.getLocale()))
        }
        return propertyInstance
    }

    def updateProperty(Long id, params){
        def propertyInstance = Property.get(id)
        if (!propertyInstance) {
            log.error(messageSource.getMessage('property.not.found.message', null, null, LCH.getLocale()))
            throw new PropertyException(msg: messageSource.getMessage('property.not.found.message', null, null, LCH.getLocale()))
        }

        propertyInstance.properties = params

        if (params.precio_deseado){
            def precio = params.precio_deseado.replace('.', '')
            precio = precio.replace(' ', '')
            precio = precio.replace('€', '')
            propertyInstance.nPrecio = precio.toInteger()
        }
        else{
            propertyInstance.nPrecio = 0
        }

        def agentInstance = Agent.findById(params.cboAgente)
        propertyInstance.agent = agentInstance

        def tipoInstance = Tipo_Inmueble.findById(params.cboTipo)
        propertyInstance.tipo = tipoInstance

        def cityInstance = City.findById(params.cboCity)
        propertyInstance.city = cityInstance

        def zoneInstance = Zone.findById(params.cboZone)
        propertyInstance.zone =zoneInstance

        if (!propertyInstance.save()) {
            propertyInstance.errors.each {
                System.out.println(it)
            }
            return null
        }
        return propertyInstance
    }

    def deleteProperty(Long id) {
        def propertyInstance = Property.get(id)
        if (!propertyInstance) {
            log.error(messageSource.getMessage('property.not.found.message', null, null, LCH.getLocale()))
            throw new PropertyException(msg: messageSource.getMessage('property.not.found.message', null, null, LCH.getLocale()))
        }
        String nameOfDeleted = propertyInstance.toString()
        propertyInstance.delete(flush: true)
        return nameOfDeleted
    }

    def countImages(Long id){
        def count=0
        def propertyInstance = Property.get(id)
        for ( i in propertyInstance.images ) {
            if ((i.name.contains('.jpg'))||(i.name.contains('.jpeg'))||(i.name.contains('.JPG'))||(i.name.contains('.JPEG'))){
                count=count+1
            }
        }
        return count
    }

    def countFiles(Long id){
        def count=0
        def propertyInstance = Property.get(id)
        for ( i in propertyInstance.images ) {
            if ((i.name.contains('.pdf'))||(i.name.contains('.doc'))||(i.name.contains('.docx'))||(i.name.contains('.xls'))||(i.name.contains('.odt'))){
                count=count+1
            }
        }
        return count
    }
}
