package es.xoubin.service

import java.util.Map;
import es.xoubin.domain.Price
import es.xoubin.domain.Property;

class PriceService {

    Map ListByProperty (Property i) {
        def priceBusqueda = Price.withCriteria {
            eq('property', i)
			order("id", "asc")
        }
        ['priceInstanceList':priceBusqueda, 'priceInstanceTotal': priceBusqueda.size()]
    }
}
