package es.xoubin.service

import java.util.Map;

import es.xoubin.domain.Property;
import es.xoubin.domain.CustomerPerson

class CustomerPersonService {

    Map ListByProperty (Property i) {
        def busqueda = CustomerPerson.withCriteria {
            eq('property', i)
			order("id", "asc")
        }
        ['customerPersonInstanceList':busqueda, 'customerPersonInstanceTotal': busqueda.size()]
    }
}
