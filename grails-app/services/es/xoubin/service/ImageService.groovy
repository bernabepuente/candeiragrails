package es.xoubin.service

import es.xoubin.domain.Property
import es.xoubin.domain.Image
import es.xoubin.exception.ImageException
import org.apache.commons.lang.RandomStringUtils
import org.springframework.web.multipart.commons.CommonsMultipartFile

class ImageService {

    Map imageList (Map params) {
        if (!params.nameBusqueda) {
            ['imageInstanceList': Image.list(params), 'imageInstanceTotal': Image.count()]
        } else {
            def imageBusqueda = Image.withCriteria {
                like('name', '%' + params.nameBusqueda +  '%')
            }
            ['imageInstanceList':imageBusqueda, 'imageInstanceTotal': Image.count()]
        }
    }

    Map ListByProperty (Property i) {
        def imageBusqueda = Image.withCriteria {
            eq('property', i)
			order("id", "asc")
        }
        ['imageInstanceList':imageBusqueda, 'imageInstanceTotal': imageBusqueda.size()]
    }

    def createImage(Map params){
        def image
        try {
            image = new Image(params)
        }
        catch (Exception e) {
            params.name=params.name + RandomStringUtils.random(32, true, true)
            image = new Image(params)
        }
        return image
    }

    /**
     * Elimina una imagen de una entidad dada mediante el discriminador.
     * @param params
     * @return
     */
    def removeImageFromEntity(params) {
        def imageInstance
        def property
        property = Property.get(params.idEntity)
        imageInstance = entity.imagenes.find { it.id == new Long (params.id) }
        entity.removeFromImagenes(imageInstance)
    }

    def updateImage(Long id, Map params, CommonsMultipartFile f) {
        def imageInstance = Image.get(id)

        //no se por qué, pero el campo imageBlob se pierde al hacer imageInstance.properties = params
        //asi que lo guardo antes y mas adelante lo vuelvo a asignar a imageInstance si no se modifica la imagen
        byte[] blob=imageInstance.imageBlob

        imageInstance.properties = params
        if(!f.empty) {
            if (imageInstance.imageBlob != f.bytes) {
                //save the blob
                imageInstance.imageBlob = f.bytes
            }
        }
        else{
            //Si no modificamos la imagen, le volvemos a asignar el campo imageBlob que tenia.
            imageInstance.imageBlob = blob
        }
        if (!imageInstance.save(flush: true)) {
            return null
        }
        return imageInstance
    }

    def showImage(Long id){
        def image = Image.get(id)
        return image
    }

    def editImage(Long id){
        def imageInstance = Image.get(id)
        if (!imageInstance) {
            log.error(messageSource.getMessage('image.not.found.message', null, null, LCH.getLocale()))
            throw new ImageException(msg: messageSource.getMessage('image.not.found.message', null, null, LCH.getLocale()))

        }
        return imageInstance
    }

    def deleteImage(Long id) {
        def imageInstance = Image.get(id)
        //imageInstance.lock()
        if (!imageInstance) {
            log.error(messageSource.getMessage('image.not.found.message', null, null, LCH.getLocale()))
            throw new ImageException(msg: messageSource.getMessage('image.not.found.message', null, null, LCH.getLocale()))
        }
        String name = imageInstance.toString()
        imageInstance.delete(flush: true)
        return name
    }
}
