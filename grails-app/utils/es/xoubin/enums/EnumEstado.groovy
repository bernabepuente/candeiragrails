package es.xoubin.enums

enum EnumEstado {
	PENDIENTE('PT'), POSITIVO('P'), NEGATIVO('NR'), CERRADO('C'), AGENCIA('A')
	
	private final String code
	
	EnumEstado(String code) {
		this.code = code
	}
	
	public String code() {
		return code
	}
}