package es.xoubin.enums

enum EnumOperacion{
	ALQUILER('A'), VENTA('V'), ALQUILER_VENTA('ALQUILER/VENTA'), ALQUILER_COMPRA('ALQUILER/COMPRA')
	
	private final String code
	
	EnumOperacion(String code) {
		this.code = code
	}
	
	public String code() {
		return code
	}
}