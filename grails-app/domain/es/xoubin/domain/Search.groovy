package es.xoubin.domain

class Search {

    String sup_parcela_min
	String sup_parcela_max
    String sup_construida_min
	String sup_construida_max
    String dormitorios
    String banos
	Boolean amueblado
	Boolean garaje
    String limite_sur
    String limite_norte
    String limite_interior
    Boolean vistas_mar
    Boolean llave
    String maximo
	Tipo_Inmueble tipoInmueble
    City city
    Zone zone
	String operacion

    static belongsTo = [customer: Customer]

    static constraints = {
        sup_parcela_min blank: true, nullable: true
		sup_parcela_max blank: true, nullable: true
        sup_construida_min blank: true, nullable: true
		sup_construida_max blank: true, nullable: true
		amueblado blank: true, nullable: true
		garaje blank: true, nullable: true
        dormitorios blank: true, nullable: true
        banos blank: true, nullable: true
        limite_sur blank: true, nullable: true
        limite_norte blank: true, nullable: true
        limite_interior blank: true, nullable: true
        vistas_mar blank: true, nullable: true
        llave blank: true, nullable: true
        maximo blank: true, nullable: true
		tipoInmueble blank:true, nullable:true
        city blank:true, nullable:true
        zone blank:true, nullable:true
		operacion blank:true, nullable:true
    }
}
