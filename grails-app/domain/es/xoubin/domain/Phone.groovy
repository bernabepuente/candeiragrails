package es.xoubin.domain

class Phone {

    String name
    String number

    static belongsTo = [person: Person]

    static constraints = {
        name blank: true, nullable: true
        number blank: true, nullable: true
    }
}
