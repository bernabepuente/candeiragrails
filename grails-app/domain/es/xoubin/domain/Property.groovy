package es.xoubin.domain

class Property {

    String referencia
	String estado
	String operacion
    Boolean reservada
    Boolean vendida
	Boolean alquilado
	Boolean anulado
    Date fecha_toma
	Date fecha_pendiente
    String address
    City city
    Zone zone
    String latitud
    String longitud
    String direccion_gmaps
    String superficie_terreno
	Integer nSuperficie_terreno
    String superficie_construida
	Integer nSuperficie_construida
    String ano_construccion
    String ano_adquisicion
    String estado_conservacion
    String certf_energetico
    String arrendatarios
    String cargas
    String motivo_venta
    String plazo
    String otros_general
    String planos
	Boolean amueblado
    Boolean ascensor
    String porches
    String terrazas
    String salones
    Integer nSalones
    String cocinas
    Integer nCocinas
    String dormitorios
    Integer nDormitorios
    String aseos
    Integer nAseos
    String banos
    Integer nBanos
    String lavaderos
    String despensas
    String garajes
    Integer nGarajes
    String trasteros
    String instalaciones
    String piscinas
    String otros_instalaciones
    String electricidad
    String agua
    String saneamiento
    String calefaccion
    String agua_caliente
    String renovables
    String fachada
    String carpinteria_ext
    String carpinteria_int
    String pavimentos_exteriores
    String pavimentos_interiores
    String cocina
    String cuartos_bano
    String cerramiento
    String otros_calidad
    String vistas
    String vecindario
    String comunidad
    String ibi
    String bus
    String supermercado
    String servicios
    String precio_deseado
	Integer nPrecio
    String razonamiento
    String minimo
    Boolean urge
    String mobiliario
    String agencias
    String publicidad
    String observaciones
    String llaves
    String aviso
    String observaciones_visita
    String contacto
    String telefono_contacto
    String horario_contacto
    String referencia_catastral
    String inscripcion
    String tomo
    String folio
    String finca
	Boolean web
	String zona
	String desc_corta
	String desc_larga

    static hasMany = [images: Image, prices:Price, floors:Floor, portals:Portal, attibutes: Attribute, owners: Owner]

    static belongsTo = [agent: Agent, tipo: Tipo_Inmueble]

    static constraints = {
        referencia blank: true, nullable: true
		estado blank: true, nullable: true
		operacion blank: true, nullable: true
        reservada blank: true, nullable: true
        vendida blank: true, nullable: true
		alquilado blank: true, nullable: true
		anulado blank: true, nullable: true
        fecha_toma blank:true, nullable: true
		fecha_pendiente blank:true, nullable: true
        address blank: true, nullable: true
        city blank: true, nullable: true
        zone blank: true, nullable: true
        latitud blank: true, nullable: true
        longitud blank: true, nullable: true
        direccion_gmaps blank:true, nullable: true
        superficie_terreno blank: true, nullable: true
		nSuperficie_terreno blank: true, nullable: true
        superficie_construida blank: true, nullable: true
		nSuperficie_construida blank: true, nullable: true
        ano_construccion blank: true, nullable: true
        ano_adquisicion blank: true, nullable: true
        estado_conservacion blank: true, nullable: true
        certf_energetico blank: true, nullable: true
        arrendatarios blank: true, nullable: true
        cargas blank: true, nullable: true
        motivo_venta blank: true, nullable: true
        plazo blank: true, nullable: true
        otros_general blank: true, nullable: true
        planos blank: true, nullable: true
        ascensor blank: true, nullable: true
		amueblado blank: true, nullable: true
        porches blank: true, nullable: true
        terrazas blank: true, nullable: true
        salones blank: true, nullable: true
        nSalones blank:true, nullable: true
        cocinas blank: true, nullable: true
        nCocinas blank:true, nullable: true
        dormitorios blank: true, nullable: true
        nDormitorios blank:true, nullable: true
        aseos blank: true, nullable: true
        nAseos blank:true, nullable: true
        banos blank: true, nullable: true
        nBanos blank:true, nullable: true
        lavaderos blank: true, nullable: true
        despensas blank: true, nullable: true
        garajes blank: true, nullable: true
        nGarajes blank: true, nullable: true
        trasteros blank: true, nullable: true
        instalaciones blank: true, nullable: true
        piscinas blank: true, nullable: true
        otros_instalaciones blank: true, nullable: true
        electricidad blank: true, nullable: true
        agua blank: true, nullable: true
        saneamiento blank: true, nullable: true
        calefaccion blank: true, nullable: true
        agua_caliente blank: true, nullable: true
        renovables blank: true, nullable: true
        fachada blank: true, nullable: true
        carpinteria_ext blank: true, nullable: true
        carpinteria_int blank: true, nullable: true
        pavimentos_exteriores blank: true, nullable: true
        pavimentos_interiores blank: true, nullable: true
        cocina blank: true, nullable: true
        cuartos_bano blank: true, nullable: true
        cerramiento blank: true, nullable: true
        otros_calidad blank: true, nullable: true
        vistas blank: true, nullable: true
        vecindario blank: true, nullable: true
        comunidad blank: true, nullable: true
        ibi blank: true, nullable: true
        bus blank: true, nullable: true
        supermercado blank: true, nullable: true
        servicios blank: true, nullable: true
        precio_deseado blank: true, nullable: true
		nPrecio blank: true, nullable: true
        razonamiento blank: true, nullable: true
        minimo blank: true, nullable: true
        urge blank: true, nullable: true
        mobiliario blank: true, nullable: true
        agencias blank: true, nullable: true
        publicidad blank: true, nullable: true
        observaciones blank: true, nullable: true
        llaves blank: true, nullable: true
        aviso blank: true, nullable: true
        observaciones_visita blank: true, nullable: true
        contacto blank: true, nullable: true
        telefono_contacto blank: true, nullable: true
        horario_contacto blank: true, nullable: true
        referencia_catastral blank: true, nullable: true
        inscripcion blank: true, nullable: true
        tomo blank: true, nullable: true
        folio blank: true, nullable: true
        finca blank: true, nullable: true
		web blank: true, nullable: true
		zona blank: true, nullable: true
		desc_corta blank: true, nullable: true
		desc_larga blank: true, nullable: true
    }
}
