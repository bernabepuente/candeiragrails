package es.xoubin.domain

class Tipo_Inmueble {

    String name

    static hasMany = [property: Property]

    static constraints = {
        name blank: true, nullable: true
    }
}
