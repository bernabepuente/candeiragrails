package es.xoubin.domain

class Image {

    String name
    String description
    Byte[] imageBlob

    static belongsTo = [property: Property]

    static constraints = {
        name blank: false, nullable: false
        description blank: true, nullable: true
        imageBlob blank:false, nullable: false
    }

    static mapping = {
        imageblob column: "imageBSlob", sqlType: "LONGBLOB"
    }
}
