package es.xoubin.domain

class Telephone {

    String telephone

    static constraints = {
        telephone blank: false, nullable: false
    }
}
