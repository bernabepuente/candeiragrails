package es.xoubin.domain

class Agent extends Person{

    static hasMany = [property: Property, customers: Customer]

    static constraints = {
        password blank: true, nullable: true
    }
}
