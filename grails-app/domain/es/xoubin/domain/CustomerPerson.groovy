package es.xoubin.domain

class CustomerPerson{

    String name
    String surname
    String nif
    String mail
    String movil
    String particular
    String trabajo
    String horario
    String idioma
    String trato

    static belongsTo = [customer: Customer]

    static constraints = {
        name blank: true, nullable: true
        surname blank: true, nullable: true
        nif blank: true, nullable: true
        movil blank: true, nullable: true
        particular blank: true, nullable: true
        trabajo blank: true, nullable: true
        horario blank: true, nullable: true
        idioma blank: true, nullable: true
        trato blank: true, nullable: true
    }
}
