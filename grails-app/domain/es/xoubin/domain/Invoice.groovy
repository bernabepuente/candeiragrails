package es.xoubin.domain

class Invoice {
	
	String reference
    Integer tipo = 0
	Integer tax	= 21
	String total
    String titular
	String payMethod
	// Quantity of invoice total taxes (total * (tax / 100))
	String totalInvoiceTaxes
	// Total of orders taxes
	String totalOrderTaxes
	String totalMinusOrderTaxes
	String totalWithTaxes
	String customer
	String nif
	String address
	Date dateInvoice
	Date dateCreated
	Date lastUpdated
    Double retention = 9
    String totalInvoiceRetention
	
	static hasMany = [invoiceOrders: InvoiceOrder]
	static belongsTo = [agent: Agent]
	
	static mapping = {		
		invoiceOrders sort: 'dateCreated', order: 'desc'
	}

    static constraints = {
		reference(blank: true, nullable: true)
        titular(blank: true, nullable: true)
		total(blank: true, nullable: true)
		payMethod(blank: true, nullable: true)
		totalInvoiceTaxes(blank: true, nullable: true)
		totalOrderTaxes(blank: true, nullable: true)
		totalMinusOrderTaxes(blank: true, nullable: true)
		totalWithTaxes(blank: true, nullable: true)
		tax(blank: true, nullable: true)
		dateInvoice(blank: true, nullable: true)
		customer (blank:true, nullable: true)
		nif (blank:true, nullable: true)
		address (blank:true, nullable: true)
        retention (blank:true, nullable: true)
        totalInvoiceRetention (blank:true, nullable: true)
    }
}