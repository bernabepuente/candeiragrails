package es.xoubin.domain

class Portal {

    String name
    String link

    static belongsTo = [property: Property]

    static constraints = {
        name blank: true, nullable: true
        link blank: true, nullable: true
    }
}
