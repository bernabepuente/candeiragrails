package es.xoubin.domain

class Price {

    String amount
    String fecha
    String comment

    static belongsTo = [property: Property]

    static constraints = {
        amount blank: true, nullable: true
        fecha blank: false, nullable: false
        comment blank: true, nullable: true

    }
}
