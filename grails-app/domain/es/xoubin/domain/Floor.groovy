package es.xoubin.domain

class Floor {

    String name
    String description

    static belongsTo = [property: Property]

    static constraints = {
        name blank: true, nullable: true
        description blank: true, nullable: true
    }
}
