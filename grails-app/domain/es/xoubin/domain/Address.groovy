package es.xoubin.domain

class Address {

    String street
    String city
    String zip
    String province
    String country
    boolean isDefault
    Person person
    Customer customer

    static belongsTo = [person: Person, customer:Customer]

    static constraints = {
        person blank: true, nullable: true
        customer blank: true, nullable: true
        street blank: false, nullable: false
        city blank: true, nullable: true
        zip blank: true, nullable: true
        province blank: true, nullable: true
        country blank: true, nullable: true
    }
}
