package es.xoubin.domain

class Zone {
	
	String name
	String description
	City city
	
	static constraints = {
		name blank:false, nullable: false
		description blank: true, nullable: true
		city blank:true, nullable: true
	}

}
