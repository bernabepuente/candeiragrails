package es.xoubin.domain


class Person {

    transient springSecurityService

    String name
    String surname
    String nif
    String mail
    String displayName
    Date dateCreated
    Date dateLogged
    Date dateLastPing
    boolean isConnected

    // s2-plugin
    String username
    String password
    boolean enabled = true
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired

    static transients = ['springSecurityService']

    static hasMany = [addresses: Address, phones: Phone]

    static constraints = {
        name blank: true, nullable: true
        surname blank: true, nullable: true
        nif blank: true, nullable: true
        mail blank: true, nullable: true
        displayName blank: true, nullable: true
        dateCreated blank: true, nullable: true
        dateLogged blank: true, nullable: true
        dateLastPing blank: true, nullable: true
        // required
        username blank: false, unique: true
        password blank: false
    }

    static mapping = {
        password column: '`password`'
        tablePerHierarchy false
    }

    Set<Role> getAuthorities() {
        PersonRole.findAllByPerson(this).collect { it.role } as Set
    }

    def beforeInsert() {
        encodePassword()
    }

    def beforeUpdate() {
        if (isDirty('password')) {
            encodePassword()
        }
    }

    protected void encodePassword() {
        password = springSecurityService.encodePassword(password)
    }

    public String toString() {
        def s
        if ((name != null) && (surname != null)) {
            s = "${name} ${surname}"
        } else {
            s = "${username}"
        }
        return s
    }
}
