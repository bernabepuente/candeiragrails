package es.xoubin.domain

class Tipo_inmueble {

    String name

    static constraints = {
        name blank: true, nullable: true
    }
}
