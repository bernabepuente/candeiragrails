package es.xoubin.domain

class Owner{

    String completeName
    String dni
    String address
    String phone1
    String phone2
    String mail
    String estado
    String regimen
    String horario_contacto
    String sector
    String observaciones
    String idioma
    String trato
    String cuenta

    static belongsTo = [property:Property]

    static constraints = {
        completeName blank: true, nullable: true
        dni blank: true, nullable: true
        phone1 blank: true, nullable: true
        phone2 blank: true, nullable: true
        mail blank: true, nullable: true
        estado blank: true, nullable: true
        regimen blank: true, nullable: true
        horario_contacto blank: true, nullable: true
        sector blank: true, nullable: true
        observaciones blank: true, nullable: true
        idioma blank: true, nullable: true
        trato blank: true, nullable: true
        cuenta blank: true, nullable: true
    }
}
