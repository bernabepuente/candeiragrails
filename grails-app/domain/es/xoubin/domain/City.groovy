package es.xoubin.domain

class City {
	
	String name
	String description
	
	static constraints = {
		name blank:false, nullable: false
		description blank: true, nullable: true
	}

}
