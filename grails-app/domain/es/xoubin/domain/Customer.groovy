package es.xoubin.domain

import org.grails.plugins.google.visualization.data.renderer.StringRenderer

class Customer{

    String referencia
    String fuente
    String sector
    String plazo_maximo
    Agent agent
    String observaciones
	String tipo

    static hasMany = [customerPersons: CustomerPerson, searches: Search, addresses: Address]

    static belongsTo = [agent: Agent]

    static constraints = {
        referencia blank: true, nullable: true
        fuente blank: true, nullable: true
        sector blank: true, nullable: true
        plazo_maximo blank: true, nullable: true
        observaciones blank: true, nullable: true
		tipo blank: true, nullable: true
    }
}
