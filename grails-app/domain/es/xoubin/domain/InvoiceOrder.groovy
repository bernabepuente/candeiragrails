package es.xoubin.domain

class InvoiceOrder {
	
	String name
	String reference
	String total
	Date dateCreated
	static belongsTo = [invoice: Invoice]
	
    static constraints = {
		name(sizeof: 5..100, blank: true)
		reference(sizeof: 5..255, blank: true, nullable: true)
		total(scale:2, blank: true, nullable: true)
	}
}
