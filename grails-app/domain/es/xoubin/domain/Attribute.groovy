package es.xoubin.domain

class Attribute {

    String name
    String description

    static belongsTo = [property: Property]

    static constraints = {
        name blank:false, nullable: false
        description blank: true, nullable: true
    }
}
