dataSource {
	pooled = true
	//driverClassName = "org.postgresql.Driver"
	driverClassName = "com.mysql.jdbc.Driver"
	// dialect = org.hibernate.dialect.PostgreSQLDialect
	//dialect = net.sf.hibernate.dialect.PostgreSQLDialect
	username = "root"
	password = "candeira_consultores"
   }
   hibernate {
	   cache.use_second_level_cache=true
	   cache.use_query_cache=true
	   cache.provider_class='com.opensymphony.oscache.hibernate.OSCacheProvider'
   }
   // environment specific settings
   environments {
	development {
	 dataSource {
	   // one of 'create', 'create-drop','update'
	   dbCreate = "update"
	   url = "jdbc:mysql://localhost:3306/gestion"
	   //url = "jdbc:mysql://139.99.193.134:3306/development"
	   username = "candeira"
	   password = "candeira_consultores"
            //password = "candeira_consultores"
         properties{
             maxActive = 50
             maxIdle = 25
             minIdle = 1
             initialSize = 1

             numTestsPerEvictionRun = 3
             maxWait = 10000

             testOnBorrow = true
             testWhileIdle = true
             testOnReturn = true

             validationQuery = "select now()"

             minEvictableIdleTimeMillis = 1000 * 60 * 5
             timeBetweenEvictionRunsMillis = 1000 * 60 * 5
         }
	 }
	}
	test {
	 dataSource {
	   dbCreate = "update"
	   //url = "jdbc:mysql://213.60.135.76:3306/gestion"
           url = "jdbc:mysql://localhost:3306/gestion"
	   //url = "jdbc:mysql://139.99.193.134:3306/development"
	   username = "candeira"
	   password = "candeira_consultores"
            //password = "candeira_consultores"
	 }
	}
	production {
	 dataSource {
	   dbCreate = "update"
	   //url = "jdbc:mysql://213.60.135.76:3306/gestion"
           url = "jdbc:mysql://localhost:3306/gestion"
	   //url = "jdbc:mysql://139.99.193.134:3306/development"
	   username = "candeira"
	   password = "candeira_consultores"
            //password = "candeira_consultores"
       properties{
           maxActive = 50
           maxIdle = 25
           minIdle = 1
           initialSize = 1

           numTestsPerEvictionRun = 3
           maxWait = 10000

           testOnBorrow = true
           testWhileIdle = true
           testOnReturn = true

           validationQuery = "select now()"

           minEvictableIdleTimeMillis = 1000 * 60 * 5
           timeBetweenEvictionRunsMillis = 1000 * 60 * 5
       }
	 }
	}
   }