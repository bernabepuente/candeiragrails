grails.servlet.version = "2.5" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.target.level = 1.6
grails.project.source.level = 1.6
//grails.project.war.file = "target/${appName}-${appVersion}.war"

// uncomment (and adjust settings) to fork the JVM to isolate classpaths
//grails.project.fork = [
//   run: [maxMemory:1024, minMemory:64, debug:false, maxPerm:256]
//]

grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // specify dependency exclusions here; for example, uncomment this to disable ehcache:
        // excludes 'ehcache'
    }
    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    checksums true // Whether to verify checksums on resolve
    legacyResolve false // whether to do a secondary resolve on plugin installation, not advised and here for backwards compatibility

    repositories {
        inherits true // Whether to inherit repository definitions from plugins
        mavenRepo "http://repo.grails.org/grails/core"
        mavenRepo "http://repo.grails.org/grails/plugins"
        grailsPlugins()
        grailsHome()
        grailsCentral()
		grailsRepo "https://grails.org/plugins"
		grailsRepo "http://grails.org/plugins"
        mavenLocal()
        mavenCentral()

        // uncomment these (or add new ones) to enable remote dependency resolution from public Maven repositories
        //mavenRepo "http://snapshots.repository.codehaus.org"
        //mavenRepo "http://repository.codehaus.org"
        //mavenRepo "http://download.java.net/maven/2/"
        //mavenRepo "http://repository.jboss.com/maven2/"
    }

    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes e.g.
		runtime 'mysql:mysql-connector-java:5.1.16'
		runtime 'commons-logging:commons-logging:1.2'
    }

    plugins {
		compile ":twitter-bootstrap:3.0.0"
		compile ":spring-security-core:1.2.7.3"
		compile ":cache:1.1.1"
		compile (":resources:1.2.1") {
			excludes ':resources:1.2.RC3'
		}

		// spring-security-ui
		compile ":spring-security-ui:0.2"
		compile ":famfamfam:1.0.1"
		compile ":jasper:1.8.0"
		compile ":bootstrap-file-upload:2.1.2"
		runtime ":jquery-ui:1.8.24"		
		runtime ":hibernate:$grailsVersion"
        runtime ":jquery:1.10.2"


        build ":tomcat:$grailsVersion"

        compile ":google-visualization:0.6.2"
    }
}
