modules = {
	application {
		resource url: 'js/application.js'
	}
	spinner {
		resource url: 'js/utils/ajax/spin.min.js'
	}
	momentjs {
		resource url: 'js/utils/date/moment.min.js'
	}
    validator {
        resource url: 'js/utils/validator/jquery.validate.min.js'
        resource url: 'js/utils/validator/additional-methods.min.js'
    }
    mustache {
        resource url: 'js/utils/mustache/mustache.js'
    }
    lightbox {
        resource url: 'js/lightbox.js'
        dependsOn 'jquery'
    }
}