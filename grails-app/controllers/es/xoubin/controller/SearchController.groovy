package es.xoubin.controller

import es.xoubin.domain.City
import es.xoubin.domain.Search
import es.xoubin.domain.Customer
import es.xoubin.domain.Tipo_Inmueble
import es.xoubin.domain.Zone
import org.springframework.dao.DataIntegrityViolationException

class SearchController {
	
	def _addSearch(){
		def p = params
		Search search=new Search()
		search.customer= Customer.findById(p.idCustomer)
        search.tipoInmueble=Tipo_Inmueble.findById(p.tipo)
        def cityInstance = City.findById(p.city)
        search.city = cityInstance
        def zoneInstance = Zone.findById(p.zone)
        search.zone =zoneInstance
        search.operacion=p.operacion
        search.sup_parcela_min=p.sup_parcela_min
        search.sup_parcela_max=p.sup_parcela_max
        search.sup_construida_min=p.sup_construida_min
        search.sup_construida_max=p.sup_construida_max
        search.dormitorios=p.dormitorios
        search.banos=p.banos
        search.limite_sur=p.limite_sur
        search.limite_norte=p.limite_norte
        search.limite_interior=p.limite_interior
        if (p.amueblado==true){
            search.amueblado = 1
        }
        else{
            search.amueblado = 0
        }
        def garaje = p.garaje
        if (garaje == 'true'){
            search.garaje = 1
        }
        else{
            search.garaje = 0
        }
        if (p.vistas_mar==true){
            search.vistas_mar = 1
        }
        else{
            search.vistas_mar = 0
        }
        if (p.llave==true){
            search.llave = 1
        }
        else{
            search.llave = 0
        }
        search.maximo=p.maximo
		if (search.save(flush: true)){
			render(contentType: 'text/json') {[
					'success': "OK",
					'id': search.id,
					'idCustomer': p.idCustomer,
					'tipo': Tipo_Inmueble.findById(p.tipo),
                    'operacion': p.operacion,
                    'city': City.findById(p.city),
                    'zone': Zone.findById(p.zone),
					'sup_parcela_min': p.sup_parcela_min,
					'sup_parcela_max': p.sup_parcela_max,
					'sup_construida_min': p.sup_construida_min,
					'sup_construida_max': p.sup_construida_max,
					'amueblado': p.amueblado,
					'dormitorios': p.dormitorios,
					'banos': p.banos,
					'limite_sur': p.limite_sur,
					'limite_norte': p.limite_norte,
					'limite_interior': p.limite_interior,
					'vistas_mar': p.vistas_mar,
					'llave': p.llave,
					'maximo': p.maximo
			]}
		}
		else{
			flash.message = message(code: 'search.add.fail')
			redirect(controller: "search", action: "list")
		}
	}
	
	def _updateSearch(){
		def p = params
		Search search=Search.findById(p.id)
		//actualizamos datos
		search.tipoInmueble=Tipo_Inmueble.findById(p.tipo)
        def cityInstance = City.findById(p.city)
        search.city = cityInstance
        def zoneInstance = Zone.findById(p.zone)
        search.zone =zoneInstance
        search.operacion=p.operacion
		search.sup_parcela_min=p.sup_parcela_min
		search.sup_parcela_max=p.sup_parcela_max
		search.sup_construida_min=p.sup_construida_min
		search.sup_construida_max=p.sup_construida_max
		search.dormitorios=p.dormitorios
		search.banos=p.banos
		search.limite_sur=p.limite_sur
		search.limite_norte=p.limite_norte
		search.limite_interior=p.limite_interior
        def amueblado = p.amueblado
        if (amueblado == 'true'){
            search.amueblado = 1
        }
        else{
            search.amueblado = 0
        }
		def garaje = p.garaje
		if (garaje == 'true'){
			search.garaje = 1
		}
		else{
			search.garaje = 0
		}
		if (p.vistas_mar==true){
			search.vistas_mar = 1
		}
		else{
			search.vistas_mar = 0
		}
		if (p.llave==true){
			search.llave = 1
		}
		else{
			search.llave = 0
		}
		search.maximo=p.maximo
		
		if (search.save(flush: true)){
			render(contentType: 'text/json') {[
					'success': "OK",
					'id': search.id,
					'idCustomer': p.idCustomer,
					'tipo': Tipo_Inmueble.findById(p.tipo),
                    'operacion': p.operacion,
                    'city': City.findById(p.city),
                    'zone': Zone.findById(p.zone),
					'sup_parcela_min': p.sup_parcela_min,
					'sup_parcela_max': p.sup_parcela_max,
					'sup_construida_min': p.sup_construida_min,
					'sup_construida_max': p.sup_construida_max,
					'amueblado': p.amueblado,
					'dormitorios': p.dormitorios,
					'banos': p.banos,
					'limite_sur': p.limite_sur,
					'limite_norte': p.limite_norte,
					'limite_interior': p.limite_interior,
					'vistas_mar': p.vistas_mar,
					'llave': p.llave,
					'maximo': p.maximo
			]}
		}
		else{
			flash.message = message(code: 'search.add.fail')
			redirect(controller: "search", action: "list")
		}
	}

	def _removeSearch(){
		Search search=Search.get(params.id)
		try {
			search.delete(flush: true)
			render(contentType: 'text/json') {[
					'success': "OK",
					'id': params.id
			]}
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'search.add.fail')
			redirect(controller: "Customer", action: "list")
		}
	}
}
