package es.xoubin.controller

import es.xoubin.domain.Person
import es.xoubin.domain.Phone
import org.springframework.dao.DataIntegrityViolationException

class PhoneController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [phoneInstanceList: Phone.list(params), phoneInstanceTotal: Phone.count()]
    }

    def create() {
        if (params.idClient) {
            return [phoneInstance: new Phone(params), clientInstance: Person.findById(params.idClient)]
        } else {
            flash.message = message(code: 'phone.cliente.choose.message')
            redirect(controller: "phone", action: "list")
        }
    }

    def _addPhone(){
        Phone phone=new Phone(params)
        phone.person= Person.findById(params.idPerson)
        if (phone.save(flush: true)){
            render(contentType: 'text/json') {[
                    'success': "OK",
                    'id': phone.id,
                    'idPerson': params.idPerson,
                    'phoneName': params.name,
                    'phoneNumber': params.number
            ]}
        }
        else{
            flash.message = message(code: 'phone.cliente.choose.message')
            redirect(controller: "agent", action: "list")
        }
    }

    def _removePhone(){
        Phone phone=Phone.get(params.id)
        try {
            phone.delete(flush: true)
            render(contentType: 'text/json') {[
                    'success': "OK",
                    'id': params.id
            ]}
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'phone.cliente.choose.message')
            redirect(controller: "agent", action: "list")
        }
    }

    def save() {
        def phoneInstance = new Phone(params)

        def personInstance = Person.findById(params.client.id)
        phoneInstance.person = personInstance
        if (!phoneInstance.save(flush: true)) {
            render(view: "create", model: [phoneInstance: phoneInstance, clientInstance: Person.findById(params.client.id)])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'phone.label'), phoneInstance.id])
        redirect(action: "create", params: [idClient: params.client.id])
    }

    def edit(Long id) {
        def phoneInstance = Phone.get(id)
        if (!phoneInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'phone.label'), id])
            redirect(action: "list")
            return
        }
        [phoneInstance: phoneInstance, clientInstance: Person.findById(params.idClient), _id: id]
    }

    def update(Long id, Long version) {
        def phoneInstance = Phone.get(id)
        if (!phoneInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'phone.label'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (phoneInstance.version > version) {
                phoneInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'phone.label', default: 'Telefono')] as Object[],
                        "Another user has updated this phone while you were editing")
                render(view: "edit", model: [phoneInstance: phoneInstance])
                return
            }
        }

        phoneInstance.properties = params

        def personInstance = Person.findById(params.client.id)
        phoneInstance.person = personInstance
        if (!phoneInstance.save(flush: true)) {
            render(view: "edit", model: [phoneInstance: phoneInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'phone.label'), phoneInstance.id])
        redirect(action: "edit", id: phoneInstance.id, params: [idClient: params.client.id])
    }

    def delete(Long id) {
        def phoneInstance = Phone.get(id)
        if (!phoneInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'phone.label'), id])
            redirect(action: "create", params: [idClient: params.client.id])
            return
        }

        try {
            phoneInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'phone.label'), id])
            redirect(action: "create", params: [idClient: params.client.id])
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'phone.label'), id])
            redirect(action: "create", params: [idClient: params.client.id])
        }
    }
}
