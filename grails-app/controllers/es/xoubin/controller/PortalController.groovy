package es.xoubin.controller

import es.xoubin.domain.Portal
import es.xoubin.domain.Property
import org.springframework.dao.DataIntegrityViolationException

class PortalController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [portalInstanceList: Portal.list(params), portalInstanceTotal: Portal.count()]
    }

    def _addPortal(){
        Portal portal=new Portal(params)
        portal.property= Property.findById(params.idProperty)
        if (portal.save(flush: true)){
            render(contentType: 'text/json') {[
                    'success': "OK",
                    'id': portal.id,
                    'idProperty': params.idProperty,
                    'portalName': params.name,
                    'portalLink': params.link
            ]}
        }
        else{
            flash.message = message(code: 'portal.save.error')
            redirect(controller: "property", action: "list")
        }
    }
	
	def  _updatePortal(){
		Portal portal=Portal.get(params.id)
		portal.name=params.name
		portal.link=params.link
		if (portal.save(flush: true)){
			render(contentType: 'text/json') {[
					'success': "OK",
					'id': portal.id,
					'idProperty': params.idProperty,
					'portalName': params.name,
					'portalLink': params.link
			]}
		}
		else{
			flash.message = message(code: 'portal.save.error')
			redirect(controller: "property", action: "list")
		}
	}

    def _removePortal(){
        Portal portal=Portal.get(params.id)
        try {
            portal.delete(flush: true)
            render(contentType: 'text/json') {[
                    'success': "OK",
                    'id': params.id
            ]}
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'portal.delete.error')
            redirect(controller: "property", action: "list")
        }
    }
}
