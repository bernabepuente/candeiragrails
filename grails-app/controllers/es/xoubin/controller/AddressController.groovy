package es.xoubin.controller

import es.xoubin.domain.Address
import es.xoubin.domain.Customer
import es.xoubin.domain.Person
import org.springframework.dao.DataIntegrityViolationException

class AddressController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [addressInstanceList: Address.list(params), addressInstanceTotal: Address.count()]
    }

    def create() {
        if (params.idClient) {
            return [addressInstance: new Address(params), clientInstance: Person.findById(params.idClient)]
        } else {
            flash.message = message(code: 'address.cliente.choose.message')
            redirect(controller: "client", action: "list")
        }
    }

    def _addAddress(){
        Address addr=new Address(params)
        switch (params.discriminator){
            case "person":
                addr.person= Person.findById(params.idEntity)
            case "customer":
                addr.customer= Customer.findById(params.idEntity)
        }
        if (addr.save(flush: true)){
            render(contentType: 'text/json') {[
                    'success': "OK",
                    'id': addr.id,
                    'idEntity': params.idEntity,
                    'addressStreet': params.street,
                    'addressCity': params.city,
                    'addressCountry': params.country,
                    'addressZip': params.zip,
            ]}
        }
        else{
            flash.message = message(code: 'address.cliente.choose.message')
            redirect(controller: "agent", action: "list")
        }
    }
	
	def _updateAddress(){
		Address addr=Address.findById(params.id)
		//actualizamos datos
		addr.street=params.street
		addr.city=params.city
		addr.country=params.country
		addr.zip=params.zip
		
		if (addr.save(flush: true)){
			render(contentType: 'text/json') {[
					'success': "OK",
					'id': addr.id,
					'addressStreet': params.street,
					'addressCity': params.city,
					'addressCountry': params.country,
					'addressZip': params.zip
			]}
		}
		else{
			flash.message = message(code: 'customerPerson.add.fail')
			redirect(controller: "customerPerson", action: "list")
		}
	}

    def _removeAddress(){
        Address addr=Address.get(params.id)
        try {
            addr.delete(flush: true)
            render(contentType: 'text/json') {[
                    'success': "OK",
                    'id': params.id
            ]}
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'address.cliente.choose.message')
            redirect(controller: "agent", action: "list")
        }
    }

    /**
     * Only one address can be default at same time, so we should check that there is no default address stored.
     * If this is the case, we must change the default stored values.
     */
    def save() {
        def addressInstance = new Address(params)

        if (addressInstance.isDefault == true) {
            def addressDefault = Address.findAll {
                isDefault == true
            }
            if (addressDefault.size() > 0) {
                def address = addressDefault.get(0)
                address.isDefault = false
                address.save(flush: true)
            }
        }

        def personInstance = Person.findById(params.client.id)
        addressInstance.person = personInstance
        if (!addressInstance.save(flush: true)) {
            render(view: "create", model: [addressInstance: addressInstance, clientInstance: personInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'address.label', default: 'Direccion'), addressInstance.id])
        redirect(action: "create", params: [idClient: params.client.id])
    }

    def edit(Long id) {
        def addressInstance = Address.get(id)
        if (!addressInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'address.label', default: 'Direccion'), id])
            redirect(action: "list")
            return
        }
        [addressInstance: addressInstance, clientInstance: Person.findById(params.idClient), _id: id]
    }

    def update(Long id, Long version) {
        def addressInstance = Address.get(id)
        if (!addressInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'address.label', default: 'Direccion'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (addressInstance.version > version) {
                addressInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'address.label', default: 'Direccion')] as Object[],
                        "Another user has updated this address while you were editing")
                render(view: "edit", model: [addressInstance: addressInstance])
                return
            }
        }

        addressInstance.properties = params
        if (addressInstance.isDefault) {
            def addressDefault = Address.findAll {
                isDefault == true
                id != addressInstance.id
            }
            System.out.println("Check: " + addressDefault)
            if (addressDefault.size() > 0) {
                def address = addressDefault.get(0)
                address.isDefault = false
                address.save(flush: true)
            }
        }

        def personInstance = Person.findById(params.client.id)
        addressInstance.person = personInstance
        if (!addressInstance.save(flush: true)) {
            render(view: "edit", model: [addressInstance: addressInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'address.label', default: 'Direccion'), addressInstance.id])
        redirect(action: "edit", id: addressInstance.id, params: [idClient: params.client.id])
    }

    def delete(Long id) {
        def addressInstance = Address.get(id)
        if (!addressInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'address.label', default: 'Direccion'), id])
            redirect(action: "create", params: [idClient: params.client.id])
            return
        }

        try {
            addressInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'address.label', default: 'Direccion'), id])
            redirect(action: "create", params: [idClient: params.client.id])
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'address.label', default: 'Direccion'), id])
            redirect(action: "create", params: [idClient: params.client.id])
        }
    }
}
