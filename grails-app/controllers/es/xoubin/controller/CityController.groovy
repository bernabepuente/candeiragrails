package es.xoubin.controller

import es.xoubin.domain.City
import org.springframework.dao.DataIntegrityViolationException

class CityController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [cityInstanceList: City.findAll(sort:"name", order:"asc"), cityInstanceTotal: City.count()]
    }

    def create() {
        return [cityInstance: new City(params)]
    }

    def _addCity(){
        City city=new City(params)
        if (city.save(flush: true)){
            render(contentType: 'text/json') {[
                    'success': "OK",
                    'id': city.id,
                    'cityName': params.name
            ]}
        }
        else{
            flash.message = message(code: 'city.choose.message')
            redirect(controller: "city", action: "list")
        }
    }

    def _removeCity(){
        City city=City.get(params.id)
        try {
            city.delete(flush: true)
            render(contentType: 'text/json') {[
                    'success': "OK",
                    'id': params.id
            ]}
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'city.choose.message')
            redirect(controller: "city", action: "list")
        }
    }

    def save() {
        def cityInstance = new City(params)

        if (!cityInstance.save(flush: true)) {
            render(view: "create", model: [cityInstance: cityInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'city.label'), cityInstance.id])
        redirect(action: "create")
    }

    def edit(Long id) {
        def cityInstance = City.get(id)
        if (!cityInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'city.label'), id])
            redirect(action: "list")
            return
        }
        [cityInstance: cityInstance, _id: id]
    }

    def update(Long id, Long version) {
        def cityInstance = City.get(id)
        if (!cityInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'city.label'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (cityInstance.version > version) {
                cityInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'city.label', default: 'Atributo')] as Object[],
                        "Another user has updated this city while you were editing")
                render(view: "edit", model: [cityInstance: cityInstance])
                return
            }
        }

        cityInstance.properties = params

        if (!cityInstance.save(flush: true)) {
            render(view: "edit", model: [cityInstance: cityInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'city.label'), cityInstance.id])
        redirect(action: "edit", id: cityInstance.id)
    }

    def delete(Long id) {
        def cityInstance = City.get(id)
        if (!cityInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'city.label'), id])
            redirect(action: "create", params: [idProperty: params.idProperty.id])
            return
        }

        try {
            cityInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'city.label'), id])
            redirect(action: "create", params: [idProperty: params.idProperty.id])
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'city.label'), id])
            redirect(action: "create", params: [idProperty: params.idProperty.id])
        }
    }
}
