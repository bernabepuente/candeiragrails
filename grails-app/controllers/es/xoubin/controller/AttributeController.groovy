package es.xoubin.controller

import es.xoubin.domain.Attribute
import es.xoubin.domain.Property
import org.springframework.dao.DataIntegrityViolationException

class AttributeController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [attributeInstanceList: Attribute.list(params), attributeInstanceTotal: Attribute.count()]
    }

    def create() {
        if (params.idProperty) {
            return [attributeInstance: new Attribute(params), propertyInstance: Property.findById(params.idProperty)]
        } else {
            flash.message = message(code: 'attribute.property.choose.message')
            redirect(controller: "attribute", action: "list")
        }
    }

    def save() {
        def attributeInstance = new Attribute(params)

        def propertyInstance = Property.findById(params.property.id)
        attributeInstance.property = propertyInstance
        if (!attributeInstance.save(flush: true)) {
            render(view: "create", model: [attributeInstance: attributeInstance, propertyInstance: Property.findById(params.property.id)])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'attribute.label'), attributeInstance.id])
        redirect(action: "create", params: [idProperty: params.property.id])
    }

    def edit(Long id) {
        def attributeInstance = Attribute.get(id)
        if (!attributeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'attribute.label'), id])
            redirect(action: "list")
            return
        }
        [attributeInstance: attributeInstance, propertyInstance: Property.findById(params.idProperty), _id: id]
    }

    def update(Long id, Long version) {
        def attributeInstance = Attribute.get(id)
        if (!attributeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'attribute.label'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (attributeInstance.version > version) {
                attributeInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'attribute.label', default: 'Atributo')] as Object[],
                        "Another user has updated this attribute while you were editing")
                render(view: "edit", model: [attributeInstance: attributeInstance])
                return
            }
        }

        attributeInstance.properties = params

        def propertyInstance = Property.findById(params.property.id)
        attributeInstance.property = attributeInstance
        if (!attributeInstance.save(flush: true)) {
            render(view: "edit", model: [attributeInstance: attributeInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'attribute.label'), attributeInstance.id])
        redirect(action: "edit", id: attributeInstance.id, params: [idProperty: params.idProperty.id])
    }

    def delete(Long id) {
        def attributeInstance = Attribute.get(id)
        if (!attributeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'attribute.label'), id])
            redirect(action: "create", params: [idProperty: params.idProperty.id])
            return
        }

        try {
            attributeInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'attribute.label'), id])
            redirect(action: "create", params: [idProperty: params.idProperty.id])
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'attribute.label'), id])
            redirect(action: "create", params: [idProperty: params.idProperty.id])
        }
    }
}
