package es.xoubin.controller

import es.xoubin.domain.Agent
import es.xoubin.domain.City
import es.xoubin.domain.Image
import es.xoubin.domain.Owner
import es.xoubin.domain.Property
import es.xoubin.domain.Search
import es.xoubin.domain.Tipo_Inmueble
import es.xoubin.domain.Zone
import grails.converters.JSON
import grails.plugins.springsecurity.SpringSecurityService
import grails.plugins.springsecurity.ui.AbstractS2UiController

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest

class PropertyController extends AbstractS2UiController{

    def propertyService
    def ownerService
    def imageService
    def priceService
    def agentService
    def springSecurityService
    def propertyInstanceList
    def busqueda

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        setIfMissing 'max', 50, 100
        setIfMissing 'offset', 0
        Integer max = params.int('max')
        Integer offset = params.int('offset')
        def term = params.term

        params.max = Math.min(max ?: 10, 100)
        def user=springSecurityService.principal.id
        def roles = springSecurityService.getPrincipal().getAuthorities()

        propertyInstanceList=null
        busqueda=term


        def total
        Search search
        if (params.idSearch){
            search=Search.findById(params.idSearch.toInteger())
        }
        if (search){
            propertyInstanceList=propertyService.searchFromCustomer(search, params, max, offset)['propertyInstanceList']
            total=propertyService.searchFromCustomer(search, params, max, offset)['total']
        }
        else{
            propertyInstanceList=propertyService.propertyList(params, max, offset)['propertyInstanceList']
            total=propertyService.propertyList(params, max, offset)['total']
        }

        def totalCount = params.term ? propertyInstanceList.size() : Property.count()
        def offsetBase = (offset == 0 ? 50 : (offset + 10))
        def endPos = (offsetBase < totalCount ? offsetBase : totalCount)
        def sort = params.sort
        def order = params.order


        def model = [propertyInstanceList: propertyInstanceList, ownerInstanceList:ownerService.ownerList()['ownerInstanceList'], cityInstanceList: City.listOrderByName(), zoneInstanceList: Zone.listOrderByName(), ownerInstanceTotal:ownerService.ownerList()['ownerInstanceTotal'], agentId: user, rol:roles, totalCount: total, searched: true, term: term, startPos: (offset + 1), endPos: endPos, max:max, sort: sort, order: order, offset:offset]
        render(model: model, contentType: 'text/html;charset=UTF-8', view: 'list')
    }

    def list_venta() {
        setIfMissing 'max', 50, 100
        setIfMissing 'offset', 0
        Integer max = params.int('max')
        Integer offset = params.int('offset')
        def term = params.term
        params.max = Math.min(max ?: 10, 100)
        def user=springSecurityService.principal.id
        def roles = springSecurityService.getPrincipal().getAuthorities()

        def propertyInstanceList
        def total
        Search search
        if (params.idSearch){
            search=Search.findById(params.idSearch.toInteger())
        }
        if (search){
            propertyInstanceList=propertyService.searchFromCustomer(search, params, max, offset)['propertyInstanceList']
            total=propertyService.searchFromCustomer(search, params, max, offset)['total']
        }
        else{
            propertyInstanceList=propertyService.propertyList(params, max, offset)['propertyInstanceList']
            total=propertyService.propertyList(params, max, offset)['total']
        }

        def totalCount = params.term ? propertyInstanceList.size() : Property.count()
        def offsetBase = (offset == 0 ? 50 : (offset + 10))
        def endPos = (offsetBase < totalCount ? offsetBase : totalCount)
        def model = [propertyInstanceList: propertyInstanceList, ownerInstanceList:ownerService.ownerList()['ownerInstanceList'], cityInstanceList: City.list(), zoneInstanceList: Zone.list(), ownerInstanceTotal:ownerService.ownerList()['ownerInstanceTotal'], agentId: user, rol:roles, totalCount: total, searched: true, term: params.term, startPos: (offset + 1), endPos: endPos, max:max, offset:offset]
        render view: 'list_venta', model: model
    }

    def newProperty() {
        def propertyInstance = propertyService.createProperty(params)
        propertyInstance.agent=Agent.findById(springSecurityService.principal.id)
        render(view: "create", model: [propertyInstance: propertyInstance, agentInstanceList: Agent.list()])
    }

    def create() {
        if (!params.cboAgent){
            params.cboAgent=springSecurityService.principal.id
        }
        def propertyInstance = propertyService.createProperty(params)
        [propertyInstance: propertyInstance, agentInstanceList: agentService.agentList(params)['agentInstanceList'], tipoInstanceList: Tipo_Inmueble.findAll(sort:"name", order:"asc"), ownerInstanceList:ownerService.ownerList()['ownerInstanceList'], ownerInstanceTotal:ownerService.ownerList()['ownerInstanceTotal']]
    }

    def save() {
        def propertyInstance = propertyService.saveProperty(params)
        if (propertyInstance==null) {
            flash.message = message(code: 'default.error.save', args: [message(code: 'property.label'), params.referencia])
            render(view: "create", model: [propertyInstance: propertyInstance])
        } else {
            flash.message = message(code: 'default.created.message', args: [message(code: 'property.label'), propertyInstance.referencia])
            redirect(action: "edit", id: propertyInstance.id)
        }
    }

    def show(Long id) {
        def propertyInstance=propertyService.showProperty(id)
        if (!propertyInstance) {
            flash.message = message(code: 'default.not.found.message', args: [
                    message(code: 'property.label'),
                    propertyInstance.referencia
                ])
            redirect(action: "list")
            return
        }
        [propertyInstance: propertyInstance]
    }

    def edit(Long id) {
        def propertyInstance = propertyService.editProperty(id)
        if (!propertyInstance) {
            flash.message = message(code: 'default.not.found.message', args: [
                    message(code: 'property.label'),
                    propertyInstance.referencia
                ])
            redirect(action: "list")
            return
        }
        def p = params        
        def countImages=propertyService.countImages(id)
        def countFiles=propertyService.countFiles(id)
        def precioAnterior
        def prices = priceService.ListByProperty(propertyInstance)['priceInstanceList']
        def images = imageService.ListByProperty(propertyInstance)['imageInstanceList']
        def localidad = ""
        if(propertyInstance.cityId){
            localidad = City.findById(propertyInstance.cityId).name
        }
        def zona = ""
        if (propertyInstance.zoneId){
            Zone.findById(propertyInstance.zoneId).name
        }
        Integer i=0
        for ( price in prices ) {
            if (i==prices.size()-2){
                precioAnterior=price
            }
            i=i+1
        }

        def zonesList
        if (propertyInstance.city){
            zonesList=Zone.findAllByCity(propertyInstance.city, [sort:"name", order:"asc"])
        }
        else{
            zonesList=Zone.list(sort:"name", order:"asc");
        }
        
        Integer max = params.int('max')
        Integer offset = params.int('offset')
        def offsetBase = 0
        if (offset != null){
            offsetBase = (offset == 0 ? 50 : (offset + 10))
        }
        else{
            offset = 0
        }
        def term = params.term
        def totalPropertyList
        if (propertyInstanceList != null){
            totalPropertyList = propertyInstanceList.size()
        }
        else{
            totalPropertyList = 0
        }
        def totalCount = params.term ? totalPropertyList : Property.count()
        def endPos = (offsetBase < totalCount ? offsetBase : totalCount)
        def sort = params.sort
        def order = params.order

        [propertyInstance: propertyInstance, localidad:localidad, zona:zona, agentInstanceList: agentService.agentList(params)['agentInstanceList'], term:params.term, cityInstanceList: City.listOrderByName(), zoneInstanceList: zonesList, tipoInstanceList: Tipo_Inmueble.findAll(sort:"name", order:"asc"), imagesInstanceList:images, imagesInstanceTotal:images.size(), term: p.term, max:p.max, offset:p.offset, ownerInstanceList:ownerService.ownerList()['ownerInstanceList'], ownerInstanceTotal:ownerService.ownerList()['ownerInstanceTotal'], countImages:countImages, countFiles:countFiles, pricesList: prices, precioAnterior: precioAnterior, term: term, startPos: (offset + 1), endPos: endPos, max:max, sort: sort, order: order, offset:offset]
    }

    def update_close(Long id) {
        if (id != null){
            def propertyInstance = propertyService.updateProperty(id, params)
            flash.message = message(code: 'default.updated.message', args:  [message(code: 'property.label'), propertyInstance?.referencia])
        }
        setIfMissing 'max', 50, 100
        setIfMissing 'offset', 0
        Integer max = params.int('max')
        Integer offset = params.int('offset')
        def term = params.term
        params.max = Math.min(max ?: 10, 100)
        def user=springSecurityService.principal.id
        def roles = springSecurityService.getPrincipal().getAuthorities()

        propertyInstanceList=null

        def total
        Search search
        if (params.idSearch){
            search=Search.findById(params.idSearch.toInteger())
        }
        if (search){
            propertyInstanceList=propertyService.searchFromCustomer(search, params, max, offset)['propertyInstanceList']
            total=propertyService.searchFromCustomer(search, params, max, offset)['total']
        }
        else{
            propertyInstanceList=propertyService.propertyList(params, max, offset)['propertyInstanceList']
            total=propertyService.propertyList(params, max, offset)['total']
        }

        Integer endPos = params.int('endPos')
        def sort = params.sort
        def order = params.order
        
        def model = [propertyInstanceList: propertyInstanceList, ownerInstanceList:ownerService.ownerList()['ownerInstanceList'], cityInstanceList: City.listOrderByName(), zoneInstanceList: Zone.listOrderByName(), ownerInstanceTotal:ownerService.ownerList()['ownerInstanceTotal'], agentId: user, rol:roles, totalCount: total, searched: true, term: term, endPos: endPos, sort: sort, order: order, offset:offset, 'action': 'list']

        render view: 'list', model: model
    }

    def update(Long id) {
        def propertyInstance = propertyService.updateProperty(id, params)
        if (propertyInstance == null) {
            render(view: "create", model: [propertyInstance: propertyInstance])
            return
        }
        flash.message = message(code: 'default.updated.message', args:  [
                message(code: 'property.label'),
                propertyInstance.referencia
            ])
        redirect(action: "edit", id: propertyInstance.id, params: [term: params.term])
    }

    def delete(Long id) {
        try {
            String nameOfDeleted = propertyService.deleteProperty(id)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'property.label'), nameOfDeleted])
            redirect(action: "list", params: [term: params.term, max:params.max, offset:params.offset])
            return

        } catch (Exception e) {
            flash.message = e.getMessage()
            redirect(action: "list", params: [term: params.term, max:params.max, offset:params.offset])
            return

        } catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'inmueble.label'), id])
            redirect(action: "list", params: [term: params.term, max:params.max, offset:params.offset])
            return
        }
    }

    def upload() {
        def p = params
        def results = []
        def propertyInstance = Property.findById(p.propertyId.toLong())
        if (request instanceof MultipartHttpServletRequest){
            for(filename in request.getFileNames()){
                MultipartFile file = request.getFile(filename)
                def imageInstance = new Image()
                //guardamos el blob
                imageInstance.imageBlob = file.bytes
                imageInstance.name=file.originalFilename
                propertyInstance.addToImages(imageInstance)
                propertyInstance.save(flush:true)
                results << [
                    name: file.originalFilename
                ]
            }
        }
        render results as JSON
    }

    def _removeImg(Long id){
        def nameDeleted
        def result
        try{
            nameDeleted=imageService.deleteImage(id)
            result = [success: true]
        } catch (Exception e) {
            result = [success: false]
        }
        render result as JSON
    }
	
    def _searchFromProperty() {
        setIfMissing 'max', 50, 100
        setIfMissing 'offset', 0
        Integer max = params.int('max')
        Integer offset = params.int('offset')
        params.max = Math.min(max ?: 10, 100)
        def idP=params.id
        Property property = Property.get(idP)
        def model = [idProperty:idP]
		
        redirect(controller:"customer", action: "list", params:model)
    }

    def reportList = {
        def user=springSecurityService.principal.id

        params.put('busqueda', params.busqueda)
        params.put('agent_id', user)
        //launch the report
        chain(controller:'jasper', action:'index', params:params)
    }
}
