package es.xoubin.controller

import es.xoubin.domain.Person
import grails.plugins.springsecurity.Secured
import grails.plugins.springsecurity.ui.AbstractS2UiController

import org.codehaus.groovy.grails.plugins.springsecurity.NullSaltSource
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils

/**
 * @author <a href='mailto:burt@burtbeckwith.com'>Burt Beckwith</a>
 */
@Secured(['ROLE_ADMIN'])
class UserController extends AbstractS2UiController {

	def saltSource
	def userCache
	def userService

	def create = {
		def user = lookupUserClass().newInstance(params)
		[user: user, authorityList: userService.sortedRoles(lookupRoleClass())]
	}

	def save = {
		def user = lookupUserClass().newInstance(params)
		if (params.password) {
			String salt = saltSource instanceof NullSaltSource ? null : params.username
			user.password = springSecurityUiService.encodePassword(params.password, salt)
			user.displayName=user.username
		}
		
		if (user.save(flush: true)) {
			userService.addRoles(user, lookupRoleClass(), lookupUserRoleClass(), params)
			flash.message = "${message(code: 'default.created.message', args: [message(code: 'user.label'), user.displayName])}"
			redirect action: edit, id: user.id
		} else {
			// Obligamos al usuario a introducir la contrase�a de nuevo
			user.password = ""
			flash.message = "${message(code: 'default.error.save', args: [message(code: 'user.label').toLowerCase()])}"
			render(view: "create", model: [user: user, authorityList: userService.sortedRoles(lookupRoleClass())])
		}
	}

    def changePass(params){
        def user=springSecurityService.currentUser
        [personInstance:user]
    }

    def updatePass(params){
        def userInstance=userService.updatePass(params)
        if (userInstance==null) {
            flash.message = message(code: 'default.error.save', args: [message(code: 'agente.label')])
            render(view: "changePass", model: [userInstance: userInstance])
        } else {
            flash.message = message(code: 'default.updated.message', args: [message(code: 'agente.label'), userInstance.name])
            redirect(action: "changePass", agenteInstance: userInstance)
        }
    }

	def edit = {
		def userInstance = Person.findById(params.id)
		if (!userInstance) {
			return
		}

		return userService.buildUserModel(userInstance, lookupRoleClass())
	}

    def update() {
		def user = Person.findById(params.uId)
		if (user) {
			String passwordFieldName = SpringSecurityUtils.securityConfig.userLookup.passwordPropertyName
			def oldPassword = user."$passwordFieldName"
			user.properties = params
			if (params.password && !params.password.equals(oldPassword)) {
				String salt = saltSource instanceof NullSaltSource ? null : params.username
				user."$passwordFieldName" = springSecurityUiService.encodePassword(params.password, salt)
			}
	
			if (!user.save(flush: true)) {
				render view: 'edit', model: userService.buildUserModel(user, lookupRoleClass())
				return
			}
	
			String usernameFieldName = SpringSecurityUtils.securityConfig.userLookup.usernamePropertyName
	
			lookupUserRoleClass().removeAll user
			userService.addRoles (user, lookupRoleClass(), lookupUserRoleClass(), params)
			userCache.removeUserFromCache user[usernameFieldName]
			flash.message = "${message(code: 'default.updated.message', args: [message(code: 'user.label'), user.toString()])}"
			redirect action: edit, id: user.id
		} else {
			redirect (action: "search")
		}
	}


	def delete() {
		def user = Person.findById(params.uId)
		if (user) {
			String usernameFieldName = SpringSecurityUtils.securityConfig.userLookup.usernamePropertyName
			String deletedName = user.toString()
			try {
				lookupUserRoleClass().removeAll user
				user.delete flush: true
				userCache.removeUserFromCache user[usernameFieldName]
				flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'user.label'), deletedName])}"				
			}
			catch (Exception e) {
				flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'user.label'), deletedName])}"				
				redirect (action: "edit", id: params.uId)
			}
		}
		redirect (action: "search")
	}

	def search() {
		render (view: 'search', model: [searched: false]) 
	}

	def userSearch = {		
		def userInstanceList = Person.findAll {
			username ==~ params.term.concat("%")
		}
		
		def model = [results: userInstanceList, totalCount: userInstanceList.size(), searched: true]
		render view: 'search', model: model
	}	
}