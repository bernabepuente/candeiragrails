package es.xoubin.controller

import es.xoubin.domain.Image
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.springframework.dao.DataIntegrityViolationException

class ImageController {

    def imageService

    private static Log log = LogFactory.getLog("file."+ ImageController.class.getName())

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [imageInstanceList:imageService.imageList(params)['imageInstanceList'], imageInstanceTotal:imageService.imageList(params)['imageInstanceTotal']]
    }

    /**
     * Elimina una imagen de una entidad vía JSON.
     *
     * @return
     */
    def _removeImage() {
        imageService.removeImageFromEntity(params)
        render(contentType: 'text/json') {[
                'success': "OK"
        ]}
    }

    def create() {
        def imageInstance = imageService.createImage(params)
        [imageInstance: imageInstance]
    }

    /**
     * El params.id es el id de la entidad propietaria.
     *
     * @return
     */
    def saveFromEntity() {
        def entityId = params.id
        def requestFile = request.getFile('imageBlob')
        if (!requestFile.empty) {
            def imageInstance = imageService.saveImage(params, requestFile)
            if (imageInstance) {
                flash.message = message(code: 'default.created.message', args: [message(code: 'image.label'), imageInstance.id])
            } else {
                flash.message = message(code: 'default.error.save', args: [message(code: 'image.label')])
            }
        } else {
            flash.message = message(code: 'image.empty.blobImage')
        }
        redirect(controller: params.discriminator, action: "addImage", id: entityId)
    }

    def save() {
        def requestFile = request.getFile('imageBlob')
        def imageInstance = imageService.saveImage(params, requestFile)
        if (imageInstance) {
            flash.message = message(code: 'default.created.message', args: [message(code: 'image.label'), imageInstance.id])
            redirect(action: "edit", id: imageInstance.id)
        } else {
            flash.message = message(code: 'default.error.save', args: [message(code: 'image.label')])
            redirect(action: "create")
        }
    }

    def show(Long id) {
        def imageInstance = imageService.showImage(id)
        if (!imageInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'image.label'), id])
            redirect(action: "list")
            return
        }
        [imageInstance: imageInstance]
    }

    def edit(Long id) {
        def imageInstance = imageService.editImage(id)
        if (!imageInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'image.label'), id])
            redirect(action: "list")
            return
        }
        [imageInstance: imageInstance]
    }

    def update(Long id, Long version) {
        def f = request.getFile('imageBlob')
        def imageInstance = imageService.updateImage(id, params, f)
        if (imageInstance) {
            flash.message = message(code: 'default.updated.message', args:  [message(code: 'image.label'), id])
        } else {
            flash.message = message(code: 'default.error.edit')
        }
        redirect(action: "edit", id: id)
    }

    def delete(Long id) {
        try {
            String nameOfDeleted = imageService.deleteImage(id)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'image.label'), nameOfDeleted])
            redirect(action: "list")
            return

        } catch (Exception e) {
            flash.message = e.getMessage()
            redirect(action: "list")
            return

        } catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'image.label'), id])
            redirect(action: "list")
            return
        }
    }

    def getImg = {
        def img = Image.get(params.id)
        //println(img.name)
        response.setContentType("image/jpeg")
        response.setContentLength(img.imageBlob.size())
        OutputStream out = response.getOutputStream();
        out.write(img.imageBlob);
        out.close();
    }

    def getFile = {
        def img = Image.get(params.id)
        if (img.name.contains('.pdf')){
            response.setContentType("application/pdf")
        }
        else
        {
            response.setContentType("application/download")
        }
        response.setContentLength(img.imageBlob.size())
        OutputStream out = response.getOutputStream();
        out.write(img.imageBlob);
        out.close();
    }

    def displayImageBlob = {
        if (params.id) {
            def image = Image.get(params.id)
            if (image) {
                byte[] blob = image.imageBlob
                response.outputStream << blob
            }
        }
    }

    def deleteImageBlob(Long id) {
        def imageInstance = Image.get(id)
        if (!imageInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'image.label'), id])
            redirect(action: "list")
            return
        }
        imageInstance.imageBlob = null
        if (!imageInstance.save(flush: true)) {
            redirect(view: "edit", id: id)
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'image.label'), imageInstance.id])
        redirect(action: "edit", id: id)
    }
}
