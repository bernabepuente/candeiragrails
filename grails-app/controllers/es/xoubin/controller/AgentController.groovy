package es.xoubin.controller

import es.xoubin.domain.Property
import org.springframework.dao.DataIntegrityViolationException

class AgentController {

    def agentService
    def propertyService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [agentInstanceList:agentService.agentList(params)['agentInstanceList'], agentInstanceTotal:agentService.agentList(params)['agentInstanceTotal']]
    }

    def create() {
        def agentInstance = agentService.createAgent(params)
        [agentInstance: agentInstance]
    }

    def save() {
        def agentInstance = agentService.saveAgent(params)
        if (agentInstance==null) {
            flash.message = message(code: 'default.error.save', args: [message(code: 'agent.label')])
            render(view: "create", model: [agentInstance: agentInstance])
        } else {
            flash.message = message(code: 'default.created.message', args: [message(code: 'agent.label'), agentInstance.name])
            redirect(action: "edit", id: agentInstance.id)
        }
    }

    def show(Long id) {
        def agentInstance=agentService.showAgent(id)
        if (!agentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'agent.label'), agentInstance.name])
            redirect(action: "list")
            return
        }
        [agentInstance: agentInstance]
    }

    def edit(Long id) {
        def agentInstance = agentService.editAgent(id)
        if (!agentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'agent.label'), agentInstance.name])
            redirect(action: "list")
            return
        }
        def propertyInstanceList = propertyService.ListByAgent(agentInstance)
        [agentInstance: agentInstance, propertyInstanceList:propertyInstanceList.propertyInstanceList, propertyInstanceTotal:propertyInstanceList.propertyInstanceTotal, personInstance: agentInstance, entityInstance: agentInstance, discriminator:"person"]
    }

    def editProperty(Long id) {
        def propertyInstance = propertyService.editProperty(id)
        if (!propertyInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'property.label')])
            redirect(action: "list")
            return
        }
        redirect(controller: "property", action: "edit", id:propertyInstance.id)
        return

    }

    def update(Long id) {
        def agentInstance = agentService.updateAgent(id, params)
        if (agentInstance == null) {
            render(view: "create", model: [agentInstance: agentInstance])
            return
        }
        flash.message = message(code: 'default.updated.message', args:  [message(code: 'agent.label'), [agentInstance.name]])
        redirect(action: "edit", id: agentInstance.id)
    }

    def delete(Long id) {
        try {
            String nameOfDeleted = agentService.deleteAgent(id)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'agent.label'), nameOfDeleted])
            redirect(action: "list")
            return

        } catch (Exception e) {
            flash.message = e.getMessage()
            redirect(action: "list")
            return

        } catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'agent.label'), id])
            redirect(action: "list")
            return
        }
    }
}
