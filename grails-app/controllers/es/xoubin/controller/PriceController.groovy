package es.xoubin.controller

import es.xoubin.domain.Price
import es.xoubin.domain.Property
import org.springframework.dao.DataIntegrityViolationException

class PriceController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [priceInstanceList: Price.list(params), priceInstanceTotal: Price.count()]
    }

    def create() {
        if (params.idProperty) {
            return [priceInstance: new Price(params), propertyInstance: Property.findById(params.idProperty)]
        } else {
            flash.message = message(code: 'price.create.error')
            redirect(controller: "price", action: "list")
        }
    }

    def _addPrice(){
        Price price=new Price(params)
        price.property= Property.findById(params.idProperty)
        if (price.save(flush: true)){
            render(contentType: 'text/json') {[
                    'success': "OK",
                    'id': price.id,
                    'idProperty': params.idProperty,
                    'priceFecha': params.fecha,
                    'priceAmount': params.amount,
                    'priceComment': params.comment
            ]}
        }
        else{
            flash.message = message(code: 'price.save.error')
            redirect(controller: "property", action: "list")
        }
    }
	
	def _updatePrice(){
		Price price=Price.get(params.id)
		price.fecha= params.fecha
		price.amount= params.amount
		price.comment= params.comment
		if (price.save(flush: true)){
			render(contentType: 'text/json') {[
					'success': "OK",
					'id': price.id,
					'priceFecha': params.fecha,
					'priceAmount': params.amount,
					'priceComment': params.comment
			]}
		}
		else{
			flash.message = message(code: 'price.save.error')
			redirect(controller: "property", action: "list")
		}
	}

    def _removePrice(){
        Price price=Price.get(params.id)
        try {
            price.delete(flush: true)
            render(contentType: 'text/json') {[
                    'success': "OK",
                    'id': params.id
            ]}
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'price.delete.error')
            redirect(controller: "property", action: "list")
        }
    }

    def save() {
        def priceInstance = new Price(params)

        def propertyInstance = Property.findById(params.property.id)
        priceInstance.property = propertyInstance
        if (!priceInstance.save(flush: true)) {
            render(view: "create", model: [priceInstance: priceInstance, propertyInstance: Property.findById(params.property.id)])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'price.label'), priceInstance.id])
        redirect(action: "create", params: [idProperty: params.property.id])
    }

    def edit(Long id) {
        def priceInstance = Price.get(id)
        if (!priceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'price.label'), id])
            redirect(action: "list")
            return
        }
        [priceInstance: priceInstance, propertyInstance: Property.findById(params.idProperty), _id: id]
    }

    def update(Long id, Long version) {
        def priceInstance = Price.get(id)
        if (!priceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'price.label'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (priceInstance.version > version) {
                priceInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'price.label', default: 'Atributo')] as Object[],
                        "Another user has updated this price while you were editing")
                render(view: "edit", model: [priceInstance: priceInstance])
                return
            }
        }

        priceInstance.properties = params

        def propertyInstance = Property.findById(params.property.id)
        priceInstance.property = priceInstance
        if (!priceInstance.save(flush: true)) {
            render(view: "edit", model: [priceInstance: priceInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'price.label'), priceInstance.id])
        redirect(action: "edit", id: priceInstance.id, params: [idProperty: params.idProperty.id])
    }

    def delete(Long id) {
        def priceInstance = Price.get(id)
        if (!priceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'price.label'), id])
            redirect(action: "create", params: [idProperty: params.idProperty.id])
            return
        }

        try {
            priceInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'price.label'), id])
            redirect(action: "create", params: [idProperty: params.idProperty.id])
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'price.label'), id])
            redirect(action: "create", params: [idProperty: params.idProperty.id])
        }
    }
}
