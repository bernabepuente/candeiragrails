package es.xoubin.controller

import es.xoubin.domain.CustomerPerson
import es.xoubin.domain.Customer
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class CustomerpersonController {

    def _addPerson(){
        CustomerPerson customerPerson=new CustomerPerson(params)
        customerPerson.customer= Customer.findById(params.idCustomer)
        if (customerPerson.save(flush: true)){
            render(contentType: 'text/json') {[
                    'success': "OK",
                    'id': customerPerson.id,
                    'idCustomer': params.idCustomer,
                    'customerPersonName': params.name,
                    'customerPersonSurName': params.surname,
                    'customerPersonNif': params.nif,
                    'customerPersonMovil': params.movil,
                    'customerPersonParticular': params.particular,
                    'customerPersonTrabajo': params.trabajo,
                    'customerPersonHorario': params.horario,
                    'customerPersonMail': params.mail,
                    'customerPersonIdioma': params.idioma,
                    'customerPersonTrato': params.trato
            ]}
        }
        else{
            flash.message = message(code: 'customerPerson.add.fail')
            redirect(controller: "customerPerson", action: "list")
        }
    }
	
	def _updatePerson(){
		CustomerPerson customerPerson=CustomerPerson.findById(params.id)
		//actualizamos datos
		customerPerson.name=params.name
		customerPerson.surname=params.surname
		customerPerson.nif=params.nif
		customerPerson.movil=params.movil
		customerPerson.particular=params.particular
		customerPerson.trabajo=params.trabajo
		customerPerson.horario=params.horario
		customerPerson.mail=params.mail
		customerPerson.idioma=params.idioma
		customerPerson.trato=params.trato
		
		if (customerPerson.save(flush: true)){
			render(contentType: 'text/json') {[
					'success': "OK",
					'id': customerPerson.id,
					'idCustomer': params.idCustomer,
					'customerPersonName': params.name,
					'customerPersonSurName': params.surname,
					'customerPersonNif': params.nif,
					'customerPersonMovil': params.movil,
					'customerPersonParticular': params.particular,
					'customerPersonTrabajo': params.trabajo,
					'customerPersonHorario': params.horario,
					'customerPersonMail': params.mail,
					'customerPersonIdioma': params.idioma,
					'customerPersonTrato': params.trato
			]}
		}
		else{
			flash.message = message(code: 'customerPerson.add.fail')
			redirect(controller: "customerPerson", action: "list")
		}
	}

    def _removePerson(){
        CustomerPerson customerPerson=CustomerPerson.get(params.id)
        try {
            customerPerson.delete(flush: true)
            render(contentType: 'text/json') {[
                    'success': "OK",
                    'id': params.id
            ]}
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'customerPerson.add.fail')
            redirect(controller: "customerPerson", action: "list")
        }
    }
	
	/**
	 * Busqueda AJAX de usuarios.
	 * @return
	 */
	def _search() {
		if ((params.term) && (params.term.trim().length() > 0)) {
			def results = CustomerPerson.where { name ==~ params.term.trim().concat("%") }.list().collect { rowResult ->
				[
					id: rowResult.id,
					value: rowResult.name + ' ' + rowResult.surname,
					nif: rowResult.nif
				]
			}
			render results as JSON
		} else {
			render ""
		}
	}
}
