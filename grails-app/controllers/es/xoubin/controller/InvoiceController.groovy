package es.xoubin.controller

import es.xoubin.domain.Invoice
import es.xoubin.domain.InvoiceOrder
import grails.plugins.springsecurity.ui.AbstractS2UiController;

import org.springframework.dao.DataIntegrityViolationException

class InvoiceController extends AbstractS2UiController{

    def invoiceService
	def invoiceOrder
	def agentService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list = {
		setIfMissing 'max', 50, 100
		setIfMissing 'offset', 0
		Integer max = params.int('max')
		Integer offset = params.int('offset')
		def term = params.term
        params.max = Math.min(max ?: 10, 100)
		def invoiceInstanceList=invoiceService.invoiceList(params, max, offset)['invoiceInstanceList']
		def total=invoiceService.invoiceList(params, max, offset)['total']
        
		def totalCount = params.term ? invoiceInstanceList.size() : Invoice.count()
		def offsetBase = (offset == 0 ? 50 : (offset + 10))
		def endPos = (offsetBase < totalCount ? offsetBase : totalCount)
		
		def model = [invoiceInstanceList: invoiceInstanceList, totalCount: total, searched: true, term: params.term, startPos: (offset + 1), endPos: endPos, max:max, offset:offset]
		render view: 'list', model: model
    }

    def create() {
        def invoiceInstance = invoiceService.createInvoice(params)
        [invoiceInstance: invoiceInstance, agentInstanceList: agentService.agentList(params)['agentInstanceList']]
    }

    def create_free() {
        params.put("tipo", 1)
        def invoiceInstance = invoiceService.createInvoice(params)
        [invoiceInstance: invoiceInstance, agentInstanceList: agentService.agentList(params)['agentInstanceList']]
    }

    def save() {
		if ((params.reference != "")&&(params.reference != null)){
	        def invoiceInstance = invoiceService.saveInvoice(params)
	        if (invoiceInstance==null) {
	            flash.message = message(code: 'default.error.save', args: [message(code: 'invoice.label')])
	            render(view: "create", model: [invoiceInstance: invoiceInstance])
	        } else {
	            flash.message = message(code: 'default.created.message', args: [message(code: 'invoice.label'), invoiceInstance.name])
	            redirect(action: "edit", id: invoiceInstance.id)
	        }
		}
    }

    def show(Long id) {
        def invoiceInstance=invoiceService.showInvoice(id)
        if (!invoiceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'invoice.label'), invoiceInstance.name])
            redirect(action: "list")
            return
        }
        [invoiceInstance: invoiceInstance]
    }

    def edit(Long id) {
        def invoiceInstance = invoiceService.editInvoice(id)
        if (!invoiceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'invoice.label'), invoiceInstance.name])
            redirect(action: "list")
            return
        }
        if (invoiceInstance.tipo==1){
            redirect(action: "edit_free", id:invoiceInstance.id, invoiceInstance: invoiceInstance, agentInstanceList: agentService.agentList(params)['agentInstanceList'])
        }
        else{
            [invoiceInstance: invoiceInstance, agentInstanceList: agentService.agentList(params)['agentInstanceList']]
        }
    }

    def edit_free(Long id) {
        def invoiceInstance = invoiceService.editInvoice(id)
        if (!invoiceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'invoice.label'), invoiceInstance.name])
            redirect(action: "list")
            return
        }
        [invoiceInstance: invoiceInstance, agentInstanceList: agentService.agentList(params)['agentInstanceList']]
    }

    def update(Long id, Long version) {
		if ((params.reference != "")&&(params.reference != null)){
	        def invoiceInstance = invoiceService.updateInvoice(id, params)
	        ///recalculamos los totales, por si hubo cambios en los order.
	        each {InvoiceOrder in InvoiceOrder.findAllByInvoice(invoiceInstance)
	
	        }
	
	        if (invoiceInstance == null) {
	            render(view: "create", model: [invoiceInstance: invoiceInstance])
	            return
	        }
	        if (version != null) {
	            if (invoiceInstance.version > version) {
	                invoiceInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
	                        [message(code: 'invoice.label', default: 'Cliente')] as Object[],
	                        "Another user has updated this invoice while you were editing")
	                render(view: "edit", model: [invoiceInstance: invoiceInstance])
	                return
	            }
	        }
	        flash.message = message(code: 'default.updated.message', args:  [message(code: 'invoice.label'), [invoiceInstance.reference]])
	        redirect(action: "edit", id: invoiceInstance.id)
		}
		else{
			delete(id)
		}
    }

    def delete(Long id) {
        try {
            String nameOfDeleted = invoiceService.deleteInvoice(id)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'invoice.label'), nameOfDeleted])
            redirect(action: "list")
            return

        } catch (Exception e) {
            flash.message = e.getMessage()
            redirect(action: "list")
            return

        } catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'invoice.label'), id])
            redirect(action: "list")
            return
        }
    }
	
	//print the details of an invoice
	def reportInvoice = {
		System.out.println("Entro por reportInvoice!: " + params.id_invoice);
				
		//Send the invoice id as a parameter to the report.
		params.put('id_invoice', Integer.parseInt(params.id_invoice))
			
		System.out.println("Lanzando report" + params);
		//launch the report
		chain(controller:'jasper', action:'index', params:params)
	}

    //print the details of an invoice
    def reportInvoice_free = {
        System.out.println("Entro por reportInvoice!: " + params.id_invoice);

        //Send the invoice id as a parameter to the report.
        params.put('id_invoice', Integer.parseInt(params.id_invoice))

        System.out.println("Lanzando report" + params);
        //launch the report
        chain(controller:'jasper', action:'index', params:params)
    }
}
