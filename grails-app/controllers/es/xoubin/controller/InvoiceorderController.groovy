package es.xoubin.controller

import es.xoubin.domain.InvoiceOrder
import es.xoubin.domain.Invoice
import org.springframework.dao.DataIntegrityViolationException
import java.text.DecimalFormat;

class InvoiceorderController {

    def _addOrder(){
        InvoiceOrder invoiceOrder=new InvoiceOrder(params)
        invoiceOrder.invoice= Invoice.findById(params.idInvoice)
		def invoice = Invoice.findById(params.idInvoice)
		if (invoice.totalOrderTaxes==null){
			invoice.totalOrderTaxes='0'
		}
		if (invoice.totalWithTaxes==null){
			invoice.totalWithTaxes='0'
		}
        invoice.retention = Double.parseDouble(params.retention)
		def totalOrder = Double.parseDouble(params.total)
		def tax = Invoice.findById(params.idInvoice).tax
		def taxOrder = totalOrder * (tax / 100)

		invoice.totalOrderTaxes = Double.parseDouble(invoice.totalOrderTaxes) + taxOrder
        invoice.totalOrderTaxes = Double.parseDouble(invoice.totalOrderTaxes)
        invoice.totalOrderTaxes = Math.rint(Double.parseDouble(invoice.totalOrderTaxes) * 100)/100;
		invoice.totalWithTaxes = Double.parseDouble(invoice.totalWithTaxes) + totalOrder + taxOrder
        invoice.totalWithTaxes = Math.rint(Double.parseDouble(invoice.totalWithTaxes) * 100)/100;

        def ret = totalOrder * (invoice.retention / 100)
        def total = Double.parseDouble(invoice.totalWithTaxes) - ret
        if (invoice.totalInvoiceRetention==null){
            invoice.totalInvoiceRetention='0'
        }
        def doubleRetention=Double.parseDouble(invoice.totalInvoiceRetention) + ret
        invoice.totalInvoiceRetention = Math.rint(doubleRetention * 100)/100;
        invoice.totalWithTaxes = Math.rint(total * 100)/100;

		invoiceOrder.dateCreated = new Date()

        if (invoiceOrder.save(flush: true)){
            render(contentType: 'text/json') {[
                    'success': "OK",
                    'id': invoiceOrder.id,
                    'idInvoice': params.idInvoice,
                    'invoiceOrderName': invoiceOrder.name,
					'invoiceOrderReference': invoiceOrder.reference,
                    'invoiceOrderTotal': invoiceOrder.total,
                    'invoiceOrderDateCreated': invoiceOrder.dateCreated,
					'totalOrderTaxes': Invoice.findById(params.idInvoice).totalOrderTaxes,
                    'totalInvoiceRetention': invoice.totalInvoiceRetention,
					'total': Invoice.findById(params.idInvoice).totalWithTaxes
            ]}
        }
        else{
            flash.message = message(code: 'invoiceOrder.add.fail')
            redirect(controller: "invoiceorder", action: "list")
        }
    }
	
	def _updateOrder(){
		InvoiceOrder invoiceOrder=InvoiceOrder.findById(params.id)
        Invoice invoice=Invoice.findById(invoiceOrder.invoice.id)

		//actualizamos datos
		invoiceOrder.name=params.name
		invoiceOrder.reference=params.reference
		invoiceOrder.total=params.total
		invoiceOrder.dateCreated=invoiceOrder.dateCreated

		if (invoiceOrder.save(flush: true)){

            invoice.totalOrderTaxes = 0
            invoice.totalWithTaxes = 0
            invoice.totalWithTaxes = 0
            invoice.totalInvoiceRetention = 0
            def totalOrdersNoTaxes = 0

            for (order in InvoiceOrder.findAllByInvoice(invoice)){
                def totalOrder = Double.parseDouble(order.total)
                totalOrdersNoTaxes += totalOrder
                def tax = invoice.tax
                def taxOrder = totalOrder * (tax / 100)

                invoice.totalOrderTaxes = Double.parseDouble(invoice.totalOrderTaxes) + taxOrder
                invoice.totalOrderTaxes = Double.parseDouble(invoice.totalOrderTaxes)
                invoice.totalOrderTaxes = Math.rint(Double.parseDouble(invoice.totalOrderTaxes) * 100)/100;
                invoice.totalWithTaxes = Double.parseDouble(invoice.totalWithTaxes) + totalOrder + taxOrder
                invoice.totalWithTaxes = Math.rint(Double.parseDouble(invoice.totalWithTaxes) * 100)/100;
            }

            def ret = totalOrdersNoTaxes * (invoice.retention / 100)
            def total = Double.parseDouble(invoice.totalWithTaxes) - ret
            if (invoice.totalInvoiceRetention==null){
                invoice.totalInvoiceRetention='0'
            }
            def doubleRetention=Double.parseDouble(invoice.totalInvoiceRetention) + ret
            invoice.totalInvoiceRetention = Math.rint(doubleRetention * 100)/100;
            invoice.totalWithTaxes = Math.rint(total * 100)/100;

			render(contentType: 'text/json') {[
					'success': "OK",
					'id': invoiceOrder.id,
                    'invoiceOrderName': invoiceOrder.name,
					'invoiceOrderReference': invoiceOrder.reference,
                    'invoiceOrderTotal': invoiceOrder.total,
                    'invoiceOrderDateCreated': invoiceOrder.dateCreated,
                    'totalOrderTaxes': invoice.totalOrderTaxes,
                    'totalRetention': invoice.totalInvoiceRetention,
                    'total': invoice.totalWithTaxes
			]}
		}
		else{
			flash.message = message(code: 'invoiceOrder.add.fail')
			redirect(controller: "invoiceorder", action: "list")
		}
	}

    def _removeOrder(){
        InvoiceOrder invoiceOrder=InvoiceOrder.get(params.id)
        Invoice invoice=Invoice.findById(invoiceOrder.invoice.id)

        try {
            invoiceOrder.delete(flush: true)

            invoice.totalOrderTaxes = 0
            invoice.totalWithTaxes = 0
            invoice.totalWithTaxes = 0
            invoice.totalInvoiceRetention = 0
            def totalOrdersNoTaxes = 0

            for (order in InvoiceOrder.findAllByInvoice(invoice)){
                def totalOrder = Double.parseDouble(order.total)
                totalOrdersNoTaxes += totalOrder
                def tax = invoice.tax
                def taxOrder = totalOrder * (tax / 100)

                invoice.totalOrderTaxes = Double.parseDouble(invoice.totalOrderTaxes) + taxOrder
                invoice.totalOrderTaxes = Double.parseDouble(invoice.totalOrderTaxes)
                invoice.totalOrderTaxes = Math.rint(Double.parseDouble(invoice.totalOrderTaxes) * 100)/100;
                invoice.totalWithTaxes = Double.parseDouble(invoice.totalWithTaxes) + totalOrder + taxOrder
                invoice.totalWithTaxes = Math.rint(Double.parseDouble(invoice.totalWithTaxes) * 100)/100;
            }

            if (invoice.tipo==1)
            {
                def ret = totalOrdersNoTaxes * (invoice.retention / 100)
                def total = Double.parseDouble(invoice.totalWithTaxes) - ret
                if (invoice.totalInvoiceRetention==null){
                    invoice.totalInvoiceRetention='0'
                }
                def doubleRetention=Double.parseDouble(invoice.totalInvoiceRetention) + ret
                invoice.totalInvoiceRetention = Math.rint(doubleRetention * 100)/100;
                invoice.totalWithTaxes = Math.rint(total * 100)/100;
            }

            invoice.save(flush: true)

            render(contentType: 'text/json') {[
                    'success': "OK",
                    'id': params.id
            ]}

        }

        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'invoiceOrder.add.fail')
            redirect(controller: "invoiceorder", action: "list")
        }
    }
}
