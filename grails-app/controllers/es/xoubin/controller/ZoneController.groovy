package es.xoubin.controller

import es.xoubin.domain.Zone
import es.xoubin.domain.City
import org.springframework.dao.DataIntegrityViolationException

class ZoneController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
		
		def lista=City.executeQuery("select id, name from City as c")
		def listaCitiesMap=""
		def j=0;
		for (i in lista){
			if (j==0){
				listaCitiesMap = listaCitiesMap + "{'id':" + i[0] + ", 'selected': 'true', 'name' :'" + i[1] + "'},"
			}
			else{
				listaCitiesMap = listaCitiesMap + "{'id':" + i[0] + ", 'name' :'" + i[1] + "'},"
			}
			j=j+1
		}
		if (listaCitiesMap.length()>0){
			listaCitiesMap=listaCitiesMap.substring(0, listaCitiesMap.length()-1)
		}
		listaCitiesMap=listaCitiesMap.replaceAll("'", '"')
		
        [zoneInstanceList: Zone.findAll(sort:"name", order:"asc"), zoneInstanceTotal: Zone.count(), cityInstanceList:City.list(), cityInstanceListMap:listaCitiesMap]
    }

    def create() {
        return [zoneInstance: new Zone(params)]
    }

    def _addZone(){
		def p = params
        Zone zone=new Zone(params)
		zone.city= City.findById(p.idCity)
        if (zone.save(flush: true)){
            render(contentType: 'text/json') {[
                    'success': "OK",
                    'id': zone.id,
					'zoneCity': params.city,
                    'zoneName': params.name
            ]}
        }
        else{
            flash.message = message(code: 'zone.choose.message')
            redirect(controller: "zone", action: "list")
        }
		
    }

    def _removeZone(){
        Zone zone=Zone.get(params.id)
        try {
            zone.delete(flush: true)
            render(contentType: 'text/json') {[
                    'success': "OK",
                    'id': params.id
            ]}
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'zone.choose.message')
            redirect(controller: "zone", action: "list")
        }
    }

    def save() {
        def zoneInstance = new Zone(params)

        if (!zoneInstance.save(flush: true)) {
            render(view: "create", model: [zoneInstance: zoneInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'zone.label'), zoneInstance.id])
        redirect(action: "create")
    }

    def edit(Long id) {
        def zoneInstance = Zone.get(id)
        if (!zoneInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'zone.label'), id])
            redirect(action: "list")
            return
        }
        [zoneInstance: zoneInstance, _id: id]
    }

    def update(Long id, Long version) {
        def zoneInstance = Zone.get(id)
        if (!zoneInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'zone.label'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (zoneInstance.version > version) {
                zoneInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'zone.label', default: 'Atributo')] as Object[],
                        "Another user has updated this zone while you were editing")
                render(view: "edit", model: [zoneInstance: zoneInstance])
                return
            }
        }

        zoneInstance.properties = params

        if (!zoneInstance.save(flush: true)) {
            render(view: "edit", model: [zoneInstance: zoneInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'zone.label'), zoneInstance.id])
        redirect(action: "edit", id: zoneInstance.id)
    }

    def delete(Long id) {
        def zoneInstance = Zone.get(id)
        if (!zoneInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'zone.label'), id])
            redirect(action: "create", params: [idProperty: params.idProperty.id])
            return
        }

        try {
            zoneInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'zone.label'), id])
            redirect(action: "create", params: [idProperty: params.idProperty.id])
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'zone.label'), id])
            redirect(action: "create", params: [idProperty: params.idProperty.id])
        }
    }

    def _getZonesByCity(Long idCity){
        def list=Zone.executeQuery("select id, name from Zone as z where z.city.id = ? order by name", [idCity])
        render(contentType: 'text/json') {[
                'success': "OK",
                'list':list
        ]}
    }
}
