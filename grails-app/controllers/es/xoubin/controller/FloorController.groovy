package es.xoubin.controller

import es.xoubin.domain.Floor
import es.xoubin.domain.Property
import org.springframework.dao.DataIntegrityViolationException

class FloorController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [floorInstanceList: Floor.list(params), floorInstanceTotal: Floor.count()]
    }

    def create() {
        if (params.idProperty) {
            return [floorInstance: new Floor(params), propertyInstance: Property.findById(params.idProperty)]
        } else {
            flash.message = message(code: 'floor.property.choose.message')
            redirect(controller: "floor", action: "list")
        }
    }

    def _addFloor(){
        Floor floor=new Floor(params)
        floor.property= Property.findById(params.idProperty)
        if (floor.save(flush: true)){
            render(contentType: 'text/json') {[
                    'success': "OK",
                    'id': floor.id,
                    'idProperty': params.idProperty,
                    'floorName': params.name,
                    'floorDescription': params.description
            ]}
        }
        else{
            flash.message = message(code: 'floor.save.error')
            redirect(controller: "property", action: "list")
        }
    }
	
	def  _updateFloor(){
		Floor floor=Floor.get(params.id)
		floor.name=params.name
		floor.description=params.description
		if (floor.save(flush: true)){
			render(contentType: 'text/json') {[
					'success': "OK",
					'id': floor.id,
					'idProperty': params.idProperty,
					'floorName': params.name,
					'floorDescription': params.description
			]}
		}
		else{
			flash.message = message(code: 'floor.save.error')
			redirect(controller: "property", action: "list")
		}
	}

    def _removeFloor(){
        Floor floor=Floor.get(params.id)
        try {
            floor.delete(flush: true)
            render(contentType: 'text/json') {[
                    'success': "OK",
                    'id': params.id
            ]}
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'floor.delete.error')
            redirect(controller: "property", action: "list")
        }
    }

    def save() {
        def floorInstance = new Floor(params)

        def personInstance = Property.findById(params.property.id)
        floorInstance.person = personInstance
        if (!floorInstance.save(flush: true)) {
            render(view: "create", model: [floorInstance: floorInstance, propertyInstance: Property.findById(params.property.id)])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'floor.label'), floorInstance.id])
        redirect(action: "create", params: [idProperty: params.property.id])
    }

    def edit(Long id) {
        def floorInstance = Floor.get(id)
        if (!floorInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'floor.label'), id])
            redirect(action: "list")
            return
        }
        [floorInstance: floorInstance, propertyInstance: Property.findById(params.idProperty), _id: id]
    }

    def update(Long id, Long version) {
        def floorInstance = Floor.get(id)
        if (!floorInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'floor.label'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (floorInstance.version > version) {
                floorInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'floor.label', default: 'Telefono')] as Object[],
                        "Another user has updated this floor while you were editing")
                render(view: "edit", model: [floorInstance: floorInstance])
                return
            }
        }

        floorInstance.properties = params

        def personInstance = Property.findById(params.property.id)
        floorInstance.person = personInstance
        if (!floorInstance.save(flush: true)) {
            render(view: "edit", model: [floorInstance: floorInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'floor.label'), floorInstance.id])
        redirect(action: "edit", id: floorInstance.id, params: [idProperty: params.property.id])
    }

    def delete(Long id) {
        def floorInstance = Floor.get(id)
        if (!floorInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'floor.label'), id])
            redirect(action: "create", params: [idProperty: params.property.id])
            return
        }

        try {
            floorInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'floor.label'), id])
            redirect(action: "create", params: [idProperty: params.property.id])
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'floor.label'), id])
            redirect(action: "create", params: [idProperty: params.property.id])
        }
    }
}
