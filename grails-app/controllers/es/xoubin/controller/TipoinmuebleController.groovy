package es.xoubin.controller

import es.xoubin.domain.Tipo_Inmueble
import org.springframework.dao.DataIntegrityViolationException

class TipoinmuebleController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [tipoInstanceList: Tipo_Inmueble.findAll(sort:"name", order:"asc"), tipoInstanceTotal: Tipo_Inmueble.count()]
    }

    def create() {
        return [tipoInstance: new Tipo_Inmueble(params)]
    }

    def _addTipo(){
        Tipo_Inmueble tipo=new Tipo_Inmueble(params)
        if (tipo.save(flush: true)){
            render(contentType: 'text/json') {[
                    'success': "OK",
                    'id': tipo.id,
                    'tipoInmuebleName': params.name
            ]}
        }
        else{
            flash.message = message(code: 'tipo.choose.message')
            redirect(controller: "tipoinmueble", action: "list")
        }
    }

    def _removeTipo(){
        Tipo_Inmueble tipo=Tipo_Inmueble.get(params.id)
        try {
            tipo.delete(flush: true)
            render(contentType: 'text/json') {[
                    'success': "OK",
                    'id': params.id
            ]}
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'tipo.choose.message')
            redirect(controller: "tipoinmueble", action: "list")
        }
    }

    def save() {
        def tipoInstance = new Tipo_Inmueble(params)

        if (!tipoInstance.save(flush: true)) {
            render(view: "create", model: [tipoInstance: tipoInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'tipo.label'), tipoInstance.id])
        redirect(action: "create")
    }

    def edit(Long id) {
        def tipoInstance = Tipo_Inmueble.get(id)
        if (!tipoInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tipo.label'), id])
            redirect(action: "list")
            return
        }
        [tipoInstance: tipoInstance, _id: id]
    }

    def update(Long id, Long version) {
        def tipoInstance = Tipo_Inmueble.get(id)
        if (!tipoInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tipo.label'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (tipoInstance.version > version) {
                tipoInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'tipo.label', default: 'Atributo')] as Object[],
                        "Another user has updated this tipo while you were editing")
                render(view: "edit", model: [tipoInstance: tipoInstance])
                return
            }
        }

        tipoInstance.properties = params

        if (!tipoInstance.save(flush: true)) {
            render(view: "edit", model: [tipoInstance: tipoInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'tipo.label'), tipoInstance.id])
        redirect(action: "edit", id: tipoInstance.id)
    }

    def delete(Long id) {
        def tipoInstance = Tipo_Inmueble.get(id)
        if (!tipoInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tipo.label'), id])
            redirect(action: "create", params: [idProperty: params.idProperty.id])
            return
        }

        try {
            tipoInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'tipo.label'), id])
            redirect(action: "create", params: [idProperty: params.idProperty.id])
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'tipo.label'), id])
            redirect(action: "create", params: [idProperty: params.idProperty.id])
        }
    }
}
