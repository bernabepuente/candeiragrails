package es.xoubin.controller

import es.xoubin.domain.City
import es.xoubin.domain.Customer
import es.xoubin.domain.Property
import es.xoubin.domain.Search
import es.xoubin.domain.Tipo_Inmueble
import es.xoubin.domain.Zone
import grails.plugins.springsecurity.ui.AbstractS2UiController;

import org.springframework.dao.DataIntegrityViolationException

class CustomerController extends AbstractS2UiController{

    def customerService
	def propertyService
	def agentService
	def tipo

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list = {
		setIfMissing 'max', 50, 100
		setIfMissing 'offset', 0
		Integer max = params.int('max')
		Integer offset = params.int('offset')
		def term = params.term
        params.max = Math.min(max ?: 10, 100)
		def user=springSecurityService.principal.id
		
		def customerInstanceList
		def total
		
		Property property
		if (params.idProperty){
			property=Property.findById(params.idProperty.toInteger())
		}
		if (property){
			customerInstanceList=customerService.searchFromProperty(property, params, max, offset)['customerInstanceList']
			total=customerService.searchFromProperty(property, params, max, offset)['total']
		}
		else{
			customerInstanceList=customerService.customerList(params, max, offset)['customerInstanceList']
			total=customerService.customerList(params, max, offset)['total']
		}
        
		def totalCount = params.term ? customerInstanceList.size() : Customer.count()
		def offsetBase = (offset == 0 ? 50 : (offset + 10))
		def endPos = (offsetBase < totalCount ? offsetBase : totalCount)
		
		def model = [customerInstanceList: customerInstanceList, totalCount: total, agentId: user, searched: true, term: params.term, startPos: (offset + 1), endPos: endPos, max:max, offset:offset]
		render view: 'list', model: model
    }

    def create() {
        def customerInstance = customerService.createCustomer(params)
        [customerInstance: customerInstance, discriminator:"person", agentInstanceList: agentService.agentList(params)['agentInstanceList']]
    }

    def save() {
        def customerInstance = customerService.saveCustomer(params)
        if (customerInstance==null) {
            flash.message = message(code: 'default.error.save', args: [message(code: 'customer.label')])
            render(view: "create", model: [customerInstance: customerInstance])
        } else {
            flash.message = message(code: 'default.created.message', args: [message(code: 'customer.label'), customerInstance.name])
            redirect(action: "edit", id: customerInstance.id)
        }
    }

    def show(Long id) {
        def customerInstance=customerService.showCustomer(id)
        if (!customerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'customer.label'), customerInstance.name])
            redirect(action: "list")
            return
        }
        [customerInstance: customerInstance]
    }

    def edit(Long id) {
        def customerInstance = customerService.editCustomer(id)
        if (!customerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'customer.label'), customerInstance.name])
            redirect(action: "list")
            return
        }
		def lista=Tipo_Inmueble.executeQuery("select id, name from Tipo_Inmueble as t order by name")
		def listaTiposMap=""
		def j=0;
		for (i in lista){
			if (j==0){
				listaTiposMap = listaTiposMap + "{'id':" + i[0] + ", 'selected': 'true', 'name' :'" + i[1] + "'},"
			}
			else{
				listaTiposMap = listaTiposMap + "{'id':" + i[0] + ", 'name' :'" + i[1] + "'},"
			}
			j=j+1
		}
		listaTiposMap=listaTiposMap.substring(0, listaTiposMap.length()-1)
		listaTiposMap=listaTiposMap.replaceAll("'", '"')

        def lista_city=City.executeQuery("select id, name from City as c order by name")
        def listCitiesMap=""
        j=0;
        for (i in lista_city){
            if (j==0){
                listCitiesMap = listCitiesMap + "{'id':" + i[0] + ", 'selected': 'true', 'name' :'" + i[1] + "'},"
            }
            else{
                listCitiesMap = listCitiesMap + "{'id':" + i[0] + ", 'name' :'" + i[1] + "'},"
            }
            j=j+1
        }
        listCitiesMap=listCitiesMap.substring(0, listCitiesMap.length()-1)
        listCitiesMap=listCitiesMap.replaceAll("'", '"')

        def lista_zone=Zone.executeQuery("select id, name from Zone as z order by name")
        def listZonesMap=""
        j=0;
        for (i in lista_zone){
            if (j==0){
                listZonesMap = listZonesMap + "{'id':" + i[0] + ", 'selected': 'true', 'name' :'" + i[1] + "'},"
            }
            else{
                listZonesMap = listZonesMap + "{'id':" + i[0] + ", 'name' :'" + i[1] + "'},"
            }
            j=j+1
        }
        listZonesMap=listZonesMap.substring(0, listZonesMap.length()-1)
        listZonesMap=listZonesMap.replaceAll("'", '"')

        [customerInstance: customerInstance, entityInstance: customerInstance, discriminator:"customer", cityInstanceList: City.listOrderByName(), zoneInstanceList: Zone.listOrderByName(), agentInstanceList: agentService.agentList(params)['agentInstanceList'], tipoInstanceList:Tipo_Inmueble.listOrderByName(), tipoInstanceListMap:listaTiposMap, cityInstanceListMap: listCitiesMap, zoneInstanceListMap: listZonesMap]
    }

    def update(Long id, Long version) {
        def customerInstance = customerService.updateCustomer(id, params)
        if (customerInstance == null) {
            render(view: "create", model: [customerInstance: customerInstance])
            return
        }
        if (version != null) {
            if (customerInstance.version > version) {
                customerInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'customer.label', default: 'Cliente')] as Object[],
                        "Another user has updated this customer while you were editing")
                render(view: "edit", model: [customerInstance: customerInstance])
                return
            }
        }
        flash.message = message(code: 'default.updated.message', args:  [message(code: 'customer.label'), [customerInstance.referencia]])
        redirect(action: "edit", id: customerInstance.id)
    }

    def delete(Long id) {
        try {
            String nameOfDeleted = customerService.deleteCustomer(id)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'customer.label'), nameOfDeleted])
            redirect(action: "list")
            return

        } catch (Exception e) {
            flash.message = e.getMessage()
            redirect(action: "list")
            return

        } catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'customer.label'), id])
            redirect(action: "list")
            return
        }
    }
	
	def _searchFromCustomer() {
		setIfMissing 'max', 50, 100
		setIfMissing 'offset', 0
		Integer max = params.int('max')
		Integer offset = params.int('offset')
		params.max = Math.min(max ?: 10, 100)
        def roles = principal.authorities*.authority
		def idS=params.id
		Search search = Search.get(idS)
		//def propertyInstanceList=propertyService.searchFromCustomer(search, params, max, offset)['propertyInstanceList']
		//def total=propertyService.searchFromCustomer(search, params, max, offset)['total']
		
		//def totalCount = params.term ? propertyService.size() : Property.count()
		//def offsetBase = (offset == 0 ? 50 : (offset + 10))
		//def endPos = (offsetBase < totalCount ? offsetBase : totalCount)
		def model = [idSearch:idS]

        Boolean ramallosa=false
        for (rol in roles){
            if (rol.equals("ROLE_RAMALLOSA")){
                ramallosa=true
            }
        }

        if (ramallosa==true){
            redirect(controller:"property", action: "list_venta", params:model)
        }
        else{
            redirect(controller:"property", action: "list", params:model)
        }


	}
}
