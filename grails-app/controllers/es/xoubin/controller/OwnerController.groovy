package es.xoubin.controller

import es.xoubin.domain.Owner
import es.xoubin.domain.Property
import org.springframework.dao.DataIntegrityViolationException

class OwnerController {

    def ownerService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [ownerInstanceList:ownerService.ownerList(params)['ownerInstanceList'], ownerInstanceTotal:ownerService.ownerList(params)['ownerInstanceTotal']]
    }

    def create() {
        def ownerInstance = ownerService.createOwner(params)
        [ownerInstance: ownerInstance]
    }

    def _addOwner(){
        Owner owner=new Owner(params)
        owner.property= Property.findById(params.idProperty)
        if (owner.save(flush: true)){
            render(contentType: 'text/json') {[
                    'success': "OK",
                    'id': owner.id,
                    'idProperty': params.idProperty,
                    'ownerCompleteName': params.completeName,
                    'ownerDni': params.dni,
                    'ownerAddress': params.address,
                    'ownerPhone1': params.phone1,
                    'ownerPhone2': params.phone2,
                    'ownerMail': params.mail,
                    'ownerEstado': params.estado,
                    'ownerRegimen': params.regimen,
                    'ownerHorario': params.horario_contacto,
                    'ownerSector': params.sector,
                    'ownerObservaciones': params.observaciones,
                    'ownerIdioma': params.idioma,
                    'ownerTrato': params.trato,
                    'ownerCuenta': params.cuenta
            ]}
        }
        else{
            flash.message = message(code: 'owner.add.fail')
            redirect(controller: "owner", action: "list")
        }
    }
	
	def _updateOwner(){
		Owner owner=Owner.get(params.id)
		owner.completeName= params.completeName
		owner.dni= params.dni
		owner.address= params.address
		owner.phone1= params.phone1
		owner.phone2= params.phone2
		owner.mail= params.mail
		owner.estado= params.estado
		owner.regimen= params.regimen
		owner.horario_contacto= params.horario_contacto
		owner.sector= params.sector
		owner.observaciones= params.observaciones
		owner.idioma= params.idioma
		owner.trato= params.trato
        owner.cuenta= params.cuenta
		if (owner.save(flush: true)){
			render(contentType: 'text/json') {[
					'success': "OK",
					'id': owner.id,
					'ownerCompleteName': params.completeName,
					'ownerDni': params.dni,
					'ownerAddress': params.address,
					'ownerPhone1': params.phone1,
					'ownerPhone2': params.phone2,
					'ownerMail': params.mail,
					'ownerEstado': params.estado,
					'ownerRegimen': params.regimen,
					'ownerHorario': params.horario_contacto,
					'ownerSector': params.sector,
					'ownerObservaciones': params.observaciones,
					'ownerIdioma': params.idioma,
					'ownerTrato': params.trato,
                    'ownerCuenta': params.cuenta
			]}
		}
		else{
			flash.message = message(code: 'owner.add.fail')
			redirect(controller: "owner", action: "list")
		}
	}

    def _removeOwner(){
        Owner owner=Owner.get(params.id)
        try {
            owner.delete(flush: true)
            render(contentType: 'text/json') {[
                    'success': "OK",
                    'id': params.id
            ]}
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'owner.add.fail')
            redirect(controller: "owner", action: "list")
        }
    }

    def save() {
        def ownerInstance = ownerService.createOwner(params)
        ownerInstance = ownerService.saveOwner(params)
        if (ownerInstance==null) {
            flash.message = message(code: 'default.error.save', args: [message(code: 'owner.label')])
            render(view: "_create", model: [ownerInstance: ownerInstance])
        } else {
            flash.message = message(code: 'default.created.message', args: [message(code: 'owner.label'), ownerInstance.name])
            redirect(action: "edit", id: ownerInstance.id)
        }
    }

    def show(Long id) {
        def ownerInstance=ownerService.showOwner(id)
        if (!ownerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'owner.label'), ownerInstance.name])
            redirect(action: "list")
            return
        }
        [ownerInstance: ownerInstance]
    }

    def edit(Long id) {
        def ownerInstance = ownerService.editOwner(id)
        if (!ownerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'owner.label'), ownerInstance.name])
            redirect(action: "list")
            return
        }
        def propertyInstanceList = propertyService.ListByPerson(ownerInstance)
        [ownerInstance: ownerInstance, propertyInstanceList:propertyInstanceList.peropertyInstanceList, propertyInstanceTotal:propertyInstanceList.propertyInstanceTotal]
    }

    def update(Long id, Long version) {
        def ownerInstance = ownerService.updateOwner(id, version, params)
        if (ownerInstance == null) {
            render(view: "_create", model: [ownerInstance: ownerInstance])
            return
        }
        if (version != null) {
            if (ownerInstance.version > version) {
                ownerInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'owner.label', default: 'Cliente')] as Object[],
                        "Another user has updated this owner while you were editing")
                render(view: "edit", model: [ownerInstance: ownerInstance])
                return
            }
        }
        flash.message = message(code: 'default.updated.message', args:  [message(code: 'owner.label'), [ownerInstance.name]])
        redirect(action: "edit", id: ownerInstance.id)
    }

    def delete(Long id) {
        try {
            String nameOfDeleted = ownerService.deleteOwner(id)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'owner.label'), nameOfDeleted])
            redirect(action: "list")
            return

        } catch (Exception e) {
            flash.message = e.getMessage()
            redirect(action: "list")
            return

        } catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'owner.label'), id])
            redirect(action: "list")
            return
        }
    }
}
