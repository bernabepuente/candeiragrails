<%@ page import="es.xoubin.domain.Agent" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
        <g:javascript library="validator" />
        <g:set var="entityName" value="${message(code: 'agent.label', default: 'Agent')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<g:render template="/layouts/navbar" />
		<g:render template="/layouts/header" />
		<div class="container" style="width: 100%">
			<div class="row-fluid">				
				<div class="span12">
					<div id="create-agent" class="content scaffold-create" role="main" style="width: 100%; margin:0 auto 0 auto;">
						<div class="page-header">
							<h2><g:message code="agent.create.label" /></h2>
						</div>
						<g:if test="${flash.message}">
							<div class="alert alert-success message" role="status">
								<button type="button" class="close" data-dismiss="alert">×</button>
								${flash.message}
							</div>
						</g:if>

                        <g:form name='userForm' class="button-style form-horizontal" action="save" >
							<fieldset>
								<g:render template="form"/>
							</fieldset>
							<br>
							<fieldset>
								<g:submitButton name="create" class="save btn btn-primary" value="${message(code: 'default.button.create.label', default: 'Create')}" />
							</fieldset>
						</g:form>
					</div>
				</div> <!-- span12 -->				
			</div> <!-- row-fluid -->
		</div> <!-- container-fluid -->
		<g:render template="/layouts/footer" />
	</body>
</html>
