<div class="form-group has-warnin">
	<label for="name" class="col-lg-2 control-label"><g:message
			code="agent.name.label" default="Name" /></label>
	<div class="col-lg-10">
		<g:textField name="name" class="form-control"
			value="${agentInstance?.name}" />
	</div>
</div>
<div class="form-group">
	<label for="surname" class="col-lg-2 control-label"><g:message
			code="agent.surname.label" default="Surname" /></label>
	<div class="col-lg-10">
		<g:textField name="surname" class="form-control"
			value="${agentInstance?.surname}" />
	</div>
</div>
<div class="form-group">
	<label for="nif" class="col-lg-2 control-label"><g:message
			code="agent.nif.label" default="Nif" /></label>
	<div class="col-lg-10">
		<g:textField name="nif" class="form-control"
			value="${agentInstance?.nif}" />
	</div>
</div>
<div class="form-group">
	<label for="mail" class="col-lg-2 control-label"><g:message
			code="agent.mail.label" default="Mail" /></label>
	<div class="col-lg-10">
		<g:textField name="mail" class="form-control"
			value="${agentInstance?.mail}" />
	</div>
</div>
<div class="form-group">
	<label for="username" class="col-lg-2 control-label"><g:message
			code="agent.username.label" default="Username" /></label>
	<div class="col-lg-10">
		<g:textField name="username" class="form-control"
			value="${agentInstance?.username}" />
	</div>
</div>
<jq:jquery>
    $("#userForm").validate({
        rules: {
            username: "required",
            name: "required"
        },
        messages: {
            username: "${message(code: 'person.property.required')}",
			name: "${message(code: 'person.property.required')}"
		}
	});
</jq:jquery>
