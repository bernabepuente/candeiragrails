<%@ page import="es.xoubin.domain.Agent" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
        <g:javascript library="validator" />
        <g:set var="entityName" value="${message(code: 'agent.label', default: 'Agent')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<g:render template="/layouts/navbar" />
		<g:render template="/layouts/header" />
		<div class="container" style="width: 100%">
			<div class="row-fluid">				
				<div class="span12">
					<div id="create-agent" class="content scaffold-create" role="main" style="width: 100%; margin:0 auto 0 auto;">
						<div class="page-header">
							<h2><g:message code="agent.edit.label" /></h2>
						</div>
						<g:if test="${flash.message}">
							<div class="alert alert-success message" role="status">
								<button type="button" class="close" data-dismiss="alert">×</button>
								${flash.message}
							</div>
						</g:if>
                        <div id="axMainMsg" class="alert hide" role="status">
                        </div>
						<g:hasErrors bean="${agentInstance}">
							<div class="alert alert-error">
								<ul class="errors" role="alert">
									<g:eachError bean="${agentInstance}" var="error">
										<li>
											<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>
											<g:message error="${error}"/>
										</li>
									</g:eachError>
								</ul>
							</div>
						</g:hasErrors>
						
						<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
				        	<li class="active">
				        		<a href="#datos-basicos" data-toggle="tab">
				        			<g:message code="agent.tab.datos" />
				        		</a>
				        	</li>
				        	<li>
				        		<a href="#properties" data-toggle="tab">
									<g:message code="agent.tab.properties" />
									<span class="badge">
										${propertyInstanceTotal}
									</span>	  							
								</a>
							</li>
                            <li>
                                <a href="#addresses" data-toggle="tab">
                                    <g:message code="agent.tab.addresses" />
                                    <span class="badge">
                                        ${agentInstance?.addresses?.size()}
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#phones" data-toggle="tab">
                                    <g:message code="agent.tab.phones" />
                                    <span class="badge">
                                        ${agentInstance?.phones?.size()}
                                    </span>
                                </a>
                            </li>
						</ul>
						<br>
						<g:form class="form-horizontal" action="save" name='agentForm'>
						<g:hiddenField name="id" id="idAgent"
									value="${agentInstance?.id}" />
							<fieldset>
							<div id="my-tab-content" class="tab-content">
		        				<div class="tab-pane active" id="datos-basicos">
									<g:render template="form"/>
								</div>
								<div class="tab-pane" id="properties">
									<table class="table table-stripped">
										<thead>
											<tr>
												<g:sortableColumn property="categoria"
													title="${message(code: 'properties.categoria.label', default: 'Referencia')}" />
											</tr>
										</thead>
										<tbody>
										<g:if test="${propertiesInstanceTotal>0}">
											<g:each in="${propertiesInstanceList}" status="i"
												var="propertiesInstance">
												<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
													<td><g:link action="editInmueble" id="${propertiesInstance.id}">
															${fieldValue(bean: propertiesInstance, field: "Referencia")}
														</g:link>
                                                    </td>
												</tr>
											</g:each>
										</g:if>
										</tbody>
									</table>
								</div>
                                <div class="tab-pane" id="addresses">
                                    <g:render template="/address/formEditAddresses" />
                                </div>
                                <div class="tab-pane" id="phones">
                                    <g:render template="/phone/formEditPhones" />
                                </div>
							</div>
							</fieldset>
							<br>
							<fieldset>
								<g:actionSubmit id="btnUpdate" class="save btn btn-primary"
										action="update"
										value="${message(code: 'default.button.update.label', default: 'Update')}" />
									<g:actionSubmit id="btnDelete" class="btn btn-danger"
										action="delete"
										value="${message(code: 'default.button.delete.label', default: 'Delete')}"
										formnovalidate=""
										onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
							</fieldset>
						</g:form>
					</div>
				</div> <!-- span12 -->				
			</div> <!-- row-fluid -->
		</div> <!-- container-fluid -->
		<g:render template="/layouts/footer" />
		<jq:jquery>
			$(document).ready(function ($) {
			        $('#tabs').tab();
			    });
		</jq:jquery>
	</body>
</html>
