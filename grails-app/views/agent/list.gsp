<%@ page import="es.xoubin.domain.Agent"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'agent.label', default: 'Agent')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<g:render template="/layouts/navbar" />
	<g:render template="/layouts/header" />
	<div class="container" style="width: 100%">
		<div class="row-fluid">
			<div class="span12">
				<div id="list-agente" class="content scaffold-list" role="main" style="width: 100%; margin:0 auto 0 auto;">
					<div class="page-header">
						<h2>
							<g:message code="agent.list.label" />
						</h2>
					</div>
					<div>
						<form action="list" class="form-horizontal">
							<g:field class="well" type="text" name="nameBusqueda" />
							<g:submitButton class="btn btn-info" name="submit" value="Buscar" />
						</form>
					</div>
					<g:if test="${flash.message}">
						<div class="alert alert-success message" role="status">
							<button type="button" class="close" data-dismiss="alert">×</button>
							${flash.message}
						</div>
					</g:if>
					<table class="table table-stripped">
						<thead>
							<tr>
								<g:sortableColumn property="name"
									title="${message(code: 'agent.name.label', default: 'Name')}" />
								<g:sortableColumn property="surname"
									title="${message(code: 'agent.surname.label', default: 'Surname')}" />
								<g:sortableColumn property="nif"
									title="${message(code: 'agent.nif.label', default: 'Nif')}" />
								<g:sortableColumn property="mail"
									title="${message(code: 'agent.mail.label', default: 'Mail')}" />
							</tr>
						</thead>
						<tbody>
							<g:each in="${agentInstanceList}" status="i"
								var="agentInstance">
								<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
									<td><g:link action="edit" id="${agentInstance.id}">
											${fieldValue(bean: agentInstance, field: "name")}
										</g:link></td>
									<td>
										${fieldValue(bean: agentInstance, field: "surname")}
									</td>
									<td>
										${fieldValue(bean: agentInstance, field: "nif")}
									</td>
									<td>
										${fieldValue(bean: agentInstance, field: "mail")}
									</td>
								</tr>
							</g:each>
						</tbody>
					</table>
					<div class="pagination">
						<g:paginate total="${agentInstanceTotal}" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<g:render template="/layouts/footer" />
</body>
</html>
