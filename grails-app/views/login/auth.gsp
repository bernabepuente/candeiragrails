<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="bootstrap" />
		<style>
			form {
				margin-top: 250px;
			}
		</style>
	</head>
	<body>		
		<div class="container">
			<div class="row">
				<div class="col-lg-9">
					<form action='${postUrl}' method='POST' id='loginForm' class="col-lg-offset-4 form-horizontal" autocomplete='off'>
						<g:if test="${flash.message}">
							<div class="alert alert-info" role="status">
								<button type="button" class="close" data-dismiss="alert">×</button>
								${flash.message}
								${flash.clear()}
							</div>
						</g:if>
						<div class="alert alert-success" style="background-color:#B0BED9">
							<div class="form-group">
								<label for="username" class="col-lg-2 control-label">
									<g:message code="springSecurity.login.username.label" />
								</label>
								<div class="col-lg-8 pull-right">
									<input 	type='text' class='form-control text_' name='j_username' id='username'
											placeholder="<g:message code="springSecurity.login.username.label" />">
								</div>
							</div>
		
							<div class="form-group">
								<label for="password" class="col-lg-2 control-label">
									<g:message code="springSecurity.login.password.label" />
								</label>
								<div class="col-lg-8 pull-right">
									<input 	type="password" class='form-control text_' name='j_password' id='password'
											placeholder="<g:message code="springSecurity.login.password.label" />">
								</div>
							</div>
		
							<div id="remember_me_holder" class="form-group">
								<div class="col-lg-offset-4 col-lg-8">
									<div class="checkbox">
										<label for='remember_me'>
											<input 	type='checkbox' class='chk' name='${rememberMeParameter}' id='remember_me'
													<g:if test='${hasCookie}'>checked='checked'</g:if> />
											<g:message code="springSecurity.login.remember.me.label" />
										</label>
									</div>
								</div>
							</div>
		
							<div class="form-group">
								<div class="col-lg-offset-4 col-lg-8">
									<input type='submit' id="submit" class="btn btn-default pull-right"
											value='${message(code: "springSecurity.login.button")}' />
								</div>
							</div>
						</div>
					</form>
				</div><!-- ./col-lg-12 -->
			</div><!-- ./row -->
		</div><!-- ./container -->
		<script type='text/javascript'>
		<!--
			(function() {
				document.forms['loginForm'].elements['j_username'].focus();
			})();
		// -->
		</script>
	</body>
</html>