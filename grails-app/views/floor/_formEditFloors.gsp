<g:javascript library="mustache" />
<div class="row-fluid">
    <div class="span12">
        <div id="list-servicio" class="content scaffold-list" role="main">
            <button type="button"
                    id="btnRenderRowFloor"
                    data-button='{"idProperty": "${propertyInstance?.id}"}'
                    class="btn btn-success btnAddFloor">
                <span class="glyphicon glyphicon-plus"></span>
            </button>
            <table class="table table-stripped">
                <thead>
                <tr>
                    <g:sortableColumn property="nombre" title="${message(code: 'floor.name.label')}" />
                    <g:sortableColumn property="description" title="${message(code: 'floor.description.label')}" />
                    <th />
                </tr>
                </thead>
                <tbody id="floors_list">
                <g:each in="${propertyInstance?.floors}" status="i" var="floorInstance">
                    <tr id="floorRowSelected_${floorInstance.id}">
                        <td><g:textField name="txtName_${floorInstance.id}" id="txtName_${floorInstance.id}" class="form-control"
                     		value="${floorInstance?.name}" /></td>
                        <td><g:textField name="txtDescription_${floorInstance.id}" id="txtDescription_${floorInstance.id}" class="form-control"
                     		value="${floorInstance?.description}" /></td>
                   		<td>
                        	<button type="button"
                                    data-button='{"id":  ${floorInstance.id}}'
                                    class="btn btn-success btnUpdateFloor">
                                <span class="glyphicon glyphicon-check"></span>
                            </button>
                        </td>
                        <td>
                            <button type="button"
                                    data-button='{"id":  ${floorInstance.id}}'
                                    class="btn btn-danger btnRemoveFloor">
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>

        </div> <!-- id=list... -->
    </div> <!-- span12 -->
</div>
<jq:jquery>
    var template;
	var templateFilePath = '<g:resource dir="templates" file="floors-row.htm" />';

	$.get(templateFilePath, function(templates) {
		template = $(templates);
		//console.log("Plantillas cargadas");
	});

	function renderFloorRow(){
        var buttonData = $.parseJSON($(this).attr('data-button'));
        var t = template.filter('#floor-row-available').html();
        $('#floors_list').append(Mustache.render(t, buttonData));
	}

    function addFloor() {
		var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/floor/_addFloor",
			dataType: 'json',
			data: {
				idProperty: buttonData.idProperty,
				name: $('#txtName__' + buttonData.idProperty).val(),
				description: $('#txtDescription__' + buttonData.idProperty).val()
			},
			success: function(data) {
				if (data.success == "OK") {
				    $("#floorRowAvailable_" + data.idProperty).detach();
					var t = template.filter('#floor-row-selected').html();
					$('#floors_list').append(Mustache.render(t, data));
				} else {
					if (data.success == "FAIL") {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}
	
	function updateFloor() {
		var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/floor/_updateFloor",
			dataType: 'json',
			data: {
				id: buttonData.id,
				name: $('#txtName_' + buttonData.id).val(),
				description: $('#txtDescription_' + buttonData.id).val()
			},
			success: function(data) {
				if (data.success == "OK") {
					alert('Datos guardados.');
				    
				} else {
					if (data.success == "FAIL") {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}

	function removeFloor() {
	    var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/floor/_removeFloor",
			dataType: 'json',
			data: {
				id: buttonData.id
			},
			success: function(data) {
				if (data.success == "OK") {
					$("#floorRowSelected_" + data.id).detach();
				} else {
					if (data.errorMsg) {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}

    $(document).on("click", "#btnRenderRowFloor", renderFloorRow);
    $(document).on("click", ".btnSaveFloor", addFloor);
    $(document).on("click", ".btnRemoveFloor", removeFloor);
    $(document).on("click", ".btnUpdateFloor", updateFloor);
</jq:jquery>