<g:javascript library="mustache" />
<div class="row-fluid">
    <div class="span12">
        <div id="list-servicio" class="content scaffold-list" role="main">
            <button type="button"
                    id="btnRenderRowAddress"
                    data-button='{"idEntity": "${entityInstance?.id}"}'
                    class="btn btn-success btnAddAddress">
                <span class="glyphicon glyphicon-plus"></span>
            </button>
            <table class="table table-stripped">
                <thead>
                <tr>
                    <g:sortableColumn property="street" title="${message(code: 'address.street.label')}" />
                    <g:sortableColumn property="city" title="${message(code: 'address.city.label')}" />
                    <g:sortableColumn property="country" title="${message(code: 'address.country.label')}" />
                    <g:sortableColumn property="zipCode" title="${message(code: 'address.zipCode.label')}" />
                    <th />
                </tr>
                </thead>
                <tbody id="addresses_list">
                <g:each in="${entityInstance?.addresses}" status="i" var="addressInstance">
                    <tr id="addressRowSelected_${addressInstance.id}">
                    	<td><g:textField name="txtStreet_${entityInstance.id}" id="txtStreet_${entityInstance.id}" class="form-control"
                     		value="${addressInstance?.street}" /></td>
                        <td><g:textField name="txtCity_${entityInstance.id}" id="txtCity_${entityInstance.id}" class="form-control"
                     		value="${addressInstance?.city}" /></td>
                        <td><g:textField name="txtCountry_${entityInstance.id}" id="txtCountry_${entityInstance.id}" class="form-control"
                     		value="${addressInstance?.country}" /></td>
                        <td><g:textField name="txtZip_${entityInstance.id}" id="txtZip_${entityInstance.id}" class="form-control"
                     		value="${addressInstance?.zip}" /></td>
                     	<td>
                        	<button type="button"
                                    data-button='{"id":  ${addressInstance.id}}'
                                    class="btn btn-success btnUpdateAddress">
                                <span class="glyphicon glyphicon-check"></span>
                            </button>
                        </td>
                        <td>
                            <button type="button"
                                    data-button='{"id":  ${addressInstance.id}}'
                                    class="btn btn-danger btnRemoveAddress">
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </td>

                    </tr>
                </g:each>
                </tbody>
            </table>
        </div> <!-- id=list... -->
    </div> <!-- span12 -->
</div>
<jq:jquery>
    var template;
	var templateFilePath = '<g:resource dir="templates" file="address-row.htm" />';

	$.get(templateFilePath, function(templates) {
		template = $(templates);
		//console.log("Plantillas cargadas");
	});

	function renderAddressRow(){
        var buttonData = $.parseJSON($(this).attr('data-button'));
        var t = template.filter('#address-row-available').html();
        $('#addresses_list').append(Mustache.render(t, buttonData));
	}

    function addAddress() {
		var buttonData = $.parseJSON($(this).attr('data-button'));
		//console.log(buttonData.idResponsible);
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/address/_addAddress",
			dataType: 'json',
			data: {
				idEntity: buttonData.idEntity,
				discriminator: '${discriminator}',
				street: $('#txtStreet_' + buttonData.idEntity).val(),
				city: $('#txtCity_' + buttonData.idEntity).val(),
				country: $('#txtCountry_' + buttonData.idEntity).val(),
				zip: $('#txtZip_' + buttonData.idEntity).val()
			},
			success: function(data) {
				if (data.success == "OK") {
				    $("#addressRowAvailable_" + data.idEntity).detach();
					var t = template.filter('#address-row-selected').html();
					$('#addresses_list').append(Mustache.render(t, data));
				} else {
					if (data.success == "FAIL") {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}
	
	function updateAddress() {
		var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/address/_updateAddress",
			dataType: 'json',
			data: {
				id: buttonData.id,
				street: $('#txtStreet_' + buttonData.idEntity).val(),
				city: $('#txtCity_' + buttonData.idEntity).val(),
				country: $('#txtCountry_' + buttonData.idEntity).val(),
				zip: $('#txtZip_' + buttonData.idEntity).val()
			},
			success: function(data) {
				if (data.success == "OK") {
					alert('Datos guardados.');
				    
				} else {
					if (data.success == "FAIL") {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}

	function removeAddress() {
	    var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/address/_removeAddress",
			dataType: 'json',
			data: {
				id: buttonData.id
			},
			success: function(data) {
				if (data.success == "OK") {
					$("#addressRowSelected_" + data.id).detach();
				} else {
					if (data.errorMsg) {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}

    $(document).on("click", "#btnRenderRowAddress", renderAddressRow);
    $(document).on("click", ".btnSaveAddress", addAddress);
    $(document).on("click", ".btnRemoveAddress", removeAddress);
    $(document).on("click", ".btnUpdateAddress", updateAddress);
</jq:jquery>