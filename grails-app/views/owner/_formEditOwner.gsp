<g:javascript library="mustache" />
<div class="row-fluid">
    <div class="span12">
        <div id="list-servicio" class="content scaffold-list" role="main">
            <button type="button"
                    id="btnRenderRowOwner"
                    data-button='{"idProperty": "${propertyInstance?.id}"}'
                    class="btn btn-success btnAddOwner">
                <span class="glyphicon glyphicon-plus"></span>
            </button>
            <table class="table table-stripped">
                <tbody id="owners_list">
                <g:each in="${propertyInstance?.owners}" status="i" var="ownerInstance">
                	<div id="ownerRowSelected_${ownerInstance.id}" class="divOwner">
                		<div style="display:block;width:90%;">
	                		<div style="float:left;width:100%;">
	                			<div style="float:left;width:25%;">${message(code: 'owner.name.label', default: 'Nombre:')}<g:textField name="txtCompleteName_${ownerInstance.id}" id="txtCompleteName_${ownerInstance.id}" class="form-control"
		                     		value="${ownerInstance?.completeName}" />
		                     	</div>
		                     	<div style="float:left;width:10%;">${message(code: 'owner.nif.label', default: 'DNI:')}<g:textField name="txtDni_${ownerInstance.id}" id="txtDni_${ownerInstance.id}" class="form-control"
		                     		value="${ownerInstance?.dni}" />
		                     	</div>
		                     	<div style="float:left;width:32%;">${message(code: 'owner.address.label', default: 'Dirección:')}<g:textField name="txtAddress_${ownerInstance.id}" id="txtAddress_${ownerInstance.id}" class="form-control"
		                     		value="${ownerInstance?.address}" />
		                     	</div>
		                     	<div style="float:left;width:15%;">${message(code: 'owner.phone1.label', default: 'Tel1:')}<g:textField name="txtPhone1_${ownerInstance.id}" id="txtPhone1_${ownerInstance.id}" class="form-control"
		                     		value="${ownerInstance?.phone1}" />
		                     	</div>
		                     	<div style="float:left;width:15%;">${message(code: 'owner.phone2.label', default: 'Tel2:')}<g:textField name="txtPhone2_${ownerInstance.id}" id="txtPhone2_${ownerInstance.id}" class="form-control"
		                     		value="${ownerInstance?.phone2}" />
		                     	</div>

                            </div>
                            <div style="float:left;width:100%;">
                                <div style="float:left;width:32%;">${message(code: 'owner.mail.label', default: 'MAil:')}<g:textField name="txtMail_${ownerInstance.id}" id="txtMail_${ownerInstance.id}" class="form-control"
                                    value="${ownerInstance?.mail}"  />
                                </div>
                                <div style="float:left;width:15%;">${message(code: 'owner.estado.label', default: 'Estado:')}<g:textField name="txtEstado_${ownerInstance.id}" id="txtEstado_${ownerInstance.id}" class="form-control"
                                                                                                                                          value="${ownerInstance?.estado}" />
                                </div>
                                <div style="float:left;width:15%;">${message(code: 'owner.regimen.label', default: 'Régimen:')}<g:textField name="txtRegimen_${ownerInstance.id}" id="txtRegimen_${ownerInstance.id}" class="form-control"
                                                                                                                                            value="${ownerInstance?.regimen}" />
                                </div>
                                <div style="float:left;width:20%;">${message(code: 'owner.horario.label', default: 'Horario:')}<g:textField name="txtHorario_${ownerInstance.id}" id="txtHorario_${ownerInstance.id}" class="form-control"
                                                                                                                                            value="${ownerInstance?.horario_contacto}" />
                                </div>
                                <div style="float:left;width:15%;">${message(code: 'owner.sector.label', default: 'Sector:')}<g:textField name="txtSector_${ownerInstance.id}" id="txtSector_${ownerInstance.id}" class="form-control"
                                                                                                                                          value="${ownerInstance?.sector}" />
                                </div>
	                		</div>
	                		<div style="float:left;width:100%;">
		                        <div style="float:left;width:40%;">${message(code: 'owner.observaciones.label', default: 'Observaciones:')}<g:textField name="txtObservaciones_${ownerInstance.id}" id="txtObservaciones_${ownerInstance.id}" class="form-control"
		                     		value="${ownerInstance?.observaciones}" />
		                     	</div>
	                     		<div style="float:left;width:15%;">${message(code: 'owner.idioma.label', default: 'Idioma:')}<g:textField name="txtIdioma_${ownerInstance.id}" id="txtIdioma_${ownerInstance.id}" class="form-control"
		                     		value="${ownerInstance?.idioma}" />
		                     	</div>
		                        <div style="float:left;width:12%;">${message(code: 'owner.trato.label', default: 'Trato:')}<g:textField name="txtTrato_${ownerInstance.id}" id="txtTrato_${ownerInstance.id}" class="form-control"
		                     		value="${ownerInstance?.trato}"/>
		                     	</div>
                                <div style="float:left;width:30%;">${message(code: 'owner.cuenta.label', default: 'Cuenta:')}<g:textField name="txtCuenta_${ownerInstance.id}" id="txtCuenta_${ownerInstance.id}" class="form-control"
                                                                                                                                                        value="${ownerInstance?.cuenta}" />
                                </div>
	                		</div>
                		</div>
                		<div style="float:left;width:10%;vertical-align: middle;">
                			<button type="button"
                                    data-button='{"id":  ${ownerInstance.id}}'
                                    class="btn btn-success btnUpdateOwner">
                                <span class="glyphicon glyphicon-check"></span>
                            </button>
                            <button type="button"
                                    data-button='{"id":  ${ownerInstance.id}}'
                                    class="btn btn-danger btnRemoveOwner">
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
               			</div>
                	</div>
                	<br>
                	<br>
                	<br>
                	<br>
                	<br> 
                	<br>
                	<br> 
                </g:each>
                </tbody>
            </table>

        </div> <!-- id=list... -->
    </div> <!-- span12 -->
</div>
<jq:jquery>
    var template;
	var templateFilePath = '<g:resource dir="templates" file="owners-row.htm" />';

	$.get(templateFilePath, function(templates) {
		template = $(templates);
		//console.log("Plantillas cargadas");
	});

	function renderOwnerRow(){
        var buttonData = $.parseJSON($(this).attr('data-button'));
        var t = template.filter('#owner-row-available').html();
        $('#owners_list').append(Mustache.render(t, buttonData));
	}

    function addOwner() {
        //Comprobamos que en los tlfs no vayan espacios ni puntos.
        var fuera=0;
        $.each($("[name*='Phone']"), function( index, element ) {
          if ($(this).val().indexOf(" ") > 0){
            alert('No se pueden introducir espacios en el teléfono.');
            fuera=1;
          };
          if ($(this).val().indexOf(".") > 0){
            alert('No se pueden introducir puntos en el teléfono.');
            fuera=1;
          };
        });

        if (fuera==1){
            return false;
        }

		var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/owner/_addOwner",
			dataType: 'json',
			data: {
				idProperty: buttonData.idProperty,
				completeName: $('#txtCompleteName__' + buttonData.idProperty).val(),
				dni: $('#txtDni__' + buttonData.idProperty).val(),
				address: $('#txtAddress__' + buttonData.idProperty).val(),
				phone1: $('#txtPhone1__' + buttonData.idProperty).val(),
				phone2: $('#txtPhone2__' + buttonData.idProperty).val(),
				mail: $('#txtMail__' + buttonData.idProperty).val(),
				estado: $('#txtEstado__' + buttonData.idProperty).val(),
				regimen: $('#txtRegimen__' + buttonData.idProperty).val(),
				horario_contacto: $('#txtHorario__' + buttonData.idProperty).val(),
				sector: $('#txtSector__' + buttonData.idProperty).val(),
				observaciones: $('#txtObservaciones__' + buttonData.idProperty).val(),
				idioma: $('#txtIdioma__' + buttonData.idProperty).val(),
				trato: $('#txtTrato__' + buttonData.idProperty).val(),
				cuenta: $('#txtCuenta__' + buttonData.idProperty).val()
			},
			success: function(data) {
				if (data.success == "OK") {
				    alert('Datos guardados.');
				} else {
					if (data.success == "FAIL") {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}
	
	function updateOwner() {
	    //Comprobamos que en los tlfs no vayan espacios ni puntos.
	    var fuera=0;
        $.each($("[name*='Phone']"), function( index, element ) {
          if ($(this).val().indexOf(" ") > 0){
            alert('No se pueden introducir espacios en el teléfono.');
            fuera=1;
          };
          if ($(this).val().indexOf(".") > 0){
            alert('No se pueden introducir puntos en el teléfono.');
            fuera=1;
          };
        });

        if (fuera==1){
            return false;
        }

		var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/owner/_updateOwner",
			dataType: 'json',
			data: {
				id: buttonData.id,
				completeName: $('#txtCompleteName_' + buttonData.id).val(),
				dni: $('#txtDni_' + buttonData.id).val(),
				address: $('#txtAddress_' + buttonData.id).val(),
				phone1: $('#txtPhone1_' + buttonData.id).val(),
				phone2: $('#txtPhone2_' + buttonData.id).val(),
				mail: $('#txtMail_' + buttonData.id).val(),
				estado: $('#txtEstado_' + buttonData.id).val(),
				regimen: $('#txtRegimen_' + buttonData.id).val(),
				horario_contacto: $('#txtHorario_' + buttonData.id).val(),
				sector: $('#txtSector_' + buttonData.id).val(),
				observaciones: $('#txtObservaciones_' + buttonData.id).val(),
				idioma: $('#txtIdioma_' + buttonData.id).val(),
				trato: $('#txtTrato_' + buttonData.id).val(),
				cuenta: $('#txtCuenta_' + buttonData.id).val()
			},
			success: function(data) {
				if (data.success == "OK") {
				    alert('Datos guardados.');
				} else {
					if (data.success == "FAIL") {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}

	function removeOwner() {
	    var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/owner/_removeOwner",
			dataType: 'json',
			data: {
				id: buttonData.id
			},
			success: function(data) {
				if (data.success == "OK") {
					$("#ownerRowSelected_" + data.id).detach();
				} else {
					if (data.errorMsg) {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}

    $(document).on("click", "#btnRenderRowOwner", renderOwnerRow);
    $(document).on("click", ".btnSaveOwner", addOwner);
    $(document).on("click", ".btnRemoveOwner", removeOwner);
    $(document).on("click", ".btnUpdateOwner", updateOwner);
    
</jq:jquery>