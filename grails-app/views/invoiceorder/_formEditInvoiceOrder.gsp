<g:javascript library="mustache" />
<div class="row-fluid">
    <div class="span12">
        <div id="list-servicio" class="content scaffold-list" role="main">
            <button type="button"
                    id="btnRenderRowOrder"
                    data-button='{"idInvoice": "${invoiceInstance?.id}"}'
                    class="btn btn-success btnAddOrder">
                <span class="glyphicon glyphicon-plus"></span>
            </button>
            <table class="table table-stripped">
                <thead>
                <tr>
                    <g:sortableColumn property="name" title="${message(code: 'invoiceorder.name.label')}" />
                    <g:sortableColumn property="total" title="${message(code: 'invoiceorder.total.label')}" />
                    <td></td>
                    <td></td>
                    
                </tr>
                </thead>
                <tbody id="orders_list">
                <g:each in="${invoiceInstance?.invoiceOrders}" status="i" var="orderInstance">
                    <tr id="orderRowSelected_${orderInstance.id}">
                        <td style="width: 70%;"><div class="col-xs-30"><g:textField name="txtName_${orderInstance.id}" id="txtName_${orderInstance.id}" class="form-control" placeholder=".col-xs-30"
                     		value="${orderInstance?.name}" /></div></td>
                        <td style="width: 20%;"><div class="col-xs-12"><g:textField name="txtTotal_${orderInstance.id}" id="txtTotal_${orderInstance.id}" class="form-control" placeholder=".col-xs-12"
                     		value="${orderInstance?.total}" /></div></td>
                        <td style="width: 5%;">
                        	<button type="button"
                                    data-button='{"id":  ${orderInstance.id}}'
                                    class="btn btn-success btnUpdateOrder">
                                <span class="glyphicon glyphicon-check"></span>
                            </button>
                        </td>
                        <td style="width: 5%;">
                            <button type="button"
                                    data-button='{"id":  ${orderInstance.id}}'
                                    class="btn btn-danger btnRemoveOrder">
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>

        </div> <!-- id=list... -->
    </div> <!-- span12 -->
</div>
<jq:jquery>
    var template;
	var templateFilePath = '<g:resource dir="templates" file="orderInvoice-row.htm" />';

	$.get(templateFilePath, function(templates) {
		template = $(templates);
		//console.log("Plantillas cargadas");
	});

	function renderOrderRow(){
        var buttonData = $.parseJSON($(this).attr('data-button'));
        var t = template.filter('#order-row-available').html();
        $('#orders_list').append(Mustache.render(t, buttonData));
	}

    function addOrder() {
		var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/invoiceorder/_addOrder",
			dataType: 'json',
			data: {
				idInvoice: buttonData.idInvoice,
				name: $('#txtName__' + buttonData.idInvoice).val(),
				total: $('#txtTotal__' + buttonData.idInvoice).val(),
				retention: $('#retention').val()
			},
			success: function(data) {
				if (data.success == "OK") {
				    $("#orderRowAvailable_" + data.idInvoice).detach();
					var t = template.filter('#order-row-selected').html();
					$('#orders_list').append(Mustache.render(t, data));
					$('#invoiceTaxes').val(data.totalOrderTaxes);
					$('#invoiceRetention').val(data.totalInvoiceRetention);
					$('#invoiceTotal').val(data.total);
				} else {
					if (data.success == "FAIL") {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}
	
	function updateOrder() {
		var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/invoiceorder/_updateOrder",
			dataType: 'json',
			data: {
				id: buttonData.id,
				name: $('#txtName_' + buttonData.id).val(),
				total: $('#txtTotal_' + buttonData.id).val()
			},
			success: function(data) {
				if (data.success == "OK") {
					alert('Datos guardados.');
					$('#invoiceTaxes').val(data.totalOrderTaxes);
					$('#invoiceRetention').val(data.totalRetention);
					$('#invoiceTotal').val(data.total);

				} else {
					if (data.success == "FAIL") {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}

	function removeOrder() {
	    var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/invoiceorder/_removeOrder",
			dataType: 'json',
			data: {
				id: buttonData.id
			},
			success: function(data) {
				if (data.success == "OK") {
					$("#orderRowSelected_" + data.id).detach();
				} else {
					if (data.errorMsg) {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}

    $(document).on("click", "#btnRenderRowOrder", renderOrderRow);
    $(document).on("click", ".btnSaveOrder", addOrder);
    $(document).on("click", ".btnRemoveOrder", removeOrder);
    $(document).on("click", ".btnUpdateOrder", updateOrder);
</jq:jquery>