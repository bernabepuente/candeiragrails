<%@ page import="es.xoubin.domain.Tipo_Inmueble" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:javascript library="validator" />
    <g:set var="entityName" value="${message(code: 'city.label', default: 'Localidad')}" />
    <title><g:message code="default.create.label" args="[entityName]" /></title>
</head>
<body>
<g:render template="/layouts/navbar" />
<g:render template="/layouts/header" />
<div class="container" style="width: 100%; margin:0 auto 0 auto;">
    <div class="row-fluid">
        <div class="span12">
            <div id="create-city" class="content scaffold-create" role="main" style="width: 80%; margin:0 auto 0 auto;">
                <div class="page-header">
                    <h2><g:message code="city.edit.label" /></h2>
                </div>
                <g:if test="${flash.message}">
                    <div class="alert alert-success message" role="status">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        ${flash.message}
                    </div>
                </g:if>
                <div id="axMainMsg" class="alert hide" role="status">
                </div>
                <br>
                <fieldset>
                    <div id="datos-basicos">
                        <g:render template="form"/>
                    </div>
                </fieldset>
            </div>
        </div> <!-- span12 -->
    </div> <!-- row-fluid -->
</div> <!-- container-fluid -->
<g:render template="/layouts/footer" />