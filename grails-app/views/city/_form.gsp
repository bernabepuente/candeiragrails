<g:javascript library="mustache" />
<div class="row-fluid">
    <div class="span12">
        <div id="list-servicio" class="content scaffold-list" role="main">
            <button type="button"
                    id="btnRenderRowCity"
                    data-button='{"id": ""}'
                    class="btn btn-success btnAddCity">
                <span class="glyphicon glyphicon-plus"></span>
            </button>
            <table class="table table-stripped">
                <thead>
                <tr>
                    <g:sortableColumn property="nombre" title="${message(code: 'city.name.label')}" />
                    <th />
                </tr>
                </thead>
                <tbody id="cities_list">
                <g:each in="${cityInstanceList}" status="i" var="cityInstance">
                    <tr id="cityRowSelected_${cityInstance.id}">
                        <td>${fieldValue(bean: cityInstance, field: "name")}</td>
                        <td>
                            <button type="button"
                                    data-button='{"id":  ${cityInstance.id}}'
                                    class="btn btn-danger btnRemoveCity">
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>

        </div> <!-- id=list... -->
    </div> <!-- span12 -->
</div>
<jq:jquery>
    var template;
	var templateFilePath = '<g:resource dir="templates" file="city-row.htm" />';

	$.get(templateFilePath, function(templates) {
		template = $(templates);
		//console.log("Plantillas cargadas");
	});

	function renderCityRow(){
        var buttonData = $.parseJSON($(this).attr('data-button'));
        var t = template.filter('#city-row-available').html();
        $('#cities_list').append(Mustache.render(t, buttonData));
	}

    function addCity() {
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/city/_addCity",
			dataType: 'json',
			data: {
				name: $('#txtName').val()
			},
			success: function(data) {
				if (data.success == "OK") {
				    $("#cityRowAvailable").detach();
					var t = template.filter('#city-row-selected').html();
					$('#cities_list').append(Mustache.render(t, data));
				} else {
					if (data.success == "FAIL") {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}

	function removeCity() {
	    var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/city/_removeCity",
			dataType: 'json',
			data: {
				id: buttonData.id
			},
			success: function(data) {
				if (data.success == "OK") {
					$("#cityRowSelected_" + data.id).detach();
				} else {
					if (data.errorMsg) {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}

    $(document).on("click", "#btnRenderRowCity", renderCityRow);
    $(document).on("click", ".btnSaveCity", addCity);
    $(document).on("click", ".btnRemoveCity", removeCity);
</jq:jquery>