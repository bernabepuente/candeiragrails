<%@ page import="es.xoubin.domain.Customer"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'customer.label', default: 'Customer')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<g:render template="/layouts/navbar" />
	<g:render template="/layouts/header" />
	<div style="width: 90%">
		<div class="row-fluid">
			<div class="span12">
				<div id="list-costumer" class="content scaffold-list" role="main" style="width: 100%; margin:0 auto 0 auto;">
					<div class="page-header">
						<h2>
							<g:message code="customer.list.label" />
						</h2>
					</div>
					<div>
						<form action="list" class="form-horizontal">
							<g:field class="well" type="text" name="nameBusqueda" />
							<g:submitButton class="btn btn-info" name="submit" value="Buscar" />
						</form>
					</div>
					<g:if test="${flash.message}">
						<div class="alert alert-success message" role="status">
							<button type="button" class="close" data-dismiss="alert">×</button>
							${flash.message}
						</div>
					</g:if>
					<table class="table table-stripped">
						<thead>
							<tr>
                                <g:sortableColumn property="referencia"
                                                  title="${message(code: 'customer.referencia.label', default: 'Referencia')}" />
                                <g:sortableColumn property="agente"
                                                  title="${message(code: 'customer.agent.label', default: 'Agente')}" />
                                <g:sortableColumn property="Persona"
                                                  title="${message(code: 'customer.persona.label', default: 'Persona')}" />
                              	<g:sortableColumn property="NIF"
                                                  title="${message(code: 'customer.nif.label', default: 'NIF')}" />
							</tr>
						</thead>
						<tbody class="table table-stripped">
							<g:each in="${customerInstanceList}" status="i"
								var="customerInstance">
                                <g:if test="${(customerInstance.tipo?.toInteger()>=2)}">
                                    <tr style="background-color: RED;">
                                </g:if>
                                <g:else>
                                    <tr style="background-color: #d3d3d3;">
                                </g:else>
									<g:if test="${(agentId==customerInstance?.agent?.id)||(agentId==1)}">
                                    <td><g:link action="edit" id="${customerInstance.id}">
                                        ${fieldValue(bean: customerInstance, field: "referencia")}
                                    </g:link></td>
                                    </g:if>
                                    <g:else>
                                    	<td>
                                    	${fieldValue(bean: customerInstance, field: "referencia")}
                                    	</td>
                                    </g:else>
                                    <g:each in="${customerInstance.customerPersons.sort{a,b->a.id.compareTo(b.id)}}" var="customerPersonInstance" status="j">
                                    	<g:if test="${j == 0}">
                                    		<g:if test="${(agentId==customerInstance?.agent?.id)||(agentId==1)}">
                                    			<td>
									               <g:link action="edit" id="${customerInstance.id}" params="['term': term, 'offset': offset]">
				                                        ${customerInstance?.agent?.name}
				                                   </g:link>
												</td>
		                                     	<td>
									               <g:link action="edit" id="${customerInstance.id}" params="['term': term, 'offset': offset]">
				                                        ${fieldValue(bean: customerPersonInstance, field: "name")}
				                                   </g:link>
				                                   <g:link action="edit" id="${customerInstance.id}" params="['term': term, 'offset': offset]">
				                                        ${fieldValue(bean: customerPersonInstance, field: "surname")}
				                                   </g:link>
												</td>
												<td>
									               <g:link action="edit" id="${customerInstance.id}" params="['term': term, 'offset': offset]">
				                                        ${fieldValue(bean: customerPersonInstance, field: "nif")}
				                                   </g:link>
												</td>
											</g:if>
											<g:else>
												<td>${customerInstance?.agent?.name}</td>
												<td>
			                                        ${fieldValue(bean: customerPersonInstance, field: "name")}
			                                        ${fieldValue(bean: customerPersonInstance, field: "surname")}
												</td>
												<td>
			                                        ${fieldValue(bean: customerPersonInstance, field: "nif")}
												</td>
											</g:else>
										</g:if>
									 </g:each>
								</tr>
							</g:each>
						</tbody>
					</table>
					<div class="pagination">
						<g:paginate total="${totalCount}" params="[term: term, 'offset': offset]" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<g:render template="/layouts/footer" />
</body>
</html>
