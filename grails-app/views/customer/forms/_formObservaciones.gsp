<div class="form-group has-warnin">
    <label for="observaciones" class="col-lg-2 control-label"><g:message
            code="customer.observaciones.label" default="Observaciones" /></label>
    <div class="col-lg-10">
        <g:textArea name="observaciones" class="form-control"
                     value="${customerInstance?.observaciones}" />
    </div>
</div>