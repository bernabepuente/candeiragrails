<div class="form-group has-warnin">
    <label for="fuente" class="col-lg-2 control-label"><g:message
            code="customer.fuente.label" default="Fuente" /></label>
    <div class="col-lg-10">
        <g:textField name="fuente" class="form-control"
                     value="${customerInstance?.fuente}" />
    </div>
</div>
<div class="form-group has-warnin">
    <label for="sector" class="col-lg-2 control-label"><g:message
            code="customer.sector.label" default="Sector" /></label>
    <div class="col-lg-10">
        <g:textField name="sector" class="form-control"
                     value="${customerInstance?.sector}" />
    </div>
</div>
<div class="form-group">
    <label for="plazo_maximo" class="col-lg-2 control-label"><g:message
            code="customer.plazo_maximo.label" default="Plazo Máximo" /></label>
    <div class="col-lg-10">
        <g:textField name="plazo_maximo" class="form-control"
                     value="${customerInstance?.plazo_maximo}" />
    </div>
</div>
<div class="form-group">
    <label for="cboAgente" class="col-lg-2 control-label"><g:message
            code="customer.agent.label" default="Agente" /></label>
    <div class="col-lg-10">
        <g:select name="cboAgente" from="${agentInstanceList}"
                  value="${customerInstance?.agent?.id}" optionKey="id"
                  optionValue="name"  noSelection="['':'--Elegir--']"/>
    </div>
</div>