<div class="form-group">
    <label for="referencia" class="col-lg-2 control-label"><g:message
            code="customer.referencia.label" default="Referencia" /></label>
    <div class="col-lg-10">
        <g:textField name="referencia" class="form-control" readonly="readonly"
                     value="${customerInstance?.referencia}" />
    </div>
</div>
<div class="form-group">
    <label for="tipo" class="col-lg-2 control-label"><g:message
            code="customer.tipo.label" default="Tipo" /></label>
    <div class="col-lg-10">
        <g:select name="tipo" from="${0..3}"
                  value="${customerInstance?.tipo}" noSelection="['':'--Elegir--']"/>
    </div>
</div>
<div>
    <g:render template="/customerperson/formEditCustomerPerson" />
</div>