<%@ page import="es.xoubin.domain.Customer" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:javascript library="validator" />
    <g:set var="entityName" value="${message(code: 'customer.label', default: 'Customer')}" />
    <title><g:message code="default.create.label" args="[entityName]" /></title>
</head>
<body>
<g:render template="/layouts/navbar" />
<g:render template="/layouts/header" />
<div class="container" style="width: 100%;">
    <div class="row-fluid">
        <div class="span12">
            <div id="create-customer" class="content scaffold-create" role="main" style="width: 100%; margin:0 auto 0 auto;">
                <div class="page-header">
                    <h2><g:message code="customer.edit.label" /></h2>
                </div>
                <g:if test="${flash.message}">
                    <div class="alert alert-success message" role="status">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        ${flash.message}
                    </div>
                </g:if>
                <div id="axMainMsg" class="alert hide" role="status">
                </div>
                <g:hasErrors bean="${customerInstance}">
                    <div class="alert alert-error">
                        <ul class="errors" role="alert">
                            <g:eachError bean="${customerInstance}" var="error">
                                <li>
                                    <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>
                                    <g:message error="${error}"/>
                                </li>
                            </g:eachError>
                        </ul>
                    </div>
                </g:hasErrors>

                <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                    <li class="active">
                        <a href="#datos-basicos" data-toggle="tab">
                            <g:message code="customer.tab.datos" />
                        </a>
                    </li>
                    <li>
                        <a href="#addresses" data-toggle="tab">
                            <g:message code="customer.tab.addresses" />
                        </a>
                    </li>
                    <li>
                        <a href="#otros" data-toggle="tab">
                            <g:message code="customer.tab.otros" />
                        </a>
                    </li>
                    <li>
                        <a href="#observaciones" data-toggle="tab">
                            <g:message code="customer.tab.observaciones" />
                        </a>
                    </li>
                    <li>
                        <a href="#busquedas" data-toggle="tab">
                            <g:message code="customer.tab.busquedas" />
                        </a>
                    </li>
                </ul>
                <br>
                <g:form class="form-horizontal" action="save" name='customerForm'>
                    <g:hiddenField name="id" id="idCustomer"
                                   value="${customerInstance?.id}" />
                    <fieldset>
                        <div id="my-tab-content" class="tab-content">
                            <div class="tab-pane active" id="datos-basicos">
                                <g:render template="forms/form"/>
                            </div>
                            <div class="tab-pane" id="addresses">
                                <g:render template="/address/formEditAddresses" />
                            </div>
                            <div class="tab-pane" id="otros">
                                <g:render template="forms/formOtros" />
                            </div>
                            <div class="tab-pane" id="observaciones">
                                <g:render template="forms/formObservaciones" />
                            </div>
                            <div class="tab-pane" id="busquedas">
                                <g:render template="/search/formEditSearches" />
                            </div>
                        </div>
                    </fieldset>
                    <br>
                    <fieldset>
                        <g:actionSubmit id="btnUpdate" class="save btn btn-primary"
                                        action="update"
                                        value="${message(code: 'default.button.update.label', default: 'Update')}" />
                        <g:actionSubmit id="btnDelete" class="btn btn-danger"
                                        action="delete"
                                        value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                        formnovalidate=""
                                        onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                    </fieldset>
                </g:form>
            </div>
        </div> <!-- span12 -->
    </div> <!-- row-fluid -->
</div> <!-- container-fluid -->
<g:render template="/layouts/footer" />
<jq:jquery>
    $(document).ready(function ($) {
            $('#tabs').tab();
        });
</jq:jquery>
</body>
</html>
