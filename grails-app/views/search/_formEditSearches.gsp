<g:javascript library="mustache" />
<div class="row-fluid">
    <div class="span12">
        <div id="list-servicio" class="content scaffold-list" role="main">
            <button type="button"
                    id="btnRenderRowSearch"
                    data-button='{"idCustomer": "${entityInstance?.id}", "options": [${tipoInstanceListMap}], "options_city": [${cityInstanceListMap}], "options_zone": [${zoneInstanceListMap}]}'
                    class="btn btn-success btnAddSearch">
                <span class="glyphicon glyphicon-plus"></span>
            </button>
            <br>
            <table class="table table-stripped">
                <tbody id="searches_list">
	                <g:each in="${entityInstance?.searches}" status="i" var="searchInstance">
                        <div id="searchRowSelected_${searchInstance.id}">
                            <div style="display:block;width:85%;">
                                <div style="float:left;width:100%;padding: 0% 0;">
                                    <div style="float:left;width:19%;padding: 4% 0;">${message(code: 'search.tipo.label')}
                                        <g:select name="cboTipo_${searchInstance.id}" id="cboTipo_${searchInstance.id}" from="${tipoInstanceList}" value="${searchInstance?.tipoInmueble?.id}" optionKey="id" optionValue="name"  noSelection="['':'--Elegir--']"/>
                                    </div>
                                    <div style="float:left;width:21%;padding: 4% 0;">${message(code: 'search.operacion.label')}
                                    <g:select name="cboOperacion_${searchInstance.id}" id="cboOperacion_${searchInstance.id}" from="['ALQUILER', 'VENTA', 'ALQUILER_VENTA', 'ALQUILER_COMPRA']" value="${searchInstance?.operacion}" noSelection="['':'--Elegir--']"/>
                                    </div>
                                    <div style="float:left;width:13%;padding: 4% 0;">${message(code: 'search.city.label', default: 'Localidad:')}
                                        <g:select class="comboCity" name="cboCity_${searchInstance.id}" id="cboCity_${searchInstance.id}" from="${cityInstanceList}" value="${searchInstance?.city?.id}" optionKey="id" optionValue="name"  noSelection="['':'--Elegir--']"/>
                                    </div>
                                    <div style="float:left;width:29%;padding: 4% 0;">${message(code: 'search.zone.label', default: 'Zone:')}
                                        <g:select name="cboZone_${searchInstance.id}" id="cboZone_${searchInstance.id}" from="${zoneInstanceList}" value="${searchInstance?.zone?.id}" optionKey="id" optionValue="name"  noSelection="['':'--Elegir--']"/>
                                    </div>
                                    <div style="float:left;width:8%;padding: 2% 0;">${message(code: 'search.parcela_min.label')}
                                        <g:textField name="txtParcelaMin_${searchInstance.id}" id="txtParcelaMin_${searchInstance.id}" class="form-control" style="width:60px;" value="${searchInstance?.sup_parcela_min}" />
                                    </div>
                                    <div style="float:left;width:8%;padding: 2% 0;">${message(code: 'search.parcela_max.label')}
                                        <g:textField name="txtParcelaMax_${searchInstance.id}" id="txtParcelaMax_${searchInstance.id}" class="form-control" style="width:60px;" value="${searchInstance?.sup_parcela_max}" />
                                    </div>
                                </div>
                                <br>
                                <div style="float:left;width:100%;padding: 0% 0;">
                                    <div style="float:left;width:4%;padding: 2% 0;">${message(code: 'search.amueblado.label')}
                                        <g:checkBox name="chkAmueblado_${searchInstance.id}" value="${searchInstance?.amueblado}"></g:checkBox>
                                    </div>
                                    <div style="float:left;width:3%;padding: 2% 0;">${message(code: 'search.garaje.label')}
                                        <g:checkBox name="chkGaraje_${searchInstance.id}" value="${searchInstance?.garaje}"></g:checkBox>
                                    </div>
                                    <div style="float:left;width:8%;">${message(code: 'search.construida_min.label')}
                                        <g:textField name="txtConstruidaMin_${searchInstance.id}" id="txtConstruidaMin_${searchInstance.id}" class="form-control" style="width:50px;" value="${searchInstance?.sup_construida_min}" />
                                    </div>
                                    <div style="float:left;width:8%;">${message(code: 'search.construida_max.label')}
                                        <g:textField name="txtConstruidaMax_${searchInstance.id}" id="txtConstruidaMax_${searchInstance.id}" class="form-control" style="width:50px;" value="${searchInstance?.sup_construida_max}" />
                                    </div>
                                    <div style="float:left;width:6%;">${message(code: 'search.dormitorios.label', default: 'Dorm.:')}
                                        <g:textField name="txtDormitorios_${searchInstance.id}" id="txtDormitorios_${searchInstance.id}" class="form-control" style="width:35px;" value="${searchInstance?.dormitorios}" />
                                    </div>
                                    <div style="float:left;width:6%;">${message(code: 'search.banos.label', default: 'Bañ.:')}
                                        <g:textField name="txtBanos_${searchInstance.id}" id="txtBanos_${searchInstance.id}" class="form-control" style="width:35px;" value="${searchInstance?.banos}" />
                                    </div>
                                    <div style="float:left;width:14%;">${message(code: 'search.sur.label', default: 'Sur:')}
                                        <g:textField name="txtSur_${searchInstance.id}" id="txtSur_${searchInstance.id}" class="form-control" style="width:110px;" value="${searchInstance?.limite_sur}" />
                                    </div>
                                    <div style="float:left;width:14%;">${message(code: 'search.norte.label', default: 'Norte:')}
                                        <g:textField name="txtNorte_${searchInstance.id}" id="txtNorte_${searchInstance.id}" class="form-control" style="width:110px;" value="${searchInstance?.limite_norte}" />
                                    </div>
                                    <div style="float:left;width:14%;">${message(code: 'search.interior.label', default: 'Int.:')}
                                        <g:textField name="txtInterior_${searchInstance.id}" id="txtInterior_${searchInstance.id}" class="form-control" style="width:110px;" value="${searchInstance?.limite_interior}" />
                                    </div>
                                    <div style="float:left;width:3%;padding: 2% 0;">${message(code: 'search.vistas.label')}
                                        <g:checkBox name="txtVista_${searchInstance.id}" value="${searchInstance?.vistas_mar}"></g:checkBox>
                                    </div>
                                    <div style="float:left;width:3%;padding: 2% 0;">${message(code: 'search.llave.label')}
                                        <g:checkBox name="txtLlave_${searchInstance.id}" value="${searchInstance?.llave}"></g:checkBox>
                                    </div>
                                    <div style="float:left;width:15%;">${message(code: 'search.maximo.label')}
                                    <g:textField name="txtMaximo_${searchInstance.id}" id="txtMaximo_${searchInstance.id}" class="form-control" style="width:100px;" value="${searchInstance?.maximo}" />
                                    </div>
                                </div>
                            </div>
                            <div style="float:left;width:15%;">
                                <g:link action="_searchFromCustomer" id="${searchInstance?.id}" class="btn btn-info"><span class="glyphicon glyphicon-search"></span></g:link>
                                <button type="button"
                                        data-button='{"id":  ${searchInstance.id}}'
                                        class="btn btn-success btnUpdateSearch">
                                    <span class="glyphicon glyphicon-check"></span>
                                </button>
                                <button type="button"
                                        data-button='{"id":  ${searchInstance.id}}'
                                        class="btn btn-danger btnRemoveSearch">
                                    <span class="glyphicon glyphicon-trash"></span>
                                </button>
                            </div>
                        </div>
	                </g:each>
                </tbody>
            </table>
        </div> <!-- id=list... -->
    </div> <!-- span12 -->
</div>
<jq:jquery>
    var template;
	var templateFilePath = '<g:resource dir="templates" file="search-row.htm" />';

	$.get(templateFilePath, function(templates) {
		template = $(templates);
		//console.log("Plantillas cargadas");
	});

	function renderSearchRow(){
        var buttonData = $.parseJSON($(this).attr('data-button'));
        var t = template.filter('#search-row-available').html();
        $('#searches_list').append(Mustache.render(t, buttonData));
	}

	//Cargamos el combo de zona, al cambiar la city
            $(document).ready(function(){
                $(".comboCity").change(function () {
                    var idSearch=($(this).attr("id")).split("_")[1];
                    $.ajax({
                            type: "POST",
                            url: "${request.contextPath}/zone/_getZonesByCity",
                            dataType: 'json',
                            data: {
                                idCity: $(this).val()
                            },
                            success: function(data) {
                                if (data.success == 'OK') {
                                    $('#cboZone_'+idSearch).empty();
                                    $('#cboZone_'+idSearch).append('<option value="">--Elegir--</option>');
                                    $.each(data.list, function(i, value) {
                                            $('#cboZone_'+idSearch).append('<option value="'+value[0]+'">'+value[1]+'</option>');
                                       });
                                } else {
                                    //alert(data);
                                }
                            },
                            error: function(data) {
                                //alert(data);
                            },
                            complete: function() {
                                //alert(data);
                            }
                    });
                })
            });

    function addSearch() {
		var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/search/_addSearch",
			dataType: 'json',
			data: {
				idCustomer: buttonData.idCustomer,
				tipo: $('#cboTipo__' + buttonData.idCustomer).val(),
				city: $('#cboCity__' + buttonData.idCustomer).val(),
				zone: $('#cboZone__' + buttonData.idCustomer).val(),
				operacion: $('#cboOperacion__' + buttonData.idCustomer).val(),
				sup_parcela_min: $('#txtParcelaMin__' + buttonData.idCustomer).val(),
				sup_parcela_max: $('#txtParcelaMax__' + buttonData.idCustomer).val(),
				sup_construida_min: $('#txtConstruidaMin__' + buttonData.idCustomer).val(),
				sup_construida_max: $('#txtConstruidaMax__' + buttonData.idCustomer).val(),
				amueblado: $('#chkAmueblado__' + buttonData.idCustomer).is(':checked'),
				garaje: $('#chkGaraje__' + buttonData.idCustomer).is(':checked'),
				dormitorios: $('#txtDormitorios__' + buttonData.idCustomer).val(),
				banos: $('#txtBanos__' + buttonData.idCustomer).val(),
				limite_sur: $('#txtSur__' + buttonData.idCustomer).val(),
				limite_norte: $('#txtNorte__' + buttonData.idCustomer).val(),
				limite_interior: $('#txtInterior__' + buttonData.idCustomer).val(),
				vistas_mar: $('#txtVistas__' + buttonData.idCustomer).is(':checked'),
				llave: $('#txtLlave__' + buttonData.idCustomer).is(':checked'),
				maximo: $('#txtMaximo__' + buttonData.idCustomer).val()
			},
			success: function(data) {
				if (data.success == "OK") {
				    location.reload();
				} else {
					if (data.success == "FAIL") {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}
	
	function updateSearch() {
		var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/search/_updateSearch",
			dataType: 'json',
			data: {
				id: buttonData.id,
				tipo: $('#cboTipo_' + buttonData.id).val(),
				city: $('#cboCity_' + buttonData.id).val(),
				zone: $('#cboZone_' + buttonData.id).val(),
				operacion: $('#cboOperacion_' + buttonData.id).val(),
				sup_parcela_min: $('#txtParcelaMin_' + buttonData.id).val(),
				sup_parcela_max: $('#txtParcelaMax_' + buttonData.id).val(),
				sup_construida_min: $('#txtConstruidaMin_' + buttonData.id).val(),
				sup_construida_max: $('#txtConstruidaMax_' + buttonData.id).val(),
				amueblado: $('#chkAmueblado_' + buttonData.id).is(':checked'),
				garaje: $('#chkGaraje_' + buttonData.id).is(':checked'),
				dormitorios: $('#txtDormitorios_' + buttonData.id).val(),
				banos: $('#txtBanos_' + buttonData.id).val(),
				limite_sur: $('#txtSur_' + buttonData.id).val(),
				limite_norte: $('#txtNorte_' + buttonData.id).val(),
				limite_interior: $('#txtInterior_' + buttonData.id).val(),
				vistas_mar: $('#txtVistas_' + buttonData.id).is(':checked'),
				llave: $('#txtLlave_' + buttonData.id).is(':checked'),
				maximo: $('#txtMaximo_' + buttonData.id).val()
			},
			success: function(data) {
				if (data.success == "OK") {
					alert('Datos guardados.');
				} else {
					if (data.success == "FAIL") {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}

	function removeSearch() {
	    var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/search/_removeSearch",
			dataType: 'json',
			data: {
				id: buttonData.id
			},
			success: function(data) {
				if (data.success == "OK") {
					$("#searchRowSelected_" + data.id).detach();
				} else {
					if (data.errorMsg) {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}
	
	function goSearch() {
	    var buttonData = $.parseJSON($(this).attr('data-button'));
	    $.ajax({
			type: "GET",
			url: "${request.contextPath}/property/_searchFromCustomer",
			dataType: 'json',
			data: {
				id: buttonData.id
			}
		});
	}

    $(document).on("click", "#btnRenderRowSearch", renderSearchRow);
    $(document).on("click", ".btnSaveSearch", addSearch);
    $(document).on("click", ".btnRemoveSearch", removeSearch);
    $(document).on("click", ".btnUpdateSearch", updateSearch);
    $(document).on("click", ".btnSearch", goSearch);
</jq:jquery>