<%@ page import="es.xoubin.domain.Customer" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:javascript library="validator" />
    <g:set var="entityName" value="${message(code: 'invoice.label', default: 'Factura')}" />
    <title><g:message code="default.create.label" args="[entityName]" /></title>
</head>
<body>
<g:render template="/layouts/navbar" />
<g:render template="/layouts/header" />
<div class="container" style="width: 100%;">
    <div class="row-fluid">
        <div class="span12">
            <div id="create-invoice" class="content scaffold-create" role="main" style="width: 100%; margin:0 auto 0 auto;">
                <div class="page-header">
                    <h2><g:message code="invoice.edit.label" /></h2>
                </div>
                <g:if test="${flash.message}">
                    <div class="alert alert-success message" role="status">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        ${flash.message}
                    </div>
                </g:if>
                <div id="axMainMsg" class="alert hide" role="status">
                </div>
                <g:hasErrors bean="${invoiceInstance}">
                    <div class="alert alert-error">
                        <ul class="errors" role="alert">
                            <g:eachError bean="${invoiceInstance}" var="error">
                                <li>
                                    <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>
                                    <g:message error="${error}"/>
                                </li>
                            </g:eachError>
                        </ul>
                    </div>
                </g:hasErrors>
                <g:form class="form-horizontal" action="save" name='customerForm'>
                    <g:hiddenField name="id" id="idInvoice"
                                   value="${invoiceInstance?.id}" />
                    <fieldset>
                        <div id="my-tab-content" class="tab-content">
                            <div class="form-group">
                                <label for="reference" class="col-lg-2 control-label"><g:message
                                        code="invoice.titular.label" default="Titular" /></label>
                                <div class="col-lg-10">
                                    <g:textField name="titular" class="form-control"
                                                 value="${invoiceInstance?.titular}" />
                                </div>
                            </div>
                            <div class="form-group">
							    <label for="reference" class="col-lg-2 control-label"><g:message
							            code="invoice.reference.label" default="Referencia" /></label>
							    <div class="col-lg-10">
							        <g:textField name="reference" class="form-control"
							                     value="0" />
							    </div>
							</div>
							<div class="form-group">
							    <label for="agent" class="col-lg-2 control-label"><g:message
							            code="invoice.agent.label" default="Agente" /></label>
							    <div class="col-lg-10">
							        <g:select name="cboAgente" from="${agentInstanceList}"
					                  value="${invoiceInstance?.agent?.id}" optionKey="id" disabled="disabled"
					                  optionValue="name"  noSelection="['':'--Elegir--']"/>
							    </div>
							</div>
							<div class="form-group">
							    <label for="dateInvoice" class="col-lg-2 control-label"><g:message
							            code="invoice.dateInvoice.label" default="Fecha" /></label>
							    <div class="col-lg-10">
							         <g:datePicker type="text" dateFormat="yy-MM-yyyy" name="dateInvoice" id="dateInvoice" class="form-control" precision="day" noSelection="['':'-Elegir-']"
                      					value="${invoiceInstance?.dateInvoice}" />
							    </div>
							</div>
                            <div class="form-group">
                                <label for="tax" class="col-lg-2 control-label"><g:message
                                        code="invoice.tax.label" default="I.V.A." /></label>
                                <div class="col-lg-10">
                                    <g:textField name="tax" class="form-control"
                                                 value="${invoiceInstance?.tax}" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tax" class="col-lg-2 control-label"><g:message
                                        code="invoice.retention.label" default="Retención" /></label>
                                <div class="col-lg-10">
                                    <g:textField name="retention" class="form-control"
                                                 value="${invoiceInstance?.retention}" />
                                </div>
                            </div>
							<div class="form-group">
							    <label for="customer" class="col-lg-2 control-label"><g:message
							            code="invoice.customer.label" default="Cliente:" /></label>
							    <div class="col-lg-10">
							         <g:textField name="customer" class="form-control"
							                     value="${invoiceInstance?.customer}" />
							    </div>
							</div>
							<div class="form-group">
							    <label for="nif" class="col-lg-2 control-label"><g:message
							            code="invoice.nif.label" default="Nif:" /></label>
							    <div class="col-lg-10">
							         <g:textField name="nif" class="form-control"
							                     value="${invoiceInstance?.nif}" />
							    </div>
							</div>
							<div class="form-group">
							    <label for="address" class="col-lg-2 control-label"><g:message
							            code="invoice.address.label" default="Dirección:" /></label>
							    <div class="col-lg-10">
							         <g:textField name="address" class="form-control"
							                     value="${invoiceInstance?.address}" />
							    </div>
							</div>
							<div class="form-group">
							    <label for="payMethod" class="col-lg-2 control-label"><g:message
							            code="invoice.payMethod.label" default="Forma de Pago:" /></label>
							    <div class="col-lg-10">
							         <g:textField name="payMethod" class="form-control"
							                     value="${invoiceInstance?.payMethod}" />
							    </div>
							</div>
							<div>
							    <g:render template="/invoiceorder/formEditInvoiceOrder" />
							</div>
							<div class="form-group">
								<label class="col-lg-6">&nbsp;</label>
							    <label for="invoiceTaxes" class="col-lg-2 control-label"><g:message
							            code="invoice.taxes.label" default="I.V.A.:" /></label>
							    <div class="col-lg-2">
							         <g:textField name="invoiceTaxes" class="form-control"
							                     value="${invoiceInstance?.totalOrderTaxes}" />
							    </div>
							</div>
                            <div class="form-group">
                                <label class="col-lg-6">&nbsp;</label>
                                <label for="invoiceRetention" class="col-lg-2 control-label"><g:message
                                        code="invoice.retention.label" default="Ret.:" /></label>
                                <div class="col-lg-2">
                                    <g:textField name="invoiceRetention" class="form-control"
                                                 value="${invoiceInstance?.totalInvoiceRetention}" />
                                </div>
                            </div>
							<div class="form-group">
								<label class="col-lg-6">&nbsp;</label>
							    <label for="invoiceTotal" class="col-lg-2 control-label"><g:message
							            code="invoice.invoiceTotal.label" default="Total:" /></label>
							    <div class="col-lg-2">
							         <g:textField name="invoiceTotal" class="form-control"
							                     value="${invoiceInstance?.totalWithTaxes}" />
							    </div>
							</div>
                        </div>
                    </fieldset>
                    <br>
                    <fieldset>
                        <g:actionSubmit id="btnUpdate" class="save btn btn-primary"
                                        action="update"
                                        value="${message(code: 'default.button.update.label', default: 'Update')}" />
                        <g:actionSubmit id="btnDelete" class="btn btn-danger"
                                        action="delete"
                                        value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                        formnovalidate=""
                                        onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                    </fieldset>
                </g:form>
            </div>
        </div> <!-- span12 -->
    </div> <!-- row-fluid -->
</div> <!-- container-fluid -->
<g:render template="/layouts/footer" />
</body>
</html>
