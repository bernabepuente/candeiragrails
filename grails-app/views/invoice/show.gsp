<%@ page import="es.xoubin.domain.Customer" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
	</head>
	<body>
		<g:render template="/layouts/navbar" />
		<g:render template="/layouts/header" />
		<div class="container">
			<div class="row">
				<div class="col-lg-2">
                    <g:render template="/layouts/sidebar" model="['entityBase': costumerInstance]"/>
				</div>
				<div class="span10">
					<div id="show-costumer" class="content scaffold-show" role="main">
						<div class="page-header">
							<h2>
								${costumerInstance?.name}
							</h2>
						</div>
						<g:if test="${flash.message}">							
							<div class="alert alert-success message" role="status">
								<button type="button" class="close" data-dismiss="alert">×</button>
								${flash.message}
							</div>
						</g:if>
						<ul class="property-list costumer">
						
							<g:if test="${costumerInstance?.name}">
								<li class="fieldcontain">
									<div id="name-label" class="property-label">
										<g:message code="costumer.name.label" />
									</div>									
									<div class="property-value" aria-labelledby="name-label">
										<g:fieldValue bean="${costumerInstance}" field="name"/>
									</div>									
								</li>
							</g:if>
						
							<g:if test="${costumerInstance?.surname}">
								<li class="fieldcontain">
									<div id="surname-label" class="property-label">
										<g:message code="costumer.surname.label" />
									</div>									
									<div class="property-value" aria-labelledby="surname-label">
										<g:fieldValue bean="${costumerInstance}" field="surname"/>
									</div>									
								</li>
							</g:if>
						
											
						</ul>
						<sec:ifAnyGranted roles="ROLE_ADMIN, ROLE_USER">
								<g:form>
									<fieldset class="buttons">
										<g:hiddenField name="id" value="${costumerInstance?.id}" />
										<g:link class="edit btn" action="edit" id="${costumerInstance?.id}">
											<g:message code="default.button.edit.label" />
										</g:link>										
									</fieldset>
								</g:form>
							
						</sec:ifAnyGranted>
					</div>
				</div> <!-- span10 -->				
			</div> <!-- row-fluid -->
		</div> <!-- container-fluid -->
		<g:render template="/layouts/footer" />
	</body>
</html>
