<%@ page import="es.xoubin.domain.Customer"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'invoice.label', default: 'Invoice')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<g:render template="/layouts/navbar" />
	<g:render template="/layouts/header" />
	<div style="width: 90%">
		<div class="row-fluid">
			<div class="span12">
				<div id="list-costumer" class="content scaffold-list" role="main" style="width: 100%; margin:0 auto 0 auto;">
					<div class="page-header">
						<h2>
							<g:message code="invoice.list.label" />
						</h2>
					</div>
					<div>
						<form action="list" class="form-horizontal">
							<g:field class="well" type="text" name="nameBusqueda" />
							<g:submitButton class="btn btn-info" name="submit" value="Buscar" />
						</form>
					</div>
					<g:if test="${flash.message}">
						<div class="alert alert-success message" role="status">
							<button type="button" class="close" data-dismiss="alert">×</button>
							${flash.message}
						</div>
					</g:if>
					<table class="table table-stripped">
						<thead>
							<tr>
                                <g:sortableColumn property="reference"
                                                  title="${message(code: 'invoice.reference.label', default: 'Referencia')}" />
                                <g:sortableColumn property="customer"
                                                  title="${message(code: 'invoice.customer.label', default: 'Cliente')}" />
                               	<g:sortableColumn property="order"
                                                  title="${message(code: 'invoice.order.label', default: 'Dirección')}" />
                                <g:sortableColumn property="dateInvoice"
                                                  title="${message(code: 'invoice.dateInvoice.label', default: 'Fecha')}" />
                              	<g:sortableColumn property="totalWithTaxes"
                                                  title="${message(code: 'invoice.totalWithTaxes.label', default: 'Total')}" />
                                <g:sortableColumn property="reference"
                                                  title="${message(code: 'invoice.pdf.label', default: 'Factura')}" />
							</tr>
						</thead>
						<tbody class="table table-stripped">
							<g:each in="${invoiceInstanceList}" status="i"
								var="invoiceInstance">
                                <tr style="background-color: #d3d3d3;">
                                    <td>
                                    	<g:link action="edit" id="${invoiceInstance.id}">
                                        ${fieldValue(bean: invoiceInstance, field: "reference")}
                                    	</g:link>
                                   	</td>
                                   	<td>
                                    	<g:link action="edit" id="${invoiceInstance.id}">
                                        ${fieldValue(bean: invoiceInstance, field: "customer")}
                                    	</g:link>
                                   	</td>
                                   	<td>
                                    	<g:link action="edit" id="${invoiceInstance.id}">
                                    	<g:each in="${invoiceInstance.invoiceOrders}" status="j" var="order">
                                    		<g:if test="${j == 0}">
                                    			${fieldValue(bean: order, field: "name")}
                                    		</g:if>
                                    	</g:each>
                                        ${fieldValue(bean: invoiceInstance, field: "customer")}
                                    	</g:link>
                                   	</td>
                                   	<td>
						               <g:formatDate format="dd-MM-yyyy" date="${invoiceInstance?.dateInvoice}"/>
									</td>
                                   	<td>
                                    	<g:link action="edit" id="${invoiceInstance.id}">
                                        ${fieldValue(bean: invoiceInstance, field: "totalWithTaxes")}
                                    	</g:link>
                                   	</td>
                                   	<td>
                                        <g:if test="${invoiceInstance.tipo==1}">
                                            <g:jasperReport controller="Invoice" jasper="invoice_free" action="reportInvoice_free" format="pdf" name="">
                                                <input type="hidden" name="id_invoice" id="${invoiceInstance.id}" value="${invoiceInstance.id}" />
                                            </g:jasperReport>
                                        </g:if>
                                        <g:else>
                                            <g:jasperReport controller="Invoice" jasper="invoice" action="reportInvoice" format="pdf" name="">
                                                <input type="hidden" name="id_invoice" id="${invoiceInstance.id}" value="${invoiceInstance.id}" />
                                            </g:jasperReport>
                                        </g:else>
                                   	</td>
								</tr>
							</g:each>
						</tbody>
					</table>
					<div class="pagination">
						<g:paginate total="${totalCount}" params="[term: term, 'offset': offset]" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<g:render template="/layouts/footer" />
</body>
</html>
