<div class="list-group">
	<div class="alert alert-info">
    	<h4 class="list-group-item-heading">
    		<g:message code="spring.security.ui.role.users" />
    	</h4>
    	<p class="list-group-item-text">
    		<g:message code="sidebar.user.tip.label" />
    	</p>
  	</div>		  	
    <a href="${createLink(controller: 'user', action: 'search')}" class="list-group-item ${selected == 'search' ? 'active' : ''}">
    	<g:message code="sidebar.search.label" />
    	<g:if test="${selected == 'search'}">
    		<span class="glyphicon glyphicon-chevron-right pull-right"></span>
    	</g:if>
    </a>
    <a href="${createLink(controller: 'user', action: 'create')}" class="list-group-item ${selected == 'create' ? 'active' : ''}">
    	<g:message code="sidebar.new.label" />
    	<g:if test="${selected == 'create'}">
    		<span class="glyphicon glyphicon-chevron-right pull-right"></span>
    	</g:if>
    </a>
</div>