<div class="list-group">
	<div class="alert alert-info">
    	<h4 class="list-group-item-heading">
    		<g:message code="spring.security.ui.user.roles" />
    	</h4>
    	<p class="list-group-item-text">
    		<g:message code="sidebar.role.tip.label" />
    	</p>
  	</div>		  	
    <a href="${createLink(controller: 'role', action: 'search')}" class="list-group-item ${selected == 'search' ? 'active' : ''}">
    	<g:message code="sidebar.search.label" />
    	<g:if test="${selected == 'search'}">
    		<span class="glyphicon glyphicon-chevron-right pull-right"></span>
    	</g:if>
    </a>
    <a href="${createLink(controller: 'role', action: 'create')}" class="list-group-item ${selected == 'create' ? 'active' : ''}">
   		<g:message code="sidebar.new.label" />
   		<g:if test="${selected == 'create'}">
    		<span class="glyphicon glyphicon-chevron-right pull-right"></span>
    	</g:if>
    </a>
</div>