<nav class="navbar navbar-default navbar-static-top" role="navigation" style="background-color:darkgrey; color: #000000;">
    <div class="" style="width: 100%; background-color:darkgrey; color: #000000;">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <div></div>

        <!-- login/logout -->
            <sec:ifLoggedIn>
                <ul class="nav navbar-nav navbar-left">
                    <sec:ifAnyGranted roles="ROLE_ADMIN">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color: #000000;">
                                <i class="glyphicon glyphicon-user"></i>
                                <g:message code="navbar.agents.label" />
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="${createLink(controller: 'agent', action: 'list') }" style="color: #000000;">
                                        <g:message code="navbar.agentes.list.label" />
                                    </a>
                                </li>
                                <li>
                                    <a href="${createLink(controller: 'agent', action: 'create') }" style="color: #000000;">
                                        <g:message code="navbar.agentes.create.label" />
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </sec:ifAnyGranted>
                    <sec:ifAnyGranted roles="ROLE_ADMIN, ROLE_USER">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color: #000000;">
                                <i class="glyphicon glyphicon-user"></i>
                                <g:message code="navbar.customer.label" />
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="${createLink(controller: 'customer', action: 'list') }" style="color: #000000;">
                                        <g:message code="navbar.customer.list.label" />
                                    </a>
                                </li>
                                <li>
                                    <a href="${createLink(controller: 'customer', action: 'create') }" style="color: #000000;">
                                        <g:message code="navbar.customer.create.label" />
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </sec:ifAnyGranted>
                    <sec:ifAnyGranted roles="ROLE_ADMIN, ROLE_USER">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color: #000000;">
                                <i class="glyphicon glyphicon-home"></i>
                                <g:message code="navbar.inmuebles.label" />
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="${createLink(controller: 'property', action: 'list') }" style="color: #000000;">
                                        <g:message code="navbar.inmuebles.list.label" />
                                    </a>
                                </li>
                                <li>
                                    <a href="${createLink(controller: 'property', action: 'create') }" style="color: #000000;">
                                        <g:message code="navbar.inmuebles.create.label" />
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </sec:ifAnyGranted>
                    <sec:ifAnyGranted roles="ROLE_ADMIN, ROLE_USER">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color: #000000;">
                                <i class="glyphicon glyphicon-tasks"></i>
                                <g:message code="navbar.tipo_inmueble.label" />
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="${createLink(controller: 'tipoinmueble', action: 'list') }" style="color: #000000;">
                                        <g:message code="navbar.tipo_inmueble.list.label" />
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </sec:ifAnyGranted>
                    <sec:ifAnyGranted roles="ROLE_ADMIN, ROLE_DIRECTOR">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color: #000000;">
                                <i class="glyphicon glyphicon-usd"></i>
                                <g:message code="navbar.invoices.label" />
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="${createLink(controller: 'invoice', action: 'list') }" style="color: #000000;">
                                        <g:message code="navbar.invoices.list.label" />
                                    </a>
                                </li>
                                <li>
                                    <a href="${createLink(controller: 'invoice', action: 'create') }" style="color: #000000;">
                                        <g:message code="navbar.invoices.create.label" />
                                    </a>
                                </li>
                                <li>
                                    <a href="${createLink(controller: 'invoice', action: 'create_free') }" style="color: #000000;">
                                        <g:message code="navbar.invoices.create_free.label" />
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </sec:ifAnyGranted>
                    <sec:ifAnyGranted roles="ROLE_ADMIN, ROLE_DIRECTOR">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color: #000000;">
                                <i class="glyphicon glyphicon-globe"></i>
                                <g:message code="navbar.cities.label" />
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="${createLink(controller: 'city', action: 'list') }" style="color: #000000;">
                                        <g:message code="navbar.cities.list.label" />
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </sec:ifAnyGranted>
                    <sec:ifAnyGranted roles="ROLE_ADMIN, ROLE_DIRECTOR">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color: #000000;">
                                <i class="glyphicon glyphicon-list"></i>
                                <g:message code="navbar.zones.label" />
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="${createLink(controller: 'zone', action: 'list') }" style="color: #000000;">
                                        <g:message code="navbar.zones.list.label" />
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </sec:ifAnyGranted>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <sec:ifAnyGranted roles="ROLE_ADMIN">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color: #000000;">
                                <i class="glyphicon glyphicon-cog"></i>
                                <g:message code="navbar.admin.tools.label" />
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="${createLink(controller: 'user', action: 'search') }" style="color: #000000;">
                                        <g:message code="navbar.users.label" />
                                    </a>
                                </li>
                                <li>
                                    <a href="${createLink(controller: 'role', action: 'search') }" style="color: #000000;">
                                        <g:message code="navbar.roles.label" />
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </sec:ifAnyGranted>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color: #000000;">
                            <i class="glyphicon glyphicon-user"></i>
                            <sec:loggedInUserInfo field="username" />
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="${createLink(controller: 'logout') }" style="color: #000000;">
                                    <g:message code="logout.label" />
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </sec:ifLoggedIn>
        </div><!-- /.navbar-collapse -->
    </div>
</nav>