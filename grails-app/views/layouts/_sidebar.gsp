<div class="page-header" style="border-bottom: 0">
    <g:if test="${controllerName.equals('user')}">
        <g:render template="/layouts/sidebars/user" />
    </g:if>
    <g:if test="${controllerName.equals('role')}">
        <g:render template="/layouts/sidebars/role" />
    </g:if>
</div>