<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6" style="height: 100%;"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7" style="height: 100%;"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8" style="height: 100%;"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9" style="height: 100%;"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js" style="height: 100%;"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Grails"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'main.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'mobile.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 's2ui.css')}" type="text/css">
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'lightbox.css')}" type="text/css">
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'jquery-ui.css')}" type="text/css">
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'jquery.fileupload.css')}" type="text/css">
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'jquery.fileupload-ui.css')}" type="text/css">
		<g:layoutHead/>
		<r:require module="bootstrap" />
		<r:require module="jquery" />
		<r:require module="jquery-ui"/>
        <r:require modules="bootstrap-file-upload, bootstrap-image-gallery"/>
		<r:layoutResources />
		<style>		
			.spinner {
			    position: fixed;
			    top: 50%;
			    left: 50%;
			    margin-left: -50px; /* half width of the spinner gif */
			    margin-top: -50px; /* half height of the spinner gif */
			}
		</style>
	</head>
	<body style="width: 100%;height: 100%; background-color:#d3d3d3;">
		<g:layoutBody/>		
		<div id="spinner" class="spinner" style="display:none;"></div>
		<g:javascript library="spinner" />
		<g:javascript library="application"/>
		<r:layoutResources />
		<jq:jquery>
			var opts = {
			  lines: 13, // The number of lines to draw
			  length: 20, // The length of each line
			  width: 10, // The line thickness
			  radius: 30, // The radius of the inner circle
			  corners: 1, // Corner roundness (0..1)
			  rotate: 0, // The rotation offset
			  direction: 1, // 1: clockwise, -1: counterclockwise
			  color: '#000', // #rgb or #rrggbb or array of colors
			  speed: 1, // Rounds per second
			  trail: 60, // Afterglow percentage
			  shadow: false, // Whether to render a shadow
			  hwaccel: false, // Whether to use hardware acceleration
			  className: 'spinner', // The CSS class to assign to the spinner
			  zIndex: 2e9, // The z-index (defaults to 2000000000)
			  top: 'auto', // Top position relative to parent in px
			  left: 'auto' // Left position relative to parent in px
			};
			var target = document.getElementById('spinner');
			var spinner = new Spinner(opts).spin(target);		
		</jq:jquery>
	</body>
</html>