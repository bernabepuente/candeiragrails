<div id="deleteModal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
		 	<div class="modal-header">
		    	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		    	<h3 id="myModalLabel">
		    		<g:message code="delete.modal.header.message" />
		    	</h3>
		  	</div>
		  	<div class="modal-body">
		  		<div id="axMsg" class="alert alert-error hide" role="status"></div>
		  		<g:message code="delete.modal.body.message" args="[message(code: messageCode).toLowerCase(), entityBaseName, message(code: entityBasePropertyName)]" />
		  		<p />
		  		<div class="form-group">
					<label for="confirmName" class="control-label">						
					</label>
					<div class="col-lg-8">
						<input type="text" id="confirmName" placeholder="" class="form-control" autocomplete="off" />
					</div>
				</div>
		  	</div>
		  	<div class="modal-footer">
		    	<button class="btn btn-default" data-dismiss="modal">
		    		<g:message code="default.button.cancel.label" />
		    	</button>
		    	<g:actionSubmit class="btn btn-danger delete" id="deleteConfirm" controller="${controllerName}" action="delete" value="${message(code: 'default.button.delete.label')}" />
		  	</div>
		  	<jq:jquery>
		  		$(".deleteModal").on("click", function() {
		  			$("#axMsg").empty().hide();
		  			$("#confirmName").val("");
		  		});
		  		
		  		$("#deleteConfirm").on("click", function() {		  			
		  			if ( ($.trim($("#confirmName").val())) == ("${entityBaseName}") ) {
		  				return true;
		  			} else {							  				
		  				$("#axMsg").empty();
		                var msg = "${message(code: 'delete.modal.error.message')}";
		                $("#axMsg").addClass("alert alert-error").text(msg).show();					                        
		  				return false;
		  			}
		  		});
		  		
		  		/**
		  		*	Activamos el botón por defecto al pulsar intro
		  		*	cuando se muestra el modal, y desactivamos el submit del form principal.
		  		*/
		  		$('#deleteModal').on('show.bs.modal', function () {
		  			$("#btnUpdate").attr("disabled", "disabled");
				 	$("#confirmName").on("keyup", function(event){
					    if (event.keyCode == 13) {								    	
					        $("#deleteConfirm").click();
					    }
					});
				})
				
				/**
				*	Activamos el botón submit del form al cerrar el modal.
				*/
				$('#deleteModal').on('hidden.bs.modal', function () {
				  	$("#btnUpdate").removeAttr("disabled");
				})
		  	</jq:jquery>
		</div> <!-- /.modal-content -->
	</div> <!-- /.modal-dialog -->
</div> <!-- /#deleteModal -->