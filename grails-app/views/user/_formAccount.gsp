<!-- activo -->
<div class="form-group">
	<div class="col-lg-6">
		<div class="checkbox">
			<label for="enabled">
				<g:checkBox name="enabled" value="${user?.enabled}" />&nbsp;<g:message code="person.enabled.label" />      										
			</label>
		</div>
	</div>
</div>
		
		<!-- accountExpired -->
<div class="form-group">
	<div class="col-lg-6">
		<div class="checkbox">
			<label for="accountExpired">
				<g:checkBox name="accountExpired" value="${user?.accountExpired}" />&nbsp;<g:message code="person.accountExpired.label" />      										
			</label>
		</div>
	</div>
</div>
		
		<!-- accountLocked -->
<div class="form-group">
	<div class="col-lg-6">
		<div class="checkbox">
			<label for="accountLocked">
				<g:checkBox name="accountLocked" value="${user?.accountLocked}" />&nbsp;<g:message code="person.accountLocked.label" />      										
			</label>
		</div>
	</div>
</div>
		
		<!-- passwordExpired -->
<div class="form-group">
	<div class="col-lg-6">
		<div class="checkbox">
			<label for="passwordExpired">
				<g:checkBox name="passwordExpired" value="${user?.passwordExpired}" />&nbsp;<g:message code="person.passwordExpired.label" />      										
			</label>
		</div>
	</div>
</div>