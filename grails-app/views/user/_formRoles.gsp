<div class="list-group">
 	<g:each var="auth" in="${authorityList}">
 		<div class="list-group-item">
 				<g:if test="${1 == 0}">
 					<!-- Posibilidad de redirigir al action de edicion de role. Desactivado de momento. -->
				<a href="${createLink(controller: 'role', action: 'edit', id: auth.id)}" class="list-group-item">
					<g:checkBox name="${auth.authority}" />
					${auth.authority.encodeAsHTML()}
				</a>
			</g:if>
			<g:else>
				<a href="#" class="list-group-item">
					<g:checkBox name="${auth.authority}" />
					${auth.authority.encodeAsHTML()}
				</a>
			</g:else>
		</div>
	</g:each>
</div>