<html>
	<head>
		<meta name='layout' content='main'/>	
	</head>
	<body>
		<g:render template="/layouts/navbar" />
		<g:render template="/layouts/header" />
		<div class="container">
			<div class="row">
				<div class="col-lg-2">
					<g:render template="/layouts/sidebar" model="['selected': 'search']" />
				</div>
				<div class="col-lg-10">
					<div id="search-user" class="content scaffold-edit" role="main">
						<div class="page-header">
							<h2>
								<g:message code="spring.security.ui.user.search" />
							</h2>
							<g:if test="${flash.message}">
								<div class="alert alert-info" role="status">
									<button type="button" class="close" data-dismiss="alert">×</button>
									${flash.message}
									${flash.clear()}
								</div>
							</g:if>
						</div>
						<g:form action='userSearch' name='userSearchForm' class="form-horizontal">
							<div class="form-group">
								<label for="username" class="col-lg-2 control-label">
									<g:message code="person.username.label" />:
								</label>
								<div class="col-lg-6">
									<g:textField name='term' size='50' maxlength='255' autocomplete='off' value='' class="form-control" style="margin-bottom: 0"/>								
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-offset-2 col-lg-4">
									<g:actionSubmit value="${message(code: 'spring.security.ui.search')}" action="userSearch" class="btn btn-default" />
								</div>
							</div>
						</g:form>		
					</div> <!-- ./content -->
				</div> <!-- ./col-lg-10 -->
			</div> <!-- ./row -->
			<g:if test='${searched}'>
				<div class="row">
					<div class="col-lg-12">					
						<g:render template="list" model="[results: results, totalCount: totalCount]" />
					</div>
				</div>
			</g:if>
		</div> <!-- ./container -->	
	</body>
</html>