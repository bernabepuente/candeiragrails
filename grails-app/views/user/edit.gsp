<html>
	
	<head>
		<meta name='layout' content='main'/>
		<g:javascript library="validator" />		
	</head>
	
	<body>
		<g:render template="/layouts/navbar" />
		<g:render template="/layouts/header" />
		<div class="container">
			<div class="row">
				<div class="col-lg-2">
					<g:render template="/layouts/sidebar" model="['selected': 'edit']" />
				</div>
				<div class="col-lg-10">
					<div id="edit-user" class="content scaffold-edit" role="main">
						<div class="page-header">
							<h2>
								<g:message code="spring.security.ui.user.edit" />
								<span class="text-info">${user?.toString()}</span>
							</h2>
							<g:if test="${flash.message}">
								<div class="alert alert-info" role="status">
									<button type="button" class="close" data-dismiss="alert">×</button>
									${flash.message}
									${flash.clear()}
								</div>
							</g:if>
						</div>
						<g:form id="userForm" name='userForm' class="button-style form-horizontal">
							<g:hiddenField name="uId" value="${user?.id}"/>
							<g:hiddenField name="version" value="${user?.version}"/>
							
							<ul class="nav nav-tabs" id="myTab">
	  							<li class="active">
	  								<a href="#userinfo" data-toggle="tab"><g:message code="spring.security.ui.user.info" /></a>
	  							</li>
	  							<li>
	  								<a href="#accountoptions" data-toggle="tab">
	  									<g:message code="spring.security.ui.user.account.options" />
	  								</a>
	  							</li>
	  							<li>
	  								<a href="#roles" data-toggle="tab"><g:message code="spring.security.ui.user.roles" /></a>
	  							</li>
							</ul>
	 
							<div class="tab-content">
							
								<!-- responsible INFO -->
							  	<div class="tab-pane active" id="userinfo">
									<br />
							  		<g:render template="form" />		  		
							  	</div>
							  	
							  	<!-- USER ACCOUNT OPTIONS -->
								<div class="tab-pane" id="accountoptions">
									<br />
									<g:render template="formAccount" />
								</div>
							  	
							  	<div class="tab-pane" id="roles">
							  		<br />
							  		<g:render template="formEditRoles" />
							  	</div>
							</div>
							
							<g:actionSubmit class="btn btn-primary save" id="btnUpdate" action="update" value="${message(code: 'default.button.update.label')}" />							
							
							<g:if test='${user}'>
								<a href="#deleteModal" role="button" class="btn btn-danger deleteModal" data-toggle="modal">
									<g:message code="default.button.delete.label" />
								</a>
								<!-- Delete modal -->
								<g:render 	template="/layouts/deleteModal" 
											model="['entityBase': user, 'entityBaseName': user?.username, 'messageCode': 'user.label', 'entityBasePropertyName': 'user.entity.confirm.property.name']" />								
							</g:if>
							
						</g:form>
						
						<jq:jquery>
							
						</jq:jquery>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>