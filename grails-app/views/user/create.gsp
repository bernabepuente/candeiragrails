<html>

	<head>
		<meta name="layout" content="main" />
		<g:javascript library="validator" />		
	</head>

	<body>
		<g:render template="/layouts/navbar" />
		<g:render template="/layouts/header" />
		<div class="container">
			<div class="row">
				<div class="col-lg-2">
					<g:render template="/layouts/sidebar" model="['selected': 'create']" />
				</div>
				<div class="col-lg-10">	
				
					<div id="edit-user" class="content scaffold-edit" role="main">
						<div class="page-header">
							<h2>
								<g:message code="spring.security.ui.user.create" />
							</h2>
							<g:if test="${flash.message}">
								<div class="alert alert-info" role="status">
									<button type="button" class="close" data-dismiss="alert">×</button>
									${flash.message}
									${flash.clear()}
								</div>
							</g:if>
						</div>
		
						<g:form id="userForm" name="userForm" class="form-horizontal" action="save">
							
							<ul class="nav nav-tabs" id="myTab">
	  							<li class="active">
	  								<a href="#userinfo" data-toggle="tab"><g:message code="spring.security.ui.user.info" /></a>
	  							</li>
	  							<li>
	  								<a href="#accountoptions" data-toggle="tab">
	  									<g:message code="spring.security.ui.user.account.options" />
	  								</a>
	  							</li>
	  							<li>
	  								<a href="#roles" data-toggle="tab"><g:message code="spring.security.ui.user.roles" /></a>
	  							</li>
							</ul>
							
							<div class="tab-content">
								
								<!-- user info -->
							  	<div class="tab-pane active" id="userinfo">
							  		<br />
							  		<g:render template="form" />
								</div>
								
								<!-- USER ACCOUNT OPTIONS -->
								<div class="tab-pane" id="accountoptions">
									<br />
									<g:render template="formAccount" />
								</div>
								
								<!-- ROLES -->
								<div class="tab-pane" id="roles">
									<br />									
									<g:render template="formRoles" />
								</div>
							</div>							
							
							<g:actionSubmit id="btnSave" action="save" value="${message(code: 'default.button.create.label')}" class="btn btn-primary"  />
							
						</g:form>
					</div>
				</div>
			</div>
		</div>
		
		<jq:jquery>			
			$('#username').focus();
		</jq:jquery>
	</body>
</html>