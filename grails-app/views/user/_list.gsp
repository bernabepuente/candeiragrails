<div class="list">
	<table class="table table-hover table-condensed">
		<thead>
			<tr>
				<g:sortableColumn property="username" title="${message(code: 'person.username.label')}" />
				<g:sortableColumn property="name" title="${message(code: 'person.name.label')}" />
				<g:sortableColumn property="surname" title="${message(code: 'person.surname.label')}" />
				<g:sortableColumn property="nif" title="${message(code: 'person.nif.label')}" />
			</tr>
		</thead>							
		<tbody>
			<g:each in="${results}" status="i" var="person">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="edit" id="${person.id}">${fieldValue(bean: person, field: "username")}</g:link></td>
					<td>${fieldValue(bean: person, field: "name")}</td>
					<td>${fieldValue(bean: person, field: "surname")}</td>
					<td>${fieldValue(bean: person, field: "nif")}</td>
				</tr>
			</g:each>
		</tbody>
	</table>
</div>

<div class="paginateButtons">
	<g:paginate total="${totalCount}" />
</div>