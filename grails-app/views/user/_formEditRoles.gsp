<div class="list-group">
 	<g:each var="entry" in="${roleMap}">
 		<div class="list-group-item">
 			<g:if test="${1 == 0}">
 				<!-- Posibilidad de redirigir al action de edicion de role. Desactivado de momento. -->
				<a href="${createLink(controller: 'role', action: 'edit', id: entry.key.id)}" class="list-group-item">
					<g:checkBox name="${entry.key.authority}" value="${entry.value}"/>
					${entry.key.authority.encodeAsHTML()}
				</a>
			</g:if>
			<g:else>
				<a href="#" class="list-group-item">
					<g:checkBox name="${entry.key.authority}" value="${entry.value}"/>
					${entry.key.authority.encodeAsHTML()}
				</a>
			</g:else>
		</div>
	</g:each>
</div>