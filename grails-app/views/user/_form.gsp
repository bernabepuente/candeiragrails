<!-- username/login -->
<div class="form-group has-warning">
	<label for="username" class="col-lg-2 control-label">
		<g:message code="person.username.label" />:
	</label>
	<div class="col-lg-6">
		<g:textField name='username' size='50' maxlength='255' autocomplete='off' value='${user?.username}' class="form-control" style="margin-bottom: 0" />								
	</div>
</div>

<!-- password -->
<div class="form-group has-warning">
    <label for="password" class="col-lg-2 control-label">
        <g:message code="person.password.label" />:
    </label>
    <div class="col-lg-4">
        <g:passwordField id="password" name='password' size='50' maxlength='255' autocomplete='off' value='${user?.password}' class="form-control" style="margin-bottom: 0"/>
    </div>
    <label for="password has-warning" class="col-lg-2 control-label">
        <g:message code="person.repeat.password.label" />:
    </label>
    <div class="col-lg-4">
        <g:passwordField id="repeatPassword" name='repeatPassword' size='50' maxlength='255' autocomplete='off' value='' class="form-control" style="margin-bottom: 0"/>
    </div>
</div>

<!-- name -->
<div class="form-group">
	<label for="name" class="col-lg-2 control-label">
		<g:message code="person.name.label" />:
	</label>
	<div class="col-lg-4">
		<g:textField name='name' size='50' maxlength='255' autocomplete='off' value='${user?.name}' class="form-control" style="margin-bottom: 0"/>								
	</div>
	<label for="surname" class="col-lg-2 control-label">
		<g:message code="person.surname.label" />:
	</label>
	<div class="col-lg-4">
		<g:textField name='surname' size='50' maxlength='255' autocomplete='off' value='${user?.surname}' class="form-control" style="margin-bottom: 0"/>								
	</div>
</div>

<jq:jquery>
	$("#userForm").validate({
		rules: {
			username: "required",
			name: "required",
			password: "required",
			repeatPassword: {
				required: true,
				equalTo: "#password"
			}
		},
		messages: {
			username: "${message(code: 'person.property.required')}",
			name: "${message(code: 'person.property.required')}",
			password: "${message(code: 'person.property.required')}",
			repeatPassword: {
					required: 	"${message(code: 'person.property.required')}",
					equalTo: 	"${message(code: 'person.password.not.match')}"
			}
		}
	});
</jq:jquery>