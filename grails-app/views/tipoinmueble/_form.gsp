<g:javascript library="mustache" />
<div class="row-fluid">
    <div class="span12">
        <div id="list-servicio" class="content scaffold-list" role="main">
            <button type="button"
                    id="btnRenderRowTipoInmueble"
                    data-button='{"id": ""}'
                    class="btn btn-success btnAddTipoInmueble">
                <span class="glyphicon glyphicon-plus"></span>
            </button>
            <table class="table table-stripped">
                <thead>
                <tr>
                    <g:sortableColumn property="nombre" title="${message(code: 'tipo_inmueble.name.label')}" />
                    <th />
                </tr>
                </thead>
                <tbody id="tipos_list">
                <g:each in="${tipoInstanceList}" status="i" var="tipoInstance">
                    <tr id="tipoInmuebleRowSelected_${tipoInstance.id}">
                        <td>${fieldValue(bean: tipoInstance, field: "name")}</td>
                        <td>
                            <button type="button"
                                    data-button='{"id":  ${tipoInstance.id}}'
                                    class="btn btn-danger btnRemoveTipoInmueble">
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>

        </div> <!-- id=list... -->
    </div> <!-- span12 -->
</div>
<jq:jquery>
    var template;
	var templateFilePath = '<g:resource dir="templates" file="tipo-inmueble-row.htm" />';

	$.get(templateFilePath, function(templates) {
		template = $(templates);
		//console.log("Plantillas cargadas");
	});

	function renderTipo_InmuebleRow(){
        var buttonData = $.parseJSON($(this).attr('data-button'));
        var t = template.filter('#tipo-inmueble-row-available').html();
        $('#tipos_list').append(Mustache.render(t, buttonData));
	}

    function addTipo_Inmueble() {
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/tipoinmueble/_addTipo",
			dataType: 'json',
			data: {
				name: $('#txtName').val()
			},
			success: function(data) {
				if (data.success == "OK") {
				    $("#tipoInmuebleRowAvailable").detach();
					var t = template.filter('#tipo-inmueble-row-selected').html();
					$('#tipos_list').append(Mustache.render(t, data));
				} else {
					if (data.success == "FAIL") {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}

	function removeTipo_Inmueble() {
	    var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/tipoinmueble/_removeTipo",
			dataType: 'json',
			data: {
				id: buttonData.id
			},
			success: function(data) {
				if (data.success == "OK") {
					$("#tipoInmuebleRowSelected_" + data.id).detach();
				} else {
					if (data.errorMsg) {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}

    $(document).on("click", "#btnRenderRowTipoInmueble", renderTipo_InmuebleRow);
    $(document).on("click", ".btnSaveTipoInmueble", addTipo_Inmueble);
    $(document).on("click", ".btnRemoveTipoInmueble", removeTipo_Inmueble);
</jq:jquery>