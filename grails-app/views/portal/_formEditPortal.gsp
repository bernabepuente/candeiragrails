<g:javascript library="mustache" />
<div class="row-fluid">
    <div class="span12">
        <div id="list-servicio" class="content scaffold-list" role="main">
            <button type="button"
                    id="btnRenderRowPortal"
                    data-button='{"idProperty": "${propertyInstance?.id}"}'
                    class="btn btn-success btnAddPortal">
                <span class="glyphicon glyphicon-plus"></span>
            </button>
            <table class="table table-stripped">
                <thead>
                <tr>
                    <g:sortableColumn property="nombre" title="${message(code: 'portal.name.label')}" />
                    <g:sortableColumn property="link" title="${message(code: 'portal.link.label')}" />
                    <th />
                </tr>
                </thead>
                <tbody id="portals_list">
                <g:each in="${propertyInstance?.portals}" status="i" var="portalInstance">
                    <tr id="portalRowSelected_${portalInstance.id}">
                        <td><g:textField name="txtName_${portalInstance.id}" id="txtName_${portalInstance.id}" class="form-control"
                     		value="${portalInstance?.name}" /></td>
                        <td><a href="${portalInstance.link}" value="${portalInstance?.link}" name="txtLink_${portalInstance.id}" id="txtLink_${portalInstance.id}" class="form-control">${portalInstance.link}</a></td>
                   		<td>
                        	<button type="button"
                                    data-button='{"id":  ${portalInstance.id}}'
                                    class="btn btn-success btnUpdatePortal">
                                <span class="glyphicon glyphicon-check"></span>
                            </button>
                        </td>
                        <td>
                            <button type="button"
                                    data-button='{"id":  ${portalInstance.id}}'
                                    class="btn btn-danger btnRemovePortal">
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>

        </div> <!-- id=list... -->
    </div> <!-- span12 -->
</div>
<jq:jquery>
    var template;
	var templateFilePath = '<g:resource dir="templates" file="portals-row.htm" />';

	$.get(templateFilePath, function(templates) {
		template = $(templates);
		//console.log("Plantillas cargadas");
	});

	function renderPortalRow(){
        var buttonData = $.parseJSON($(this).attr('data-button'));
        var t = template.filter('#portal-row-available').html();
        $('#portals_list').append(Mustache.render(t, buttonData));
	}

    function addPortal() {
		var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/portal/_addPortal",
			dataType: 'json',
			data: {
				idProperty: buttonData.idProperty,
				name: $('#txtName__' + buttonData.idProperty).val(),
				link: $('#txtLink__' + buttonData.idProperty).val()
			},
			success: function(data) {
				if (data.success == "OK") {
				    $("#portalRowAvailable_" + data.idProperty).detach();
					var t = template.filter('#portal-row-selected').html();
					$('#portals_list').append(Mustache.render(t, data));
				} else {
					if (data.success == "FAIL") {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}
	
	function updatePortal() {
		var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/portal/_updatePortal",
			dataType: 'json',
			data: {
				id: buttonData.id,
				name: $('#txtName_' + buttonData.id).val(),
				link: $('#txtLink_' + buttonData.id).text()
			},
			success: function(data) {
				if (data.success == "OK") {
					alert('Datos guardados.');
				    
				} else {
					if (data.success == "FAIL") {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}

	function removePortal() {
	    var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/portal/_removePortal",
			dataType: 'json',
			data: {
				id: buttonData.id
			},
			success: function(data) {
				if (data.success == "OK") {
					$("#portalRowSelected_" + data.id).detach();
				} else {
					if (data.errorMsg) {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}

    $(document).on("click", "#btnRenderRowPortal", renderPortalRow);
    $(document).on("click", ".btnSavePortal", addPortal);
    $(document).on("click", ".btnRemovePortal", removePortal);
    $(document).on("click", ".btnUpdatePortal", updatePortal);
</jq:jquery>