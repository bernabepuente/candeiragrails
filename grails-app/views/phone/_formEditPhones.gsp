<g:javascript library="mustache" />
<div class="row-fluid">
    <div class="span12">
        <div id="list-servicio" class="content scaffold-list" role="main">
            <button type="button"
                    id="btnRenderRowPhone"
                    data-button='{"idPerson": "${personInstance?.id}"}'
                    class="btn btn-success btnAddPhone">
                <span class="glyphicon glyphicon-plus"></span>
            </button>
            <table class="table table-stripped">
                <thead>
                <tr>
                    <g:sortableColumn property="name" title="${message(code: 'phone.name.label')}" />
                    <g:sortableColumn property="number" title="${message(code: 'phone.number.label')}" />
                    <th />
                </tr>
                </thead>
                <tbody id="phones_list">
                <g:each in="${personInstance?.phones}" status="i" var="phoneInstance">
                    <tr id="phoneRowSelected_${phoneInstance.id}">
                        <td>${fieldValue(bean: phoneInstance, field: "name")}</td>
                        <td>${fieldValue(bean: phoneInstance, field: "number")}</td>
                        <td>
                            <button type="button"
                                    data-button='{"id":  ${phoneInstance.id}}'
                                    class="btn btn-danger btnRemovePhone">
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </td>

                    </tr>
                </g:each>
                </tbody>
            </table>
            <div class="pagination">
                <g:paginate total="${personInstance?.phones?.size()}" />
            </div>
        </div> <!-- id=list... -->
    </div> <!-- span12 -->
</div>
<jq:jquery>
    var template;
	var templateFilePath = '<g:resource dir="templates" file="phone-row.htm" />';

	$.get(templateFilePath, function(templates) {
		template = $(templates);
		//console.log("Plantillas cargadas");
	});

	function renderPhoneRow(){
        var buttonData = $.parseJSON($(this).attr('data-button'));
        var t = template.filter('#phone-row-available').html();
        $('#phones_list').append(Mustache.render(t, buttonData));
	}

    function addPhone() {
		var buttonData = $.parseJSON($(this).attr('data-button'));
		//console.log(buttonData.idResponsible);
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/phone/_addPhone",
			dataType: 'json',
			data: {
				idPerson: buttonData.idPerson,
				name: $('#txtName_' + buttonData.idPerson).val(),
				number: $('#txtNumber_' + buttonData.idPerson).val()
			},
			success: function(data) {
				if (data.success == "OK") {
				    $("#phoneRowAvailable_" + data.idPerson).detach();
					var t = template.filter('#phone-row-selected').html();
					$('#phones_list').append(Mustache.render(t, data));
				} else {
					if (data.success == "FAIL") {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}

	function removePhone() {
	    var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/phone/_removePhone",
			dataType: 'json',
			data: {
				id: buttonData.id
			},
			success: function(data) {
				if (data.success == "OK") {
					$("#phoneRowSelected_" + data.id).detach();
				} else {
					if (data.errorMsg) {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}

    $(document).on("click", "#btnRenderRowPhone", renderPhoneRow);
    $(document).on("click", ".btnSavePhone", addPhone);
    $(document).on("click", ".btnRemovePhone", removePhone);
</jq:jquery>