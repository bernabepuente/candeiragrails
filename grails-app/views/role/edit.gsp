<html>

	<head>
		<meta name='layout' content='main'/>
	</head>

	<body>
		<g:render template="/layouts/navbar" />
		<g:render template="/layouts/header" />
		<div class="container">
			<div class="row">
				<div class="col-lg-2">
					<g:render template="/layouts/sidebar" model="['selected': 'edit']" />
				</div>
				<div class="col-lg-10">		
					<div class="page-header">
						<h2><g:message code="spring.security.ui.role.edit" /></h2>
					</div>
		
					<g:form action="update" name='roleEditForm' class="form-horizontal">
						<g:hiddenField name="id" value="${role?.id}"/>
						<g:hiddenField name="version" value="${role?.version}"/>
		
						<ul class="nav nav-tabs" id="myTab">
  							<li class="active">
  								<a href="#roleinfo" data-toggle="tab"><g:message code="spring.security.ui.role.info" /></a>
  							</li>
  							<li>
  								<a href="#users" data-toggle="tab"><g:message code="spring.security.ui.role.users" /></a>
  							</li>
						</ul>
						
						<div class="tab-content">
						  	<div class="tab-pane active" id="roleinfo">
						  		<br />
						  		<!-- role name -->
						  		<div class="form-group">
									<label for="authority" class="col-lg-2 control-label">
										<g:message code="role.authority.label" />
									</label>
									<div class="col-lg-6">
										<g:textField name='authority' size='50' maxlength='255' autocomplete='off' value='${role?.authority}' class="form-control" style="margin-bottom: 0"/>								
									</div>
								</div>
							</div>
							<div class="tab-pane" id="users">
								<br />
								<div class="list-group">
									<g:each var="u" in="${users}">
										<div class="list-group-item">											
											${u.username.encodeAsHTML()}											
										</div>
									</g:each>
								</div>
							</div>
						</div>		
						
						<s2ui:submitButton elementId='update' form='roleEditForm' messageCode='default.button.update.label' class="btn btn-primary" />		
						<g:if test='${role}'>
							<s2ui:deleteButton />
						</g:if>
						
						<script>
							$(document).ready(function() {
								$('#deleteButton').addClass("btn btn-danger");
							});
						</script>
					</g:form>
		
					<g:if test='${role}'>
						<s2ui:deleteButtonForm instanceId='${role.id}'/>
					</g:if>
				</div><!-- span12 -->
			</div><!-- row -->
		</div><!-- container -->		
	</body>
</html>