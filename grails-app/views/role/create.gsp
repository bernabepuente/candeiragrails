<html>

	<head>
		<meta name='layout' content='main' />		
	</head>

	<body>
		<g:render template="/layouts/navbar" />
		<g:render template="/layouts/header" />
		<div class="container">
			<div class="row">
				<div class="col-lg-2">
					<g:render template="/layouts/sidebar" model="['selected': 'create']" />
				</div>
				<div class="col-lg-10">
					<div class="page-header">
						<h2>
							<g:message code="spring.security.ui.role.create" />
						</h2>
						<g:if test="${flash.message}">
							<div class="alert alert-info" role="status">
								<button type="button" class="close" data-dismiss="alert">×</button>
								${flash.message}
								${flash.clear()}
							</div>
						</g:if>
					</div>
						
					<g:form action="save" name='roleCreateForm' class="form-horizontal">
						<br />
				  		<!-- role name -->
				  		<div class="form-group">
							<label for="username" class="col-lg-2 control-label">
								<g:message code="role.authority.label" />
							</label>
							<div class="col-lg-6">
								<g:textField name='authority' size='50' maxlength='255' autocomplete='off' value='${role?.authority}' class="form-control" style="margin-bottom: 0"/>								
							</div>
						</div>
						<s2ui:submitButton elementId='create' form='roleCreateForm' messageCode='default.button.create.label' class="btn btn-primary" />					
					</g:form>
				</div>
			</div>
		</div>	
	
		<script>
			$(document).ready(function() {
				$('#authority').focus();
			});
		</script>
	
	</body>
</html>
