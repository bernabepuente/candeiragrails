<html>
	<head>
		<meta name='layout' content='main'/>	
	</head>	
	<body>
		<g:render template="/layouts/navbar" />
		<g:render template="/layouts/header" />
		<div class="container">
			<div class="row">
				<div class="col-lg-2">
					<g:render template="/layouts/sidebar" model="['selected': 'search']" />
				</div>
				<div class="col-lg-10">
					<div class="page-header">
						<h2><g:message code="spring.security.ui.role.search" /></h2>
					</div>
					<g:form name='roleSearchForm' class="form-horizontal">		
						<br/>
						
						<div class="form-group">
							<label for="username" class="col-lg-2 control-label">
								<g:message code='role.authority.label' />
							</label>
							<div class="col-lg-6">
								<g:textField name='authority' class='form-control textField' size='50' maxlength='255' autocomplete='on' value='${authority}' style="margin-bottom: 0" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-offset-2 col-lg-4">
								<g:actionSubmit value="${message(code: 'spring.security.ui.search')}" action="roleSearch" class="btn btn-default" />
							</div>
						</div>
					</g:form>					
				</div> <!-- span12 -->
			</div> <!-- row -->
			<div class="row">
				<div class="col-lg-12">
					<g:if test='${searched}'>
						<%
						def queryParams = [authority: authority]
						%>
		
						<div class="list">
							<table class="table table-hover table-condensed">
								<thead>
									<tr>
										<g:sortableColumn property="authority" title="${message(code: 'role.authority.label')}" params="${queryParams}"/>
									</tr>
								</thead>
						
								<tbody>
									<g:each in="${results}" status="i" var="role">
										<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
											<td><g:link action="edit" id="${role.id}">${fieldValue(bean: role, field: "authority")}</g:link></td>
										</tr>
									</g:each>
								</tbody>
							</table>
						</div>
								
						<div class="paginateButtons">
							<g:paginate total="${totalCount}" params="${queryParams}" />
						</div>
					
						<div style="text-align:center">
							<s2ui:paginationSummary total="${totalCount}"/>
						</div>		
					</g:if>
				</div>
			</div>
		</div> <!-- container -->
	</body>
</html>
