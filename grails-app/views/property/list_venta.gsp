<%@ page import="es.xoubin.domain.Property"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'property.label', default: 'Property')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<g:render template="/layouts/navbar" />
	<g:render template="/layouts/header" />
	<div class="container" style="width: 100%; margin:0 auto 0 auto;">
		<div class="row-fluid">
			<div class="span12">
				<div id="list-property" class="content scaffold-list" role="main"  style="width: 100%; margin:0 auto 0 auto;">
					<div class="page-header">
						<h2>
							<g:message code="property.list.label" />
						</h2>
					</div>
					<div>
						<form action="list" class="form-horizontal">
							<g:field class="well" type="text" name="term" autocomplete='off' value='${term}'/>
							<g:submitButton class="btn btn-info" name="submit" value="Buscar" />
						</form>
					</div>
					<g:if test="${flash.message}">
						<div class="alert alert-success message" role="status">
							<button type="button" class="close" data-dismiss="alert">×</button>
							${flash.message}
						</div>
					</g:if>
					<table class="table table-stripped">
						<thead>
							<tr>
								<g:sortableColumn property="referencia"
									              title="${message(code: 'property.referencia_list.label', default: 'Referencia')}" params="['term': term, 'offset': offset]"/>
                                <g:sortableColumn property="address"
                                                  title="${message(code: 'property.address.label', default: 'Dirección')}" params="['term': term, 'offset': offset]"/>
                                <g:sortableColumn property="precio_deseado"
                                                  title="${message(code: 'property.precio_deseado.list.label', default: 'Precio')}" params="['term': term, 'offset': offset]"/>
                                <g:sortableColumn property="images"
                                                  title="${message(code: 'property.images.label', default: 'Imágenes')}" params="['term': term, 'offset': offset]"/>
							</tr>
						</thead>
						<tbody>
							<g:each in="${propertyInstanceList}" status="i"
								var="propertyInstance">
                                    <tr style="padding: 4% 0;" class="${(i % 2) == 0 ? 'even' : 'odd'}">
                                        <td style="vertical-align: middle;font-size: 14px;"><g:link action="edit" style="font-size: large;" id="${propertyInstance?.id}" params="['term': term, 'offset': offset]">
                                                ${fieldValue(bean: propertyInstance, field: "referencia")}
                                            </g:link></td>
                                        <td style="vertical-align: middle;font-size: 14px;"><g:link style="font-size: large;" action="edit" id="${propertyInstance?.id}" params="['term': term, 'offset': offset]">
                                            ${fieldValue(bean: propertyInstance, field: "address")}
                                        </g:link></td>

                                        <td style="width: 100px;font-weight: bold; font-size: large;vertical-align: middle;">${propertyInstance?.precio_deseado} <g:if test="${propertyInstance?.precio_deseado}">€</g:if></td>
                                        <td>
                                            <g:each in="${propertyInstance.images.sort{a,b->a.id.compareTo(b.id)}}" var="imageInstance" status="z">
                                                <g:if test="${z == 0}">
                                                    <a href="${createLink(controller: 'image', action: 'getImg', id: imageInstance?.id)}" data-lightbox="gallery" title="">
                                                        <img style="padding: 2px 2px 2px 2px;" src="${createLink(controller: 'image', action: 'getImg', id: imageInstance?.id)}" name="grande" width="600" height="450" id="grande" />
                                                    </a>
                                                </g:if>
                                            </g:each>
                                        </td>
								    </tr>
							</g:each>
						</tbody>
					</table>
					<div class="pagination">
						<g:paginate total="${totalCount}" params="[term: term, 'offset': offset]" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<g:render template="/layouts/footer" />
</body>
</html>
