<%@ page import="es.xoubin.domain.Property"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main">
        <g:set var="entityName"
        value="${message(code: 'property.label', default: 'Property')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <g:render template="/layouts/navbar" />
        <g:render template="/layouts/header" />
        <div style="width: 90%; margin: 0 auto 0 auto;">
            <div class="row-fluid">
                <div class="span12">
                    <div id="list-property" class="content scaffold-list" role="main"
                    style="width: 100%; margin: 0 auto 0 auto;">
                    <div class="page-header">
                        <h2>
                            <g:message code="property.list.label" />
                        </h2>
                    </div>
                    <div style="float: left; width: 100%;">
                        <div style="float: left">
                            <form action="list" class="form-horizontal">
                                <g:field class="well" type="text" name="term" autocomplete='off'
                                    value='${term}' />
                                <g:submitButton class="btn btn-info" name="submit"
                                    value="Buscar" />
                            </form>
                        </div>
                        <div style="float: left">&nbsp;&nbsp;&nbsp;</div>
                        <div style="float: left">
                            &nbsp;
                            <g:jasperReport controller="property" jasper="list"
                                action="reportList" format="pdf" name="">
                                <input type="hidden" name="busqueda" id="busqueda"
                                value="${term}" />
                            </g:jasperReport>
                        </div>
                        <div style="float: left">&nbsp;&nbsp;&nbsp;</div>
                        <div style="float: left; width: 1000px;">
                            <g:if test="${flash.message}">
                                <div class="alert alert-success message" role="status">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    ${flash.message}
                                </div>
                            </g:if>
                        </div>
                    </div>
                    <table class="table table-stripped">
                        <thead>
                            <tr>
                                <g:sortableColumn property="referencia"
                                title="${message(code: 'property.referencia_list.label', default: 'Referencia')}"
                                    params="['term': term, 'offset': offset, 'sort': sort, 'order': order]" />
                                <g:sortableColumn property="estado"
                                title="${message(code: 'property.estado.label', default: 'Estado')}"
                                    params="['term': term, 'offset': offset, 'sort': sort, 'order': order]" />
                                <g:sortableColumn property="Web"
                                title="${message(code: 'property.web.label', default: 'Web')}"
                                    params="['term': term, 'offset': offset, 'sort': sort, 'order': order]" />
                                <g:sortableColumn property="operacion"
                                title="${message(code: 'property.operacion.label', default: 'Operación')}"
                                    params="['term': term, 'offset': offset, 'sort': sort, 'order': order]" />
                                <g:sortableColumn property="tipo"
                                title="${message(code: 'property.tipo.label', default: 'Tipo')}"
                                    params="['term': term, 'offset': offset, 'sort': sort, 'order': order]" />
                                <g:sortableColumn property="address"
                                title="${message(code: 'property.address.label', default: 'Dirección')}"
                                    params="['term': term, 'offset': offset, 'sort': sort, 'order': order]" />
                                <g:sortableColumn property="phone"
                                title="${message(code: 'property.phone.label', default: 'Teléfono')}"
                                    params="['term': term, 'offset': offset, 'sort': sort, 'order': order]" />
                                <g:sortableColumn property="completename"
                                title="${message(code: 'property.completename.label', default: 'Nombre')}"
                                    params="['term': term, 'offset': offset, 'sort': sort, 'order': order]" />
                                <g:sortableColumn property="precio_deseado"
                                title="${message(code: 'property.precio_deseado.list.label', default: 'Precio')}"
                                    params="['term': term, 'offset': offset, 'sort': sort, 'order': order]" />
                                <g:sortableColumn property="fecha_toma"
                                title="${message(code: 'property.fecha_toma_list.label', default: 'Fecha Alta')}"
                                    params="['term': term, 'offset': offset, 'sort': sort, 'order': order]" />
                                <g:sortableColumn property="fecha_toma"
                                title="${message(code: 'property.fecha_pendiente_list.label', default: 'Fecha Pend.')}"
                                    params="['term': term, 'offset': offset, 'sort': sort, 'order': order]" />
                                <g:sortableColumn property="agent"
                                title="${message(code: 'property.agent.label', default: 'Agente')}"
                                    params="['term': term, 'offset': offset, 'sort': sort, 'order': order]" />
                            </tr>
                        </thead>
                        <tbody>
                            <g:each in="${propertyInstanceList}" status="i"
                                var="propertyInstance">

                                <g:if test="${propertyInstance.fecha_pendiente}">
                                    <g:if
                                    test="${propertyInstance.fecha_pendiente.compareTo(new Date())>0}">
                                        <tr style="background-color: #a3a3a3;">
                                    </g:if>
                                    <g:elseif
                                    test="${propertyInstance.alquilado && propertyInstance.estado=='POSITIVO'}">
                                    <tr style="background-color: #fbd850;">
                                    </g:elseif>
                                    <g:elseif test="${propertyInstance.reservada}">
                                    <tr style="background-color: orange; color: #000000;">
                                    </g:elseif>
                                    <g:elseif test="${propertyInstance.vendida}">
                                    <tr style="background-color: cornflowerblue; color: #000000;">
                                    </g:elseif>
                                    <g:elseif test="${propertyInstance.anulado}">
                                    <tr style="background-color: red; color: #000000;">
                                    </g:elseif>
                                    <g:else>
                                    <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                                    </g:else>
                                </g:if>
                                <g:else>
                                <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                                </g:else>

                            <sec:ifAnyGranted roles="ROLE_ADMIN, ROLE_SUPER_USER">
                                <td><g:link action="edit" id="${propertyInstance?.id}"
                                        params="['term': term, 'offset': offset, 'sort': sort, 'order': order]">
                                        ${fieldValue(bean: propertyInstance, field: "referencia")}
                                    </g:link></td>
                                <td><g:link action="edit" id="${propertyInstance?.id}"
                                        params="['term': term, 'offset': offset, 'sort': sort, 'order': order]">
                                        ${fieldValue(bean: propertyInstance, field: "estado")}
                                    </g:link></td>
                                <td><g:link action="edit" id="${propertyInstance?.id}"
                                        params="['term': term, 'offset': offset, 'sort': sort, 'order': order]">
                                        <g:if
                                        test="${propertyInstance.web==true}">
                                            SI
                                        </g:if>
                                        <g:else>
                                            NO
                                        </g:else>      
                                    </g:link></td>
                                <td><g:link action="edit" id="${propertyInstance?.id}"
                                        params="['term': term, 'offset': offset, 'sort': sort, 'order': order]">
                                        ${fieldValue(bean: propertyInstance, field: "operacion")}
                                    </g:link></td>
                                <td><g:link action="edit" id="${propertyInstance?.id}"
                                        params="['term': term, 'offset': offset, 'sort': sort, 'order': order]">
                                        ${fieldValue(bean: propertyInstance, field: "tipo.name")}
                                    </g:link></td>
                                <td><g:link action="edit" id="${propertyInstance?.id}"
                                        params="['term': term, 'offset': offset, 'sort': sort, 'order': order]">
                                        ${fieldValue(bean: propertyInstance, field: "address")}
                                    </g:link></td>
                                    <g:if test="${(agentId==propertyInstance?.agent?.id)}">
                                        <g:if test="${propertyInstance.owners}">
                                            <g:each in="${propertyInstance.owners}" var="owner"
                                                status="j">
                                                <g:if test="${j == 0}">
                                                <td style="width: 100px;">
                                                    ${owner?.phone1}
                                                </td>
                                                <td>
                                                    ${owner?.completeName}
                                                </td>
                                            </g:if>
                                        </g:each>
                                    </g:if>
                                </g:if>
                                <g:else>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </g:else>
                                <td style="width: 100px; font-weight: bold;">
                                    ${propertyInstance?.precio_deseado}
                                </td>
                                <td><g:if
                                    test="${propertyInstance?.fecha_toma!='0000-01-01'}">
                                        <g:formatDate format="dd-MM-yyyy"
                                        date="${propertyInstance?.fecha_toma}"
                                            params="['term': term, 'offset': offset, 'sort': sort, 'order': order]" />
                                    </g:if></td>
                                <td><g:if
                                    test="${propertyInstance?.fecha_pendiente!='0000-01-01'}">
                                        <g:formatDate format="dd-MM-yyyy"
                                        date="${propertyInstance?.fecha_pendiente}"
                                            params="['term': term, 'offset': offset, 'sort': sort, 'order': order]" />
                                    </g:if></td>
                                <td>
                                    ${fieldValue(bean: propertyInstance, field: "agent.name")}
                                </td>
                                <td><g:link action="_searchFromProperty"
                                    id="${propertyInstance?.id}" class="btn btn-info">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </g:link></td>
                            </sec:ifAnyGranted>
                            <sec:ifNotGranted roles="ROLE_SUPER_USER, ROLE_ADMIN">
                                <g:if
                                test="${(agentId==propertyInstance?.agent?.id)||(agentId==2)||(agentId==3)||(agentId==4)}">
                                    <td><g:link action="edit" id="${propertyInstance?.id}"
                                            params="['term': term, 'offset': offset, 'sort': sort, 'order': order]">
                                            ${fieldValue(bean: propertyInstance, field: "referencia")}
                                        </g:link></td>
                                    <td><g:link action="edit" id="${propertyInstance?.id}"
                                            params="['term': term, 'offset': offset, 'sort': sort, 'order': order]">
                                            ${fieldValue(bean: propertyInstance, field: "estado")}
                                        </g:link></td>
                                    <td><g:link action="edit" id="${propertyInstance?.id}"
                                            params="['term': term, 'offset': offset, 'sort': sort, 'order': order]">
                                            <g:if
                                            test="${propertyInstance.web==true}">
                                                SI
                                            </g:if>
                                            <g:else>
                                                NO
                                            </g:else>
                                        </g:link></td>
                                    <td><g:link action="edit" id="${propertyInstance?.id}"
                                            params="['term': term, 'offset': offset, 'sort': sort, 'order': order]">
                                            ${fieldValue(bean: propertyInstance, field: "operacion")}
                                        </g:link></td>
                                    <td><g:link action="edit" id="${propertyInstance?.id}"
                                            params="['term': term, 'offset': offset, 'sort': sort, 'order': order]">
                                            ${fieldValue(bean: propertyInstance, field: "tipo.name")}
                                        </g:link></td>
                                    <td><g:link action="edit" id="${propertyInstance?.id}"
                                            params="['term': term, 'offset': offset, 'sort': sort, 'order': order]">
                                            ${fieldValue(bean: propertyInstance, field: "address")}
                                        </g:link></td>
                                        <g:if test="${(agentId==propertyInstance?.agent?.id)}">
                                            <g:if test="${propertyInstance.owners}">
                                                <g:each in="${propertyInstance.owners}" var="owner"
                                                    status="j">
                                                    <g:if test="${j == 0}">
                                                    <td style="width: 100px;">
                                                        ${owner?.phone1}
                                                    </td>
                                                    <td>
                                                        ${owner?.completeName}
                                                    </td>
                                                </g:if>
                                            </g:each>
                                        </g:if>
                                        <g:else>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </g:else>
                                    </g:if>
                                    <g:else>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </g:else>

                                    <td style="width: 100px; font-weight: bold;">
                                        ${propertyInstance?.precio_deseado}
                                    </td>
                                    <td><g:if
                                        test="${propertyInstance?.fecha_toma!='0000-01-01'}">
                                            <g:formatDate format="dd-MM-yyyy"
                                            date="${propertyInstance?.fecha_toma}"
                                                params="['term': term, 'offset': offset, 'sort': sort, 'order': order]" />
                                        </g:if></td>
                                    <td><g:if
                                        test="${propertyInstance?.fecha_pendiente!='0000-01-01'}">
                                            <g:formatDate format="dd-MM-yyyy"
                                            date="${propertyInstance?.fecha_pendiente}"
                                                params="['term': term, 'offset': offset, 'sort': sort, 'order': order]" />
                                        </g:if></td>
                                    <td>
                                        ${fieldValue(bean: propertyInstance, field: "agent.name")}
                                    </td>
                                    <td><g:link action="_searchFromProperty"
                                        id="${propertyInstance?.id}" class="btn btn-info">
                                            <span class="glyphicon glyphicon-search"></span>
                                        </g:link></td>
                                    </g:if>
                                    <g:else>
                                    <td>
                                        ${fieldValue(bean: propertyInstance, field: "referencia")}
                                    </td>
                                    <td>
                                        ${fieldValue(bean: propertyInstance, field: "estado")}
                                    </td>
                                    <td>
                                        ${fieldValue(bean: propertyInstance, field: "operacion")}
                                    </td>
                                    <td>
                                        ${fieldValue(bean: propertyInstance, field: "tipo.name")}
                                    </td>
                                    <td>
                                        ${fieldValue(bean: propertyInstance, field: "address")}
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>
                                        ${fieldValue(bean: propertyInstance, field: "precio_deseado")}
                                    </td>
                                    <td><g:if
                                        test="${propertyInstance?.fecha_toma!='0000-01-01'}">
                                            <g:formatDate format="dd-MM-yyyy"
                                            date="${propertyInstance?.fecha_toma}"
                                                params="['term': term, 'offset': offset, 'sort': sort, 'order': order]" />
                                        </g:if></td>
                                    <td><g:if
                                        test="${propertyInstance?.fecha_pendiente!='0000-01-01'}">
                                            <g:formatDate format="dd-MM-yyyy"
                                            date="${propertyInstance?.fecha_pendiente}"
                                                params="['term': term, 'offset': offset, 'sort': sort, 'order': order]" />
                                        </g:if></td>
                                    <td>
                                        ${fieldValue(bean: propertyInstance, field: "agent.name")}
                                    </td>
                                    <td>&nbsp;</td>
                                </g:else>
                            </sec:ifNotGranted>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                    <div class="pagination">
                        <g:paginate total="${totalCount}"
                            params="[term: term, 'offset': offset, 'sort': sort, 'order': order]" />
                    </div>
                </div>
            </div>
        </div>
        <g:render template="/layouts/footer" />
    </body>
</html>
