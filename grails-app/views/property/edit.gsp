<%@ page import="es.xoubin.domain.Property" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html style="height: 100%">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main">
        <g:javascript library="lightbox" />
        <g:javascript library="validator" />
        <g:set var="entityName" value="${message(code: 'property.label', default: 'Property')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHkd_xB3_SIFo4OApOKRlrXRAj_Tg6H1s&sensor=false" type="text/javascript"></script>
        <script src="http://maps.google.com/maps/api/js?sensor=true&libraries=places" type="text/javascript"></script>
        <script type="text/javascript">
            var geocoder;
            var autocomplete;
            var map;
            var query;
            var infowindow;
            var marker;
            var lat;
            var lon;

            function initialize() {
            var mapOptions;
        	lat = '${propertyInstance?.latitud}';
            lon = '${propertyInstance?.longitud}';
            if ((lat!='') && (lon!='')){
            mapOptions = {
            center: new google.maps.LatLng(lat, lon),
            zoom: 14
            }
            }
            else{
            mapOptions = {
            zoom: 14
            }
            }
            map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);


            marker = new google.maps.Marker({
            map: map
            });

            autocomplete = new google.maps.places.Autocomplete(document.getElementById('direccion_gmaps'));
            geocoder = new google.maps.Geocoder();
            infowindow = new google.maps.InfoWindow();
            if ((lat!='') && (lon!='')){
            searchAddressByCoord();
            }
            else{
            	searchAddress('${propertyInstance?.direccion_gmaps}');
            }

            /**
            * Autocomplete selection
            */
            google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
            return;
            }
            var latLng = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
            displayInfo(place, latLng);
            //setMarker(map, latLng, place.icon);
            setInfoWindow(place);
            });
            }

            function searchAddressByCoord() {
            var latlong = new google.maps.LatLng(lat, lon);
            geocoder.geocode( { 'latLng':latlong }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
            var latLng = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
            displayInfo(results[0], latLng);
            setMarker(map, latLng, results[0].icon);
            setInfoWindow(null);
            } else {
            //alert('Huy, vaya... ' + status);
            }
            });
            return false;
            }

            function searchAddress(query) {        	
            var address = query;
            geocoder.geocode( { 'address': address }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                    if (results.length > 1) {
            var res = '';
            $("#locationResults").html("");
                        $("#locationResults").append($("<ul id='locationsList' class='nav nav-pills nav-stacked'></ul>"));
            $.each(results, function(idx, val) {
            $("#locationsList").append(
                                    $("<li class='location' data-location='{\"lat\":\"" + val.geometry.location.lat() + "\", \"lng\":\"" + val.geometry.location.lng() + "\"}'></li>").append(
                                            $("<a href='#'></a>").append(val.formatted_address)
            ).on("click", handleLocation)
            );
            });
            } else {
            var latLng = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
            displayInfo(results[0], latLng);
            setMarker(map, latLng, results[0].icon);
            setInfoWindow(null);
            }
            } else {
            //alert('Huy, vaya... ' + status);
            }
            });
            return false;
            }

            function setInfoWindow(place) {
            infowindow.close();
            if (place && place.formatted_address) {
        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + place.formatted_address);
        infowindow.open(map, marker);
        }
        }

        function setMarker(map, latLng, icon) {
        marker.setVisible(false);
        if (icon) {
        marker.setIcon(getIcon(icon));
        }
        marker.setPosition(latLng);
        map.setCenter(latLng);
        marker.setVisible(true);
        marker.setDraggable(true);
        /**
        * Marker dragend event
        */
        google.maps.event.addListener(marker, 'dragend', function(evt) {
        $("#latitud").val(evt.latLng.lat());
        $("#longitud").val(evt.latLng.lng());
        });
        }

        function displayInfo(place, latLng) {
        $("#latitud").val(latLng.lat());
        $("#longitud").val(latLng.lng());
        if (place) {
        $("#locationAddr").html(place.formatted_address);
        $("#direccion_gmaps").val(place.formatted_address);
        }
        }

        function getIcon(strIcon) {
        var icon = 	{
        url: strIcon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(35, 35)
        };
        return icon;
        }

        function handleLocation () {
        var locations = $(this).data("location");
        var latLng = new google.maps.LatLng(locations.lat, locations.lng)
        displayInfo(null, latLng);
        setMarker(map, latLng);
        setInfoWindow(null);
        $("#locationAddr").val("");
        $("#direccion_gmaps").html(place.formatted_address);
        var query = $("#direccion_gmaps").val();
        searchAddress(query);
        }
        /**
        * initialize google.map
        */
        //google.maps.event.addDomListener(document.getElementById('maps'), 'onClick', initialize);
        $(function() {
        $(".location").on("click", handleLocation);
        /**
        * Search button
        */
        $("#btnSearch").on("click", function() {
        var query = $("#direccion_gmaps").val();
        searchAddress(query);
        })
        });


    </script>
</head>
<body>
    <g:render template="/layouts/navbar" />
    <g:render template="/layouts/header" />
    <div style="width: 75%;margin:0 auto 0 auto;">
        <div class="row-fluid" style="width: 100%;">
            <div class="span12" style="width: 100%;">
                <div id="edit-property" class="content scaffold-create" role="main" style="width: 100%; margin:0 auto 0 auto;">
                    <div class="page-header">
                        <h2><g:message code="property.edit.label" /></h2>
                    </div>
                    <g:if test="${flash.message}">
                        <div class="alert alert-success message" role="status">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            ${flash.message}
                        </div>
                    </g:if>
                    <g:if test="${flash.error}">
                        <div class="alert alert-warning message" role="status">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            ${flash.error}
                        </div>
                    </g:if>

                    <div id="axMainMsg" class="alert hide" role="status">
                    </div>
                    <g:hasErrors bean="${propertyInstance}">
                        <div class="alert alert-error">
                            <ul class="errors" role="alert">
                                <g:eachError bean="${propertyInstance}" var="error">
                                    <li>
                                        <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>
                                        <g:message error="${error}"/>
                                    </li>
                                </g:eachError>
                            </ul>
                        </div>
                    </g:hasErrors>

                    <ul id="tabs" class="nav nav-tabs" style="border-radius: 20px 20px 0px 0px; -moz-border-radius: 20px 20px 0px 0px; -webkit-border-radius: 20px 20px 0px 0px; background-color: #d3d3d3; border: 0px solid #000000;" data-tabs="tabs">
                        <li class="active">
                            <a href="#portada" class="pestana" data-toggle="tab" style="padding-left: 5px; padding-right: 5px; padding-bottom: 5px;">
                                <g:message code="property.tab.portada" />
                            </a>
                        </li>
                        <li>
                            <a href="#datos-basicos" class="pestana" data-toggle="tab" style="padding-left: 5px; padding-right: 5px; padding-bottom: 5px;">
                                <g:message code="property.tab.datos" />
                            </a>
                        </li>
                        <li>
                            <a href="#info_general" class="pestana" data-toggle="tab" style="padding-left: 5px; padding-right: 5px; padding-bottom: 5px;">
                                <g:message code="property.tab.info_general" />
                            </a>
                        </li>
                        <li>
                            <a href="#caracteristicas" class="pestana" data-toggle="tab" style="padding-left: 5px; padding-right: 5px; padding-bottom: 5px;">
                                <g:message code="property.tab.caracteristicas" />
                            </a>
                        </li>
                        <li>
                            <a href="#distribucion" class="pestana" data-toggle="tab" style="padding-left: 5px; padding-right: 5px; padding-bottom: 5px;">
                                <g:message code="property.tab.distribucion" />
                                <span class="badge">
                                    ${propertyInstance?.floors?.size()}
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#calidades" class="pestana" data-toggle="tab" style="padding-left: 5px; padding-right: 5px; padding-bottom: 5px;">
                                <g:message code="property.tab.calidades" />
                            </a>
                        </li>
                        <li>
                            <a href="#otros" class="pestana" data-toggle="tab" style="padding-left: 5px; padding-right: 5px; padding-bottom: 5px;">
                                <g:message code="property.tab.otros_aspectos" />
                            </a>
                        </li>
                        <li>
                            <a href="#precios" class="pestana" data-toggle="tab" style="padding-left: 5px; padding-right: 5px; padding-bottom: 5px;">
                                <g:message code="property.tab.precios" />
                                <span class="badge">
                                    ${propertyInstance?.prices?.size()}
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#promocion" class="pestana" data-toggle="tab" style="padding-left: 5px; padding-right: 5px; padding-bottom: 5px;">
                                <g:message code="property.tab.promocion" />
                            </a>
                        </li>
                        <li>
                            <a href="#propietarios" class="pestana" data-toggle="tab" style="padding-left: 5px; padding-right: 5px; padding-bottom: 5px;">
                                <g:message code="property.tab.propietarios" />
                                <span class="badge">
                                    ${propertyInstance?.owners?.size()}
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#info_visitas" class="pestana" data-toggle="tab" style="padding-left: 5px; padding-right: 5px; padding-bottom: 5px;">
                                <g:message code="property.tab.info_visitas" />
                            </a>
                        </li>
                        <li>
                            <a href="#datos_registrales" class="pestana" data-toggle="tab" style="padding-left: 5px; padding-right: 5px; padding-bottom: 5px;">
                                <g:message code="property.tab.datos_registrales" />
                            </a>
                        </li>
                        <li>
                            <a href="#maps" data-toggle="tab" class="pestana" onclick="initialize()" style="padding-left: 5px; padding-right: 5px; padding-bottom: 5px;">
                                <g:message code="property.tab.maps" />
                            </a>
                        </li>
                        <li>
                            <a href="#gallery" data-toggle="tab" class="pestana" style="padding-left: 5px; padding-right: 5px; padding-bottom: 5px;">
                                <g:message code="property.tab.gallery" />
                                <span class="badge">
                                    ${countImages}
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#files" data-toggle="tab" class="pestana" style="padding-left: 5px; padding-right: 5px; padding-bottom: 5px;">
                                <g:message code="property.tab.files" />
                                <span class="badge">
                                    ${countFiles}
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#upload" data-toggle="tab" class="pestana" style="padding-left: 5px; padding-right: 5px; padding-bottom: 5px;">
                                <g:message code="property.tab.images" />
                            </a>
                        </li>
                    </ul>
                    <g:form class="form-horizontal" action='list' name='costumerForm'>
                        <g:hiddenField  id="offset" name="offset" value="${offset}"></g:hiddenField>
                        <g:hiddenField  id="sort" name="sort" value="${sort}"></g:hiddenField>
                        <g:hiddenField  id="order" name="order" value="${order}"></g:hiddenField>
                        <g:hiddenField name="id" id="idProperty"
                        value="${propertyInstance?.id}" />
                        <div id="my-tab-content" class="tab-content" style="background-color: #B1B1B1;">
                            <div class="tab-pane active" id="portada">
                                <g:render template="forms/formPortada"/>
                            </div>
                            <div class="tab-pane" id="datos-basicos">
                                <br>
                                <g:render template="forms/form"/>
                            </div>
                            <div class="tab-pane" id="info_general">
                                <br>
                                <g:render template="forms/formInfo" />
                            </div>
                            <div class="tab-pane" id="caracteristicas">
                                <br>
                                <g:render template="forms/formCaracteristicas" />
                            </div>
                            <div class="tab-pane" id="distribucion">
                                <br>
                                <g:render template="forms/formDistribucion" />
                            </div>
                            <div class="tab-pane" id="calidades">
                                <br>
                                <g:render template="forms/formCalidades" />
                            </div>
                            <div class="tab-pane" id="otros">
                                <br>
                                <g:render template="forms/formOtros" />
                            </div>
                            <div class="tab-pane" id="precios">
                                <br>
                                <g:render template="forms/formPrecios" />
                            </div>
                            <div class="tab-pane" id="promocion">
                                <br>
                                <g:render template="forms/formPromocion" />
                            </div>
                            <div class="tab-pane" id="propietarios">
                                <br>
                                <g:render template="forms/formOwners" />
                            </div>
                            <div class="tab-pane" id="info_visitas">
                                <br>
                                <g:render template="forms/formVisitas" />
                            </div>
                            <div class="tab-pane" id="datos_registrales">
                                <br>
                                <g:render template="forms/formRegistrales" />
                            </div>
                    </g:form>
                            <div class="tab-pane" id="maps">
                                <br>
                                <g:render template="forms/formMaps" />
                            </div>
                            <div class="tab-pane" id="gallery">
                                <br>
                                <g:render template="forms/formGaleria" />
                            </div>
                            <div class="tab-pane" id="files">
                                <br>
                                <g:render template="forms/formFiles" />
                            </div>
                            <div class="tab-pane" id="upload">
                                <br>
                                <g:render template="forms/formUpload" />
                            </div>
                            <br>
                            <fieldset style="padding: 20px;">
                                <g:hiddenField  id="term" name="term" value="${term}"></g:hiddenField>
                                <g:actionSubmit id="btnUpdate" class="save btn btn-primary"
                                    action="update"
                                value="${message(code: 'default.button.update.label', default: 'Update')}" />
                                <g:actionSubmit id="btnUpdateClose" class="save btn btn-primary"
                                    action="update_close"
                                value="${message(code: 'default.button.update_close.label', default: 'Actualizar y Volver')}" />
                                <g:actionSubmit id="btnDelete" class="btn btn-danger"
                                    action="delete"
                                value="${message(code: 'property.delete.label', default: 'Eliminar Inmueble')}"
                                    formnovalidate=""
                                onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                            </fieldset>
                        </div>
                </div> <!-- span12 -->				
            </div> <!-- row-fluid -->
        </div> <!-- container-fluid -->
        <g:render template="/layouts/footer" />
        <jq:jquery>
            //Comprobamos si el precio está bien metido
            $("#btnUpdate").bind("click", function() {
            if($("#precio_deseado").val().match("[a-zA-Z]")){
            alert('Ha introducido el precio incorrectamente.')
            return false;
            }
            if($(".divOwner").length==0){
            alert('Alguien se ha olvidado de introducir el teléfono...')
            return false;
            }
            });

            //Cargamos el combo de zona, al cambiar la city
            $(document).ready(function(){
            $("#cboCity").change(function () {
            $.ajax({
            type: "POST",
            url: "${request.contextPath}/zone/_getZonesByCity",
            dataType: 'json',
            data: {
            idCity: $("#cboCity").val()
            },
            success: function(data) {
            if (data.success == 'OK') {
            $("#cboZone").empty();
            $('#cboZone').append('<option value="">--Elegir--</option>');
            $.each(data.list, function(i, value) {
            $('#cboZone').append('<option value="'+value[0]+'">'+value[1]+'</option>');
            });
            } else {
            //alert(data);
            }
            },
            error: function(data) {
            //alert(data);
            },
            complete: function() {
            //alert(data);
            }
            });
            })
            });


            $(document).ready(function(){
            $("#divPeques img").click(function(){
            var imagen=$(this).attr("src");
            $("#grande").attr("src",imagen);
            });
            })

            $(".pestana").bind("click", function() {
            if((this.href.search('gallery')>0)||(this.href.search('files')>0)||(this.href.search('upload')>0)){
            $("#btnDelete").prop('disabled', true);
            }
            else{
            $("#btnDelete").prop('disabled', false);
            }
            });

            $(document).ready(function ($) {
            $('#tabs').tab();
            });
            ///marcamos o desmarcamos todos
            $("#checkBorrarTodos").bind("click", function() {
            if($("#checkBorrarTodos").is(':checked')){
            $(".checkBorrar").each(function(){
            this.checked = true;
            });
            }
            else{
            $(".checkBorrar").each(function(){
            this.checked = false;
            });
            }
            });

            $("#checkBorrarTodosFiles").bind("click", function() {
            if($("#checkBorrarTodosFiles").is(':checked')){
            $(".checkBorrarFile").each(function(){
            this.checked = true;
            });
            }
            else{
            $(".checkBorrarFile").each(function(){
            this.checked = false;
            });
            }
            });

            ///Borramos las imagenes
            $("#btnDeleteImagenes").bind("click", function(event) {
            $(".checkBorrar").each(function(){
            if ($(this).is(':checked')){
            var buttonData = $(this).attr('data-button');
            $.ajax({
            type: "POST",
            url: "${request.contextPath}/property/_removeImg",
            dataType: 'json',
            data: {
            id: buttonData
            },
            success: function(data) {
            if (data.success == OK) {
            } else {
            //alert(data);
            }
            },
            error: function(data) {
            //alert(data);
            },
            complete: function() {
            //alert(data);
            }
            });
            }
            });
            window.setTimeout('location.reload()', 2000);
            });

            ///Borramos los files
            $("#btnDeleteFiles").bind("click", function(event) {
            $(".checkBorrarFile").each(function(){
            if ($(this).is(':checked')){
            var buttonData = $(this).attr('data-button');
            $.ajax({
            type: "POST",
            url: "${request.contextPath}/property/_removeImg",
            dataType: 'json',
            data: {
            id: buttonData
            },
            success: function(data) {
            if (data.success == OK) {
            } else {
            //alert(data);
            }
            },
            error: function(data) {
            //alert(data);
            },
            complete: function() {
            //alert(data);
            }
            });
            }
            });
            window.setTimeout('location.reload()', 2000);
            });

            $('#bsfuUp').bind('fileuploadstop', function (e, data) {
            window.setTimeout('location.reload()', 2000);
            });
        </jq:jquery>
</body>
</html>

