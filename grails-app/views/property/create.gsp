<%@ page import="es.xoubin.domain.Property" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
        <g:javascript library="validator" />
        <g:set var="entityName" value="${message(code: 'property.label', default: 'Property')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<g:render template="/layouts/navbar" />
		<g:render template="/layouts/header" />
		<div class="container">
			<div class="row-fluid">
				<div class="span12">
					<div id="create-costumer" class="content scaffold-create" role="main" style="width: 90%;margin:0 auto 0 auto;">
						<div class="page-header">
							<h2><g:message code="property.create.label" /></h2>
						</div>
						<g:if test="${flash.message}">
							<div class="alert alert-success message" role="status">
								<button type="button" class="close" data-dismiss="alert">×</button>
								${flash.message}
							</div>
						</g:if>
                        <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                            <li class="active">
                                <a href="#datos-basicos" data-toggle="tab">
                                    <g:message code="property.tab.datos" />
                                </a>
                            </li>
                        </ul>
                        <g:form class="form-horizontal" action="save" name='propertyForm'>
                            <g:hiddenField name="id" id="idProperty"
                                           value="${propertyInstance?.id}" />
                            <fieldset>
                                <div id="my-tab-content" class="tab-content">
                                    <div class="tab-pane active" id="datos-basicos">
                                        <br>
                                        <g:render template="forms/form"/>
                                    </div>
                                </div>
                            </fieldset>
                            <br>
                            <fieldset>
                                <g:submitButton name="create" class="save btn btn-primary" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                            </fieldset>
						</g:form>
					</div>
				</div> <!-- span12 -->				
			</div> <!-- row-fluid -->
		</div> <!-- container-fluid -->
		<g:render template="/layouts/footer" />
        <jq:jquery>
            $("#propertyForm").validate({
                rules: {
                	fecha_toma: "required",
                    cboAgente: "required",
                    cboTipo: "required"
                },
                messages: {
                    fecha_toma: "${message(code: 'property.fecha.required')}",
                    cboAgente: "${message(code: 'property.agente.required')}",
                    cboTipo: "${message(code: 'property.tipo.required')}"
            }
        });
        </jq:jquery>
	</body>
</html>
