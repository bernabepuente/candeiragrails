<table id="tblSentMessages" class="table table-stripped">
    <thead>
    <tr>
        <th>Tipo</th>
        <th>Nombre</th>
        <th style="border-top: 0"><g:checkBox name="checkBorrarTodosFiles"
                                              class="checkBorrarTodosFiles" id="checkBorrarTodosFiles" /></th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${propertyInstance?.images}" var="imageInstance">
        <tr>
            <g:if test="${imageInstance?.name.contains('.pdf') || imageInstance?.name.contains('.xls') || imageInstance?.name.contains('.doc') || imageInstance?.name.contains('.odt') || imageInstance?.name.contains('.docx')}">
                <td>
                <g:if test="${imageInstance?.name.contains('.pdf')}">
                    <g:img dir="images" file="PDF.jpg" width="40" height="40"/>
                </g:if>
                <g:elseif test="${imageInstance?.name.contains('.doc') || imageInstance?.name.contains('.docx')}">
                    <g:img dir="images" file="word.png" width="40" height="40"/>
                </g:elseif>
                <g:elseif test="${imageInstance?.name.contains('.xls')}">
                    <g:img dir="images" file="excel-png" width="40" height="40"/>
                </g:elseif>
                <g:elseif test="${imageInstance?.name.contains('.odt')}">
                    <g:img dir="images" file="odt.png" width="40" height="40"/>
                </g:elseif>
                </td>
                <td style="vertical-align: middle;">
                    <a href="${createLink(controller: 'image', action: 'getFile', id: imageInstance?.id)}" title="">${imageInstance?.name}</a>
                </td>
                <td style="vertical-align: middle;">
                    <g:checkBox name="chk_${imageInstance?.name}" data-button="${imageInstance?.id}" class="checkBorrarFile" id="chk_${imageInstance?.id}" />
                </td>
            </g:if>
        </tr>
    </g:each>
    </tbody>
</table>
<input type="button" value="Borrar" name="btnDeleteFiles" id="btnDeleteFiles" class="btn btn-danger"
                onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"></button>
