<%@ page import="es.xoubin.domain.Property"%>
<div class="form-group has-warnin">
	<label for="referencia" class="col-lg-2 control-label"><g:message
			code="property.referencia.label" default="Referencia" /></label>
	<div class="col-lg-10">
		<g:textField name="referencia" class="form-control"
			value="${propertyInstance?.referencia}" />
	</div>
</div>
<div class="form-group">
	<label for="fecha_toma" class="col-lg-2 control-label"><g:message
			code="property.fecha_toma.label" default="Fecha Toma" /></label>
	<div class="col-lg-10">
        <g:datePicker type="text" dateFormat="yy-MM-yyyy" name="fecha_toma" id="fecha_toma" class="form-control" precision="day" noSelection="['':'-Elegir-']"
                      value="${propertyInstance?.fecha_toma}" />
	</div>
</div>
<div class="form-group">
	<label for="fecha_pendiente" class="col-lg-2 control-label"><g:message
			code="property.fecha_pendiente.label" default="Fecha Pendiente" /></label>
	<div class="col-lg-10">
        <g:datePicker type="text" dateFormat="yy-MM-yyyy" name="fecha_pendiente" id="fecha_pendiente" class="form-control" precision="day" noSelection="['':'-Elegir-']"
                      value="${propertyInstance?.fecha_pendiente}" />
	</div>
</div>
<div class="form-group">
	<label for="cboAgente" class="col-lg-2 control-label"><g:message
			code="property.agent.label" default="Agente" /></label>
	<div class="col-lg-10">
        <g:select name="cboAgente" from="${agentInstanceList}"
                  value="${propertyInstance?.agent?.id}" optionKey="id"
                  optionValue="name"  noSelection="['':'--Elegir--']"/>
	</div>
</div>
<div class="form-group">
    <label for="cboTipo" class="col-lg-2 control-label"><g:message
            code="property.tipo.label" default="Tipo" /></label>
    <div class="col-lg-10">
        <g:select name="cboTipo" from="${tipoInstanceList}"
                  value="${propertyInstance?.tipo?.id}" optionKey="id"
                  optionValue="name"  noSelection="['':'--Elegir--']"/>
        <label>&nbsp;&nbsp;</label>
        <label for="reservada" style="background-color: orange;"><g:message code="property.reservada.label" default="Reservada" /></label>
        <g:checkBox name="reservada" value="${propertyInstance?.reservada}"></g:checkBox>
        <label>&nbsp;&nbsp;</label> <label for="vendida" style="background-color: cornflowerblue;"><g:message code="property.vendida.label" default="Vendida" /></label>
        <g:checkBox name="vendida" value="${propertyInstance?.vendida}"></g:checkBox>
        <label>&nbsp;&nbsp;</label> <label for="alquilado"><g:message code="property.alquilado.label" default="Alquilado" /></label>
        <g:checkBox name="alquilado" value="${propertyInstance?.alquilado}"></g:checkBox>
        <label>&nbsp;&nbsp;</label> <label for="anulado" style="background-color: red;"><g:message code="property.anulado.label" default="Anulado" /></label>
        <g:checkBox name="anulado" value="${propertyInstance?.anulado}"></g:checkBox>
    </div>
</div>
<div class="form-group">
    <label for="cboEstado" class="col-lg-2 control-label"><g:message
            code="property.estado.label" default="Estado" /></label>
    <div class="col-lg-10">
        <g:select name="estado"
				value="${propertyInstance?.estado}"
				from="${es.xoubin.enums.EnumEstado?.values()}" 
				keys="${es.xoubin.enums.EnumEstado.values()}" 
				valueMessagePrefix="inmueble.tipo.label" />
    </div>
</div>
<div class="form-group">
    <label for="cboOperacion" class="col-lg-2 control-label"><g:message
            code="property.operacion.label" default="Operación" /></label>
    <div class="col-lg-10">
        <g:select name="operacion"
				value="${propertyInstance?.operacion}"
				from="${es.xoubin.enums.EnumOperacion?.values()}" 
				keys="${es.xoubin.enums.EnumOperacion.values()}" 
				valueMessagePrefix="inmueble.operacion.label" />
    </div>
</div>
<div class="form-group">
     	<label for="web" class="col-lg-2 control-label"><g:message code="property.web.label" default="Web" /></label>
        <g:checkBox name="web" value="${propertyInstance?.web}"></g:checkBox>
</div>
<div class="form-group has-warnin">
	<label for="zona" class="col-lg-2 control-label"><g:message
			code="property.zona.label" default="Zona" /></label>
	<div class="col-lg-10">
		<g:textField name="zona" class="form-control"
			value="${propertyInstance?.zona}" />
	</div>
</div>
<div class="form-group has-warnin">
	<label for="desc_corta" class="col-lg-2 control-label"><g:message
			code="property.desc_corta.label" default="Descripcion Corta" /></label>
	<div class="col-lg-10">
		<g:textArea name="desc_corta" class="form-control"
			value="${propertyInstance?.desc_corta}" />
	</div>
</div>
<div class="form-group has-warnin">
	<label for="desc_larga" class="col-lg-2 control-label"><g:message
			code="property.desc_larga.label" default="Descripcion Larga" /></label>
	<div class="col-lg-10">
		<g:textArea name="desc_larga" class="form-control"
			value="${propertyInstance?.desc_larga}" />
	</div>
</div>
