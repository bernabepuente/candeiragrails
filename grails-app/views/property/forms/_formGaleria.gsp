<table id="tblSentMessages" class="table table-stripped">
    <thead>
    <tr>
        <th>Imagen</th>
        <th>Nombre</th>
        <th style="border-top: 0"><g:checkBox name="checkBorrarTodos"
                                              class="checkBorrarTodos" id="checkBorrarTodos" /></th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${imagesInstanceList}" var="imageInstance">
        <tr>
            <g:if test="${imageInstance?.name.contains('.JPG') || imageInstance?.name.contains('.jpg') || imageInstance?.name.contains('.JPEG') || imageInstance?.name.contains('.jpeg')}">
                <td>
                    <a href="${createLink(controller: 'image', action: 'getImg', id: imageInstance?.id)}" data-lightbox="gallery" title="">
                        <img style="width: 50px; height: 50px;" src="${createLink(controller: 'image', action: 'getImg', id: imageInstance?.id)}" />
                    </a>
                </td>
                <td style="vertical-align: middle;">
                    <a href="${createLink(controller: 'image', action: 'getImg', id: imageInstance?.id)}" title="">${imageInstance?.name}</a>
                </td>
                <td style="vertical-align: middle;">
                    <g:checkBox name="chk_${imageInstance?.name}" data-button="${imageInstance?.id}" class="checkBorrar" id="chk_${imageInstance?.id}" />
                </td>
            </g:if>
        </tr>
    </g:each>
    </tbody>
</table>
<input type="button" value="Borrar" name="btnDeleteImagenes" id="btnDeleteImagenes" class="btn btn-danger"
                onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"></button>
