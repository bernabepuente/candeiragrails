<div class="form-group has-warnin">
    <label for="referencia_catastral" class="col-lg-2 control-label"><g:message
            code="property.referencia_catastral.label" default="Ref. Catastral" /></label>
    <div class="col-lg-10">
        <g:textField name="referencia_catastral" class="form-control"
                     value="${propertyInstance?.referencia_catastral}" />
    </div>
</div><div class="form-group has-warnin">
    <label for="inscripcion" class="col-lg-2 control-label"><g:message
            code="property.inscripcion.label" default="Inscripcion" /></label>
    <div class="col-lg-10">
        <g:textField name="inscripcion" class="form-control"
                     value="${propertyInstance?.inscripcion}" />
    </div>
</div>
<div class="form-group">
    <label for="tomo" class="col-lg-2 control-label"><g:message
            code="property.tomo.label" default="Tomo" /></label>
    <div class="col-lg-10">
        <g:textField name="tomo" class="form-control"
                     value="${propertyInstance?.tomo}" />
    </div>
</div>
<div class="form-group">
    <label for="folio" class="col-lg-2 control-label"><g:message
            code="property.folio.label" default="Folio" /></label>
    <div class="col-lg-10">
        <g:textField name="folio" class="form-control"
                     value="${propertyInstance?.folio}" />
    </div>
</div>
<div class="form-group">
    <label for="finca" class="col-lg-2 control-label"><g:message
            code="property.finca.label" default="Finca" /></label>
    <div class="col-lg-10">
        <g:textField name="finca" class="form-control"
                     value="${propertyInstance?.finca}" />
    </div>
</div>
