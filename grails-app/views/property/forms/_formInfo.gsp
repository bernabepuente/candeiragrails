<div class="form-group has-warnin">
    <label for="address" class="col-lg-2 control-label"><g:message
            code="property.address.label" default="Dirección" /></label>
    <div class="col-lg-10">
        <g:textField name="address" class="form-control"
                     value="${propertyInstance?.address}" />
    </div>
</div>
<div class="form-group">
    <label for="cboCity" class="col-lg-2 control-label"><g:message
            code="property.city.label" default="Localidad" /></label>
    <div class="col-lg-4">
        <g:select class="form-control" name="cboCity" from="${cityInstanceList}"
              value="${propertyInstance?.city?.id}" optionKey="id"
              optionValue="name"  noSelection="['':'--Elegir--']"/>
    </div>
    <div class="col-lg-1" style="text-align: right;vertical-align: middle;">
        <label for="cboZone" class="col-lg-2 control-label">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<g:message code="property.zone.label" default="Zona"/></label>
    </div>
    <div class="col-lg-5">
        <g:select class="form-control" name="cboZone" from="${zoneInstanceList}"
                  value="${propertyInstance?.zone?.id}" optionKey="id"
                  optionValue="name"  noSelection="['':'--Elegir--']"/>
    </div>
</div>
<div class="form-group">
    <label for="superficie_terreno" class="col-lg-2 control-label"><g:message
            code="property.superficie_terreno.label" default="Superficie Terreno" /></label>
    <div class="col-lg-1">
    	<g:textField name="nSuperficie_terreno" class="form-control"
                     value="${propertyInstance?.nSuperficie_terreno}" />
    </div>
    <div class="col-lg-9">
        <g:textField name="superficie_terreno" class="form-control"
                     value="${propertyInstance?.superficie_terreno}" />
    </div>
</div>
<div class="form-group">
    <label for="superficie_construida" class="col-lg-2 control-label"><g:message
            code="property.superficie_construida.label" default="Superficie Construida" /></label>
    <div class="col-lg-1">
    	<g:textField name="nSuperficie_construida" class="form-control"
                     value="${propertyInstance?.nSuperficie_construida}" />
    </div>
    <div class="col-lg-9">
        <g:textField name="superficie_construida" class="form-control"
                     value="${propertyInstance?.superficie_construida}" />
    </div>
</div>
<div class="form-group">
    <label for="superficie_construida" class="col-lg-2 control-label"><g:message
            code="property.ano_construccion.label" default="Año Construcción" /></label>
    <div class="col-lg-10">
        <g:textField name="ano_construccion" class="form-control"
                     value="${propertyInstance?.ano_construccion}" />
    </div>
</div>
<div class="form-group">
    <label for="ano_adquisicion" class="col-lg-2 control-label"><g:message
            code="property.ano_adquisicion.label" default="Año Adquisición" /></label>
    <div class="col-lg-10">
        <g:textField name="ano_adquisicion" class="form-control"
                     value="${propertyInstance?.ano_adquisicion}" />
    </div>
</div>
<div class="form-group">
    <label for="estado_conservacion" class="col-lg-2 control-label"><g:message
            code="property.estado_conservacion.label" default="Estado Conservación" /></label>
    <div class="col-lg-10">
        <g:textField name="estado_conservacion" class="form-control"
                     value="${propertyInstance?.estado_conservacion}" />
    </div>
</div>
<div class="form-group">
    <label for="certf_energetico" class="col-lg-2 control-label"><g:message
            code="property.certf_energetico.label" default="Certificado Energético" /></label>
    <div class="col-lg-10">
        <g:textField name="certf_energetico" class="form-control"
                     value="${propertyInstance?.certf_energetico}" />
    </div>
</div>
<div class="form-group">
    <label for="arrendatarios" class="col-lg-2 control-label"><g:message
            code="property.arrendatarios.label" default="Arrendatarios" /></label>
    <div class="col-lg-10">
        <g:textField name="arrendatarios" class="form-control"
                     value="${propertyInstance?.arrendatarios}" />
    </div>
</div>
<div class="form-group">
    <label for="cargas" class="col-lg-2 control-label"><g:message
            code="property.cargas.label" default="Cargas" /></label>
    <div class="col-lg-10">
        <g:textField name="cargas" class="form-control"
                     value="${propertyInstance?.cargas}" />
    </div>
</div>
<div class="form-group">
    <label for="motivo_venta" class="col-lg-2 control-label"><g:message
            code="property.motivo_venta.label" default="Motivo Venta" /></label>
    <div class="col-lg-10">
        <g:textField name="motivo_venta" class="form-control"
                     value="${propertyInstance?.motivo_venta}" />
    </div>
</div>
<div class="form-group">
    <label for="plazo" class="col-lg-2 control-label"><g:message
            code="property.plazo.label" default="Plazo máx. venta" /></label>
    <div class="col-lg-10">
        <g:textField name="plazo" class="form-control"
                     value="${propertyInstance?.plazo}" />
    </div>
</div>
<div class="form-group">
    <label for="otros_general" class="col-lg-2 control-label"><g:message
            code="property.otros_general.label" default="Otros" /></label>
    <div class="col-lg-10">
        <g:textField name="otros_general" class="form-control"
                     value="${propertyInstance?.otros_general}" />
    </div>
</div>
<div class="form-group">
<label for="observaciones" class="col-lg-2 control-label"><g:message
            code="property.observaciones.label" default="Observaciones" /></label>
    <div class="col-lg-10">
        <g:textArea name="observaciones" class="form-control"
                     value="${propertyInstance?.observaciones}" />
    </div>
</div>
