<div class="form-group has-warnin">
    <label for="llaves" class="col-lg-2 control-label"><g:message
            code="property.llaves.label" default="Llaves" /></label>
    <div class="col-lg-10">
        <g:textField name="llaves" class="form-control"
                     value="${propertyInstance?.llaves}" />
    </div>
</div><div class="form-group has-warnin">
    <label for="aviso" class="col-lg-2 control-label"><g:message
            code="property.aviso.label" default="Aviso" /></label>
    <div class="col-lg-10">
        <g:textField name="aviso" class="form-control"
                     value="${propertyInstance?.aviso}" />
    </div>
</div>
<div class="form-group">
    <label for="observaciones_visita" class="col-lg-2 control-label"><g:message
            code="property.observaciones_visita.label" default="Observaciones" /></label>
    <div class="col-lg-10">
        <g:textField name="observaciones_visita" class="form-control"
                     value="${propertyInstance?.observaciones_visita}" />
    </div>
</div>
<div class="form-group">
    <label for="contacto" class="col-lg-2 control-label"><g:message
            code="property.contacto.label" default="Contacto" /></label>
    <div class="col-lg-10">
        <g:textField name="contacto" class="form-control"
                     value="${propertyInstance?.contacto}" />
    </div>
</div>
<div class="form-group">
    <label for="telefono_contacto" class="col-lg-2 control-label"><g:message
            code="property.telefono_contacto.label" default="Teléfono" /></label>
    <div class="col-lg-10">
        <g:textField name="telefono_contacto" class="form-control"
                     value="${propertyInstance?.telefono_contacto}" />
    </div>
</div>
<div class="form-group">
    <label for="horario_contacto" class="col-lg-2 control-label"><g:message
            code="property.horario_contacto.label" default="Horario" /></label>
    <div class="col-lg-10">
        <g:textField name="horario_contacto" class="form-control"
                     value="${propertyInstance?.horario_contacto}" />
    </div>
</div>
