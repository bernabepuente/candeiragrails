<div class="form-group has-warnin">
    <label for="precio_deseado" class="col-lg-2 control-label"><g:message
            code="property.precio_deseado.label" default="Precio Deseado" /></label>
    <div class="col-lg-10">
        <g:textField name="precio_deseado" class="form-control"
                     value="${propertyInstance?.precio_deseado}" />
    </div>
</div><div class="form-group has-warnin">
    <label for="razonamiento" class="col-lg-2 control-label"><g:message
            code="property.razonamiento.label" default="Razonamiento" /></label>
    <div class="col-lg-10">
        <g:textField name="razonamiento" class="form-control"
                     value="${propertyInstance?.razonamiento}" />
    </div>
</div>
<div class="form-group">
    <label for="minimo" class="col-lg-2 control-label"><g:message
            code="property.minimo.label" default="Minimo" /></label>
    <div class="col-lg-10">
        <g:textField name="minimo" class="form-control"
                     value="${propertyInstance?.minimo}" />
    </div>
</div>
<div class="form-group">
    <label for="urge" class="col-lg-2 control-label"><g:message
            code="property.urge.label" default="Urge Venta" /></label>
    <div class="col-lg-10">
        <g:checkBox name="urge" class=""
                     value="${propertyInstance?.urge}" />
    </div>
</div>
<div>
    <g:render template="/price/formEditPrices" />
</div>