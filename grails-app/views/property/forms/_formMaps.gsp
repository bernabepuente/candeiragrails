<div class="form-group has-warnin" style="align-content: center;">
    <div class="form-group">
        <div class="form-group">
            <label for="direccion_gmaps" class="col-lg-2 control-label"><g:message
                    code="property.direccion_gmaps.label" default="Dirección" /></label>
            <div class="col-lg-10">
                <g:textField name="direccion_gmaps" class="form-control"
                             value="${propertyInstance?.direccion_gmaps}" />
                <br>
                <input type="button" value="Buscar" id="btnSearch" onclick="searchAddress(document.getElementById('direccion_gmaps').value)" class="btn btn-info"></button>
            </div>


        </div>
    </div>
    <div class="form-group">
        <label for="latitud" class="col-lg-2 control-label"><g:message
                code="property.latitud.label" default="Latitud" /></label>
        <div class="col-lg-10">
            <g:textField name="latitud" class="form-control"
                         value="${propertyInstance?.latitud}" />
        </div>
    </div>
    <div class="form-group">
        <label for="longitud" class="col-lg-2 control-label"><g:message
                code="property.longitud.label" default="Longitud" /></label>
        <div class="col-lg-10">
            <g:textField name="longitud" class="form-control"
                         value="${propertyInstance?.longitud}" />
        </div>
    </div>
    <div id="locationAddr"></div>
    <div id="newLocationPoint"></div>
    <br>
    <div id="map_canvas" style="width:1700px;height:600px;">
    </div>
</div>