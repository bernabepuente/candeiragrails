<div style="width:100%; height: 100%; display:inline-block;" >
	<div style="width:100%; display:inline-block;float: left;">
		<div>
            <h1>${localidad} - ${zona}</h1>
        </div>
	</div>
    <div style="width:40%; height: 100%; display:inline-block;float: left;">
        <table style="width: 100%;font-size: 18px;">
        	<g:if test="${propertyInstance?.superficie_terreno != "0"}">
	            <tr style="width: 100%;">
	                <td style="width: 40%;"><label class="control-label"><g:message
	                        code="property.superficie_terreno.label"/></label>:&nbsp;</td>
	                <td style="width: 50%;"><label class="control-label">${propertyInstance?.nSuperficie_terreno} m2</label></td>
	            </tr>
            </g:if>
            <g:if test="${propertyInstance?.superficie_construida != "0"}">
	            <tr style="width: 100%;">
	                <td style="width: 50%;"><label class="control-label"><g:message
	                        code="property.superficie_construida.label"/></label>:&nbsp;</td>
	                <td style="width: 50%;"><label class="control-label">${propertyInstance?.nSuperficie_construida} m2</label></td>
	            </tr>
            </g:if>
            <g:if test="${propertyInstance?.nDormitorios > 0}">
	            <tr>
	                <td style="width: 50%;"><label class="control-label"><g:message
	                        code="property.dormitorios.label"/></label>:&nbsp;</td>
	                <td style="width: 50%;"><label class="control-label">${propertyInstance?.nDormitorios}</label></td>
	            </tr>
            </g:if>
            <g:if test="${propertyInstance?.nBanos > 0}">
	            <tr>
	                <td style="width: 50%;"><label class="control-label"><g:message
	                        code="property.banos.label"/></label>:&nbsp;</td>
	                <td style="width: 50%;"><label class="control-label">${propertyInstance?.nBanos}</label></td>
	            </tr>
            </g:if>
            <g:if test="${propertyInstance?.nAseos > 0}">
	            <tr>
	                <td style="width: 50%;"><label class="control-label"><g:message
	                        code="property.aseos.label"/></label>:&nbsp;</td>
	                <td style="width: 50%;"><label class="control-label">${propertyInstance?.nAseos}</label></td>
	            </tr>
            </g:if>
            <g:if test="${propertyInstance?.nGarajes > 0}">
                <tr>
                    <td style="width: 50%;text-align: left;"><label class="control-label"><g:message
                            code="property.garajes.label"/></label>:&nbsp;</td>
                    <td style="width: 50%;"><label class="control-label">${propertyInstance?.nGarajes}</label></td>
                </tr>
            </g:if>
            <g:if test="${propertyInstance?.trasteros}">
                <tr>
                    <td style="width: 50%;text-align: left;"><label style="text-align: left;"><g:message
                            code="property.trasteros.label"/></label>:&nbsp;</td>
                    <td style="width: 50%;text-align: left;"><label class="control-label" style="text-align: left;">${propertyInstance?.trasteros}</label></td>
                </tr>
            </g:if>
            <g:if test="${propertyInstance?.piscinas}">
                <tr>
                    <td style="width: 50%;text-align: left;"><label class="control-label"><g:message
                            code="property.piscinas.label"/></label>:&nbsp;</td>
                    <td style="width: 50%;text-align: left;"><label class="control-label" style="text-align: left;">${propertyInstance?.piscinas}</label></td>
                </tr>
            </g:if>
            <g:if test="${propertyInstance?.precio_deseado != ""}">
	            <tr>
	                <td style="width: 50%;"><label class="control-label"><g:message
	                        code="property.precio_deseado.label"/></label>:&nbsp;</td>
	                <td style="width: 50%;"><label class="control-label">${propertyInstance?.precio_deseado}</label></td>
	            </tr>
            <tr>
            </g:if>
            <g:if test="${precioAnterior?.amount != "0"}">
	            <tr>
	                <td style="width: 50%;"><label class="control-label"><g:message
	                        code="property.precio_anterior.label"/></label>:&nbsp;</td>
	                <td style="width: 50%;"><label class="control-label" style="text-decoration: line-through;">${precioAnterior?.amount}</label></td>
	            </tr>
            </g:if>
        </table>
    </div>
    <div style="width:60%; height: 100%; display: inline; float: right;">
        <div id="divGrande" style="width:800px; height: 600px;">
            <g:each in="${imagesInstanceList}" var="imageInstance" status="i">
                <g:if test="${i == 0}">
                    <a href="${createLink(controller: 'image', action: 'getImg', id: imageInstance?.id)}" data-lightbox="gallery" data-lightshow="gallery" title="">
                        <img style="padding: 2px 2px 2px 2px;" src="${createLink(controller: 'image', action: 'getImg', id: imageInstance?.id)}" name="grande" width="800" height="600" id="grande" />
                    </a>
                </g:if>
            </g:each>
        </div>
        <div id="divPeques" style="overflow-y: scroll; height:300px;width:800px;">
            <g:each in="${imagesInstanceList}" var="imageInstance" status="j">
                <g:if test="${j > 0}">
                    <g:if test="${imageInstance?.name.contains('.JPG') || imageInstance?.name.contains('.jpg') || imageInstance?.name.contains('.JPEG') || imageInstance?.name.contains('.jpeg')}">
                        <img style="margin: 2px 2px 2px 2px;" src="${createLink(controller: 'image', action: 'getImg', id: imageInstance?.id)}" width="187" height="145" />
                    </g:if>
                </g:if>
            </g:each>
        </div>
    </div>
</div>