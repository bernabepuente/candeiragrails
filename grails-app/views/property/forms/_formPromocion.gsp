<div class="form-group has-warnin">
    <label for="agencias" class="col-lg-2 control-label"><g:message
            code="property.agencias.label" default="Agencias" /></label>
    <div class="col-lg-10">
        <g:textField name="agencias" class="form-control"
                     value="${propertyInstance?.agencias}" />
    </div>
</div><div class="form-group has-warnin">
    <label for="publicidad" class="col-lg-2 control-label"><g:message
            code="property.publicidad.label" default="Publicidad" /></label>
    <div class="col-lg-10">
        <g:textField name="publicidad" class="form-control"
                     value="${propertyInstance?.publicidad}" />
    </div>
</div>
<div>
    <g:render template="/portal/formEditPortal" />
</div>
