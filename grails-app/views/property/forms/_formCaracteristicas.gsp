<div class="form-group has-warnin">
    <label for="planos" class="col-lg-2 control-label"><g:message
            code="property.planos.label" default="Planos distribución" /></label>
    <div class="col-lg-9">
        <g:textField name="planos" class="form-control" style="width: 101%;"
                     value="${propertyInstance?.planos}" />
    </div>
</div>
<div class="form-group has-warnin">
    <label for="ascensor" class="col-lg-2 control-label"><g:message
            code="property.ascensor.label" default="Ascensor" /></label>
    <div class="col-lg-9" style="text-align:left;">
        <g:checkBox name="ascensor" class=""
                     value="${propertyInstance?.ascensor}" />
    </div>
</div>
<div class="form-group has-warnin">
    <label for="amueblado" class="col-lg-2 control-label"><g:message
            code="property.amueblado.label" default="Amueblado" /></label>
    <div class="col-lg-9" style="text-align:left;">
        <g:checkBox name="amueblado" class=""
                     value="${propertyInstance?.amueblado}" />
    </div>
</div>
<div class="form-group">
    <label for="porches" class="col-lg-2 control-label"><g:message
            code="property.porches.label" default="Porches" /></label>
    <div class="col-lg-9">
        <g:textField name="porches" class="form-control" style="width: 101%;"
                     value="${propertyInstance?.porches}" />
    </div>
</div>
<div class="form-group">
    <label for="terrazas" class="col-lg-2 control-label"><g:message
            code="property.terrazas.label" default="Terrazas" /></label>
    <div class="col-lg-9">
        <g:textField name="terrazas" class="form-control" style="width: 101%;"
                     value="${propertyInstance?.terrazas}" />
    </div>
</div>
<div class="form-group">
    <label for="salones" class="col-lg-2 control-label"><g:message
            code="property.salones.label" default="Salones" /></label>
    <div class="col-lg-10">
        <div class="col-lg-1">
        <g:select name="nSalones" from="${1..15}" value="${propertyInstance?.nSalones}" optionKey="" noSelection="['':'--']"/>
        </div>
        <div class="col-lg-10">
            <g:textField name="salones" class="form-control"
                         value="${propertyInstance?.salones}" />
        </div>
    </div>
</div>
<div class="form-group">
    <label for="cocinas" class="col-lg-2 control-label"><g:message
            code="property.cocinas.label" default="Cocinas" /></label>
    <div class="col-lg-10">
        <div class="col-lg-1">
            <g:select name="nCocinas" from="${1..15}" value="${propertyInstance?.nCocinas}" optionKey="" noSelection="['':'--']"/>
        </div>
        <div class="col-lg-10">
            <g:textField name="cocinas" class="form-control"
                         value="${propertyInstance?.cocinas}" />
        </div>
    </div>
</div>
<div class="form-group">
    <label for="dormitorios" class="col-lg-2 control-label"><g:message
            code="property.dormitorios.label" default="Dormitorios" /></label>
    <div class="col-lg-10">
        <div class="col-lg-1">
            <g:select name="nDormitorios" from="${1..15}" value="${propertyInstance?.nDormitorios}" optionKey="" noSelection="['':'--']"/>
        </div>
        <div class="col-lg-10">
            <g:textField name="dormitorios" class="form-control"
                         value="${propertyInstance?.dormitorios}" />
        </div>
    </div>
</div>
<div class="form-group">
    <label for="aseos" class="col-lg-2 control-label"><g:message
            code="property.aseos.label" default="Aseos" /></label>
    <div class="col-lg-10">
        <div class="col-lg-1">
            <g:select name="nAseos" from="${1..15}" value="${propertyInstance?.nAseos}" optionKey="" noSelection="['':'--']"/>
        </div>
        <div class="col-lg-10">
            <g:textField name="aseos" class="form-control"
                         value="${propertyInstance?.aseos}" />
        </div>
    </div>
</div>
<div class="form-group">
    <label for="banos" class="col-lg-2 control-label"><g:message
            code="property.banos.label" default="Baños" /></label>
    <div class="col-lg-10">
        <div class="col-lg-1">
            <g:select name="nBanos" from="${1..15}" value="${propertyInstance?.nBanos}" optionKey="" noSelection="['':'--']"/>
        </div>
        <div class="col-lg-10">
            <g:textField name="banos" class="form-control"
                         value="${propertyInstance?.banos}" />
        </div>
    </div>
</div>
<div class="form-group">
    <label for="lavaderos" class="col-lg-2 control-label"><g:message
            code="property.lavaderos.label" default="Lavaderos" /></label>
    <div class="col-lg-9">
        <g:textField name="lavaderos" class="form-control" style="width: 101%;"
                     value="${propertyInstance?.lavaderos}" />
    </div>
</div>
<div class="form-group">
    <label for="despensas" class="col-lg-2 control-label"><g:message
            code="property.despensas.label" default="Despensas" /></label>
    <div class="col-lg-9">
        <g:textField name="despensas" class="form-control" style="width: 101%;"
                     value="${propertyInstance?.despensas}" />
    </div>
</div>
<div class="form-group">
    <label for="garajes" class="col-lg-2 control-label"><g:message
            code="property.garajes.label" default="Garajes" /></label>
    <div class="col-lg-10">
        <div class="col-lg-1">
            <g:select name="nGarajes" from="${1..15}" value="${propertyInstance?.nGarajes}" optionKey="" noSelection="['':'--']"/>
        </div>
        <div class="col-lg-10">
            <g:textField name="garajes" class="form-control"
                         value="${propertyInstance?.garajes}" />
        </div>
    </div>
</div>
<div class="form-group">
    <label for="trasteros" class="col-lg-2 control-label"><g:message
            code="property.trasteros.label" default="Trasteros" /></label>
    <div class="col-lg-9">
        <g:textField name="trasteros" class="form-control" style="width: 101%;"
                     value="${propertyInstance?.trasteros}" />
    </div>
</div>
<div class="form-group">
    <label for="instalaciones" class="col-lg-2 control-label"><g:message
            code="property.instalaciones.label" default="Cuarto Instalaciones" /></label>
    <div class="col-lg-9">
        <g:textField name="instalaciones" class="form-control" style="width: 101%;"
                     value="${propertyInstance?.instalaciones}" />
    </div>
</div>
<div class="form-group">
    <label for="piscinas" class="col-lg-2 control-label"><g:message
            code="property.piscinas.label" default="Piscinas" /></label>
    <div class="col-lg-9">
        <g:textField name="piscinas" class="form-control" style="width: 101%;"
                     value="${propertyInstance?.piscinas}" />
    </div>
</div>
<div class="form-group">
    <label for="otros_instalaciones" class="col-lg-2 control-label"><g:message
            code="property.otros_instalaciones.label" default="Otros" /></label>
    <div class="col-lg-9">
        <g:textField name="otros_instalaciones" class="form-control" style="width: 101%;"
                     value="${propertyInstance?.otros_instalaciones}" />
    </div>
</div>
