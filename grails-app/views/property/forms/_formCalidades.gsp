<div class="form-group has-warnin">
    <label for="electricidad" class="col-lg-2 control-label"><g:message
            code="property.electricidad.label" default="Electricidad" /></label>
    <div class="col-lg-10">
        <g:textField name="electricidad" class="form-control"
                     value="${propertyInstance?.electricidad}" />
    </div>
</div><div class="form-group has-warnin">
    <label for="agua" class="col-lg-2 control-label"><g:message
            code="property.agua.label" default="Agua" /></label>
    <div class="col-lg-10">
        <g:textField name="agua" class="form-control"
                     value="${propertyInstance?.agua}" />
    </div>
</div>
<div class="form-group">
    <label for="saneamiento" class="col-lg-2 control-label"><g:message
            code="property.saneamiento.label" default="Saneamiento" /></label>
    <div class="col-lg-10">
        <g:textField name="saneamiento" class="form-control"
                     value="${propertyInstance?.saneamiento}" />
    </div>
</div>
<div class="form-group">
    <label for="calefaccion" class="col-lg-2 control-label"><g:message
            code="property.calefaccion.label" default="Calefaccion" /></label>
    <div class="col-lg-10">
        <g:textField name="calefaccion" class="form-control"
                     value="${propertyInstance?.calefaccion}" />
    </div>
</div>
<div class="form-group">
    <label for="agua_caliente" class="col-lg-2 control-label"><g:message
            code="property.agua_caliente.label" default="Agua Caliente" /></label>
    <div class="col-lg-10">
        <g:textField name="agua_caliente" class="form-control"
                     value="${propertyInstance?.agua_caliente}" />
    </div>
</div>
<div class="form-group">
    <label for="renovables" class="col-lg-2 control-label"><g:message
            code="property.renovables.label" default="Energías Renovables" /></label>
    <div class="col-lg-10">
        <g:textField name="renovables" class="form-control"
                     value="${propertyInstance?.renovables}" />
    </div>
</div>
<div class="form-group">
    <label for="fachada" class="col-lg-2 control-label"><g:message
            code="property.fachada.label" default="fachada" /></label>
    <div class="col-lg-10">
        <g:textField name="fachada" class="form-control"
                     value="${propertyInstance?.fachada}" />
    </div>
</div>
<div class="form-group">
    <label for="carpinteria_ext" class="col-lg-2 control-label"><g:message
            code="property.carpinteria_ext.label" default="Carpinteria Ext" /></label>
    <div class="col-lg-10">
        <g:textField name="carpinteria_ext" class="form-control"
                     value="${propertyInstance?.carpinteria_ext}" />
    </div>
</div>
<div class="form-group">
    <label for="carpinteria_int" class="col-lg-2 control-label"><g:message
            code="property.carpinteria_int.label" default="Carpinteria Int" /></label>
    <div class="col-lg-10">
        <g:textField name="carpinteria_int" class="form-control"
                     value="${propertyInstance?.carpinteria_int}" />
    </div>
</div>
<div class="form-group">
    <label for="pavimentos_exteriores" class="col-lg-2 control-label"><g:message
            code="property.pavimentos_exteriores.label" default="Pavimentos Exteriores" /></label>
    <div class="col-lg-10">
        <g:textField name="pavimentos_exteriores" class="form-control"
                     value="${propertyInstance?.pavimentos_exteriores}" />
    </div>
</div>
<div class="form-group">
    <label for="pavimentos_interiores" class="col-lg-2 control-label"><g:message
            code="property.pavimentos_interiores.label" default="Pavimentos Interiores" /></label>
    <div class="col-lg-10">
        <g:textField name="pavimentos_interiores" class="form-control"
                     value="${propertyInstance?.pavimentos_interiores}" />
    </div>
</div>
<div class="form-group">
    <label for="cocina" class="col-lg-2 control-label"><g:message
            code="property.cocina.label" default="Cocina" /></label>
    <div class="col-lg-10">
        <g:textField name="cocina" class="form-control"
                     value="${propertyInstance?.cocina}" />
    </div>
</div>
<div class="form-group">
    <label for="cuartos_bano" class="col-lg-2 control-label"><g:message
            code="property.cuartos_bano.label" default="Cuartos Baño" /></label>
    <div class="col-lg-10">
        <g:textField name="cuartos_bano" class="form-control"
                     value="${propertyInstance?.cuartos_bano}" />
    </div>
</div>
<div class="form-group">
    <label for="cerramiento" class="col-lg-2 control-label"><g:message
            code="property.cerramiento.label" default="Cerramiento" /></label>
    <div class="col-lg-10">
        <g:textField name="cerramiento" class="form-control"
                     value="${propertyInstance?.cerramiento}" />
    </div>
</div>
<div class="form-group">
    <label for="otros_calidad" class="col-lg-2 control-label"><g:message
            code="property.otros_calidad.label" default="Otros" /></label>
    <div class="col-lg-10">
        <g:textField name="otros_calidad" class="form-control"
                     value="${propertyInstance?.otros_calidad}" />
    </div>
</div>
