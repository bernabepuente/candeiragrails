<div class="form-group has-warnin">
    <label for="vistas" class="col-lg-2 control-label"><g:message
            code="property.vistas.label" default="Vistas" /></label>
    <div class="col-lg-10">
        <g:textField name="vistas" class="form-control"
                     value="${propertyInstance?.vistas}" />
    </div>
</div><div class="form-group has-warnin">
    <label for="vecindario" class="col-lg-2 control-label"><g:message
            code="property.vecindario.label" default="Vecindario" /></label>
    <div class="col-lg-10">
        <g:textField name="vecindario" class="form-control"
                     value="${propertyInstance?.vecindario}" />
    </div>
</div>
<div class="form-group">
    <label for="comunidad" class="col-lg-2 control-label"><g:message
            code="property.comunidad.label" default="Comunidad" /></label>
    <div class="col-lg-10">
        <g:textField name="comunidad" class="form-control"
                     value="${propertyInstance?.comunidad}" />
    </div>
</div>
<div class="form-group">
    <label for="ibi" class="col-lg-2 control-label"><g:message
            code="property.ibi.label" default="IBI" /></label>
    <div class="col-lg-10">
        <g:textField name="ibi" class="form-control"
                     value="${propertyInstance?.ibi}" />
    </div>
</div>
<div class="form-group">
    <label for="bus" class="col-lg-2 control-label"><g:message
            code="property.bus.label" default="Bus" /></label>
    <div class="col-lg-10">
        <g:textField name="bus" class="form-control"
                     value="${propertyInstance?.bus}" />
    </div>
</div>
<div class="form-group">
    <label for="supermercado" class="col-lg-2 control-label"><g:message
            code="property.supermercado.label" default="Supermercado" /></label>
    <div class="col-lg-10">
        <g:textField name="supermercado" class="form-control"
                     value="${propertyInstance?.supermercado}" />
    </div>
</div>
<div class="form-group">
    <label for="servicios" class="col-lg-2 control-label"><g:message
            code="property.servicios.label" default="Servicios" /></label>
    <div class="col-lg-10">
        <g:textField name="servicios" class="form-control"
                     value="${propertyInstance?.servicios}" />
    </div>
</div>
<div class="form-group">
    <label for="mobiliario" class="col-lg-2 control-label"><g:message
            code="property.mobiliario.label" default="Mobiliario" /></label>
    <div class="col-lg-10">
        <g:textField name="mobiliario" class="form-control"
                     value="${propertyInstance?.mobiliario}" />
    </div>
</div>
