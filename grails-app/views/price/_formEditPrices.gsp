<g:javascript library="mustache" />
<div class="row-fluid">
    <div class="span12">
        <div id="list-servicio" class="content scaffold-list" role="main">
            <button type="button"
                    id="btnRenderRowPrice"
                    data-button='{"idProperty": "${propertyInstance?.id}"}'
                    class="btn btn-success btnAddPrice">
                <span class="glyphicon glyphicon-plus"></span>
            </button>
            <table class="table table-stripped">
                <thead>
                <tr>
                    <g:sortableColumn property="fecha" title="${message(code: 'price.fecha.label')}" />
                    <g:sortableColumn property="amount" title="${message(code: 'price.amount.label')}" />
                    <g:sortableColumn property="comment" title="${message(code: 'price.comment.label')}" />
                    <th />
                </tr>
                </thead>
                <tbody id="prices_list">
                <g:each in="${pricesList}" status="i" var="priceInstance">
                    <tr id="priceRowSelected_${priceInstance?.id}">
                    	<td><g:textField name="txtFecha_${priceInstance?.id}" id="txtFecha_${priceInstance?.id}" class="form-control"
                     		value="${priceInstance?.fecha}" /></td>
                        <td><g:textField name="txtAmount_${priceInstance?.id}" id="txtAmount_${priceInstance?.id}" class="form-control"
                     		value="${priceInstance?.amount}" /></td>
                        <td><g:textField name="txtComment_${priceInstance?.id}" id="txtComment_${priceInstance?.id}" class="form-control"
                     		value="${priceInstance?.comment}" /></td>
                   		<td>
                        	<button type="button"
                                    data-button='{"id":  ${priceInstance?.id}}'
                                    class="btn btn-success btnUpdatePrice">
                                <span class="glyphicon glyphicon-check"></span>
                            </button>
                        </td>
                        <td>
                            <button type="button"
                                    data-button='{"id":  ${priceInstance?.id}}'
                                    class="btn btn-danger btnRemovePrice">
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </td>

                    </tr>
                </g:each>
                </tbody>
            </table>

        </div> <!-- id=list... -->
    </div> <!-- span12 -->
</div>
<jq:jquery>
    var template;
	var templateFilePath = '<g:resource dir="templates" file="prices-row.htm" />';

	$.get(templateFilePath, function(templates) {
		template = $(templates);
		//console.log("Plantillas cargadas");
	});

	function renderPriceRow(){
        var buttonData = $.parseJSON($(this).attr('data-button'));
        var t = template.filter('#price-row-available').html();
        $('#prices_list').append(Mustache.render(t, buttonData));
	}

    function addPrice() {
		var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/price/_addPrice",
			dataType: 'json',
			data: {
				idProperty: buttonData.idProperty,
				fecha: $('#txtFecha__' + buttonData.idProperty).val(),
				amount: $('#txtAmount__' + buttonData.idProperty).val(),
				comment: $('#txtComment__' + buttonData.idProperty).val()
			},
			success: function(data) {
				if (data.success == "OK") {
				    $("#priceRowAvailable_" + data.idProperty).detach();
					var t = template.filter('#price-row-selected').html();
					$('#prices_list').append(Mustache.render(t, data));
				} else {
					if (data.success == "FAIL") {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}
	
	function updatePrice() {
		var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/price/_updatePrice",
			dataType: 'json',
			data: {
				id: buttonData.id,
				fecha: $('#txtFecha_' + buttonData.id).val(),
				amount: $('#txtAmount_' + buttonData.id).val(),
				comment: $('#txtComment_' + buttonData.id).val()
			},
			success: function(data) {
				if (data.success == "OK") {
					alert('Datos guardados.');
				    
				} else {
					if (data.success == "FAIL") {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}

	function removePrice() {
	    var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/price/_removePrice",
			dataType: 'json',
			data: {
				id: buttonData.id
			},
			success: function(data) {
				if (data.success == "OK") {
					$("#priceRowSelected_" + data.id).detach();
				} else {
					if (data.errorMsg) {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}

    $(document).on("click", "#btnRenderRowPrice", renderPriceRow);
    $(document).on("click", ".btnSavePrice", addPrice);
    $(document).on("click", ".btnRemovePrice", removePrice);
    $(document).on("click", ".btnUpdatePrice", updatePrice);
    
</jq:jquery>