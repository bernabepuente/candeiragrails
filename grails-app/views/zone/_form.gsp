<g:javascript library="mustache" />
<div class="row-fluid">
    <div class="span12">
        <div id="list-servicio" class="content scaffold-list" role="main">
            <button type="button"
                    id="btnRenderRowZone"
                    data-button='{"id": "${entityInstance?.id}", "options": [${cityInstanceListMap}]}'
                    class="btn btn-success btnAddZone">
                <span class="glyphicon glyphicon-plus"></span>
            </button>
            <table class="table table-stripped">
                <thead>
                <tr>
                    <g:sortableColumn property="nombre" title="${message(code: 'zone.name.label')}" />
                    <th />
                </tr>
                </thead>
                <tbody id="zones_list">
                <g:each in="${zoneInstanceList}" status="i" var="zoneInstance">
                    <tr id="zoneRowSelected_${zoneInstance.id}">
                    	<td>
                    		<g:select name="cboCity_${zoneInstance.id}" id="cboCity_${zoneInstance.id}" from="${cityInstanceList}" value="${zoneInstance?.city?.id}" optionKey="id" optionValue="name"  noSelection="['':'--Elegir--']"/>
                    	</td>
                        <td>${fieldValue(bean: zoneInstance, field: "name")}</td>
                        <td>
                            <button type="button"
                                    data-button='{"id":  ${zoneInstance.id}}'
                                    class="btn btn-danger btnRemoveZone">
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>

        </div> <!-- id=list... -->
    </div> <!-- span12 -->
</div>
<jq:jquery>
    var template;
	var templateFilePath = '<g:resource dir="templates" file="zone-row.htm" />';

	$.get(templateFilePath, function(templates) {
		template = $(templates);
		//console.log("Plantillas cargadas");
	});

	function renderZoneRow(){
        var buttonData = $.parseJSON($(this).attr('data-button'));
        var t = template.filter('#zone-row-available').html();
        $('#zones_list').append(Mustache.render(t, buttonData));
	}

    function addZone() {
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/zone/_addZone",
			dataType: 'json',
			data: {
				name: $('#txtName').val(),
				idCity: $('#cboCity').val()
			},
			success: function(data) {
				if (data.success == "OK") {
					alert('Datos guardados.');
				    location.reload();
					if (data.success == "FAIL") {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}

	function removeZone() {
	    var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/zone/_removeZone",
			dataType: 'json',
			data: {
				id: buttonData.id
			},
			success: function(data) {
				if (data.success == "OK") {
					$("#zoneRowSelected_" + data.id).detach();
				} else {
					if (data.errorMsg) {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}

    $(document).on("click", "#btnRenderRowZone", renderZoneRow);
    $(document).on("click", ".btnSaveZone", addZone);
    $(document).on("click", ".btnRemoveZone", removeZone);
</jq:jquery>