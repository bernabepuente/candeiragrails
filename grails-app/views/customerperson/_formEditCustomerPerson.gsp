<g:javascript library="mustache" />
<div class="row-fluid">
    <div class="span12">
        <div id="list-servicio" class="content scaffold-list" role="main">
            <button type="button"
                    id="btnRenderRowPerson"
                    data-button='{"idCustomer": "${customerInstance?.id}"}'
                    class="btn btn-success btnAddPerson">
                <span class="glyphicon glyphicon-plus"></span>
            </button>
            <table class="table table-stripped">
                <thead>
                <tr>
                    <g:sortableColumn property="name" title="${message(code: 'customerperson.name.label')}" />
                    <g:sortableColumn property="surname" title="${message(code: 'customerperson.surname.label')}" />
                    <g:sortableColumn property="nif" title="${message(code: 'customerperson.nif.label')}" />
                    <g:sortableColumn property="movil" title="${message(code: 'customerperson.movil.label')}" />
                    <g:sortableColumn property="particular" title="${message(code: 'customerperson.particular.label')}" />
                    <g:sortableColumn property="trabajo" title="${message(code: 'customerperson.trabajo.label')}" />
                    <g:sortableColumn property="horario" title="${message(code: 'customerperson.horario.label')}" />
                    <g:sortableColumn property="mail" title="${message(code: 'customerperson.mail.label')}" />
                    <g:sortableColumn property="idioma" title="${message(code: 'customerperson.idioma.label')}" />
                    <g:sortableColumn property="trato" title="${message(code: 'customerperson.trato.label')}" />
                    <th />
                </tr>
                </thead>
                <tbody id="persons_list">
                <g:each in="${customerInstance?.customerPersons}" status="i" var="personInstance">
                    <tr id="personRowSelected_${personInstance.id}">
                        <td><g:textField name="txtName_${personInstance.id}" id="txtName_${personInstance.id}" class="form-control"
                     		value="${personInstance?.name}" /></td>
                        <td><g:textField name="txtSurName_${personInstance.id}" id="txtSurName_${personInstance.id}" class="form-control"
                     		value="${personInstance?.surname}" /></td>
                        <td><g:textField name="txtNif_${personInstance.id}" id="txtNif_${personInstance.id}" class="form-control"
                     		value="${personInstance?.nif}" /></td>
                        <td><g:textField name="txtMovil_${personInstance.id}" id="txtMovil_${personInstance.id}" class="form-control"
                     		value="${personInstance?.movil}" /></td>
                        <td><g:textField name="txtParticular_${personInstance.id}" id="txtParticular_${personInstance.id}" class="form-control"
                     		value="${personInstance?.particular}" /></td>
                        <td><g:textField name="txtTrabajo_${personInstance.id}" id="txtTrabajo_${personInstance.id}" class="form-control"
                     		value="${personInstance?.trabajo}" /></td>
                        <td><g:textField name="txtHorario_${personInstance.id}" id="txtHorario_${personInstance.id}" class="form-control"
                     		value="${personInstance?.horario}" /></td>
                        <td><g:textField name="txtMail_${personInstance.id}" id="txtMail_${personInstance.id}" class="form-control"
                     		value="${personInstance?.mail}" /></td>
                        <td><g:textField name="txtIdioma_${personInstance.id}" id="txtIdioma_${personInstance.id}" class="form-control"
                     		value="${personInstance?.idioma}" /></td>
                        <td><g:textField name="txtTrato_${personInstance.id}" id="txtTrato_${personInstance.id}" class="form-control"
                     		value="${personInstance?.trato}" /></td>
                        <td>
                        	<button type="button"
                                    data-button='{"id":  ${personInstance.id}}'
                                    class="btn btn-success btnUpdatePerson">
                                <span class="glyphicon glyphicon-check"></span>
                            </button>
                        </td>
                        <td>
                            <button type="button"
                                    data-button='{"id":  ${personInstance.id}}'
                                    class="btn btn-danger btnRemovePerson">
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>

        </div> <!-- id=list... -->
    </div> <!-- span12 -->
</div>
<jq:jquery>
    var template;
	var templateFilePath = '<g:resource dir="templates" file="customerPerson-row.htm" />';

	$.get(templateFilePath, function(templates) {
		template = $(templates);
		//console.log("Plantillas cargadas");
	});

	function renderPersonRow(){
        var buttonData = $.parseJSON($(this).attr('data-button'));
        var t = template.filter('#person-row-available').html();
        $('#persons_list').append(Mustache.render(t, buttonData));
	}

    function addPerson() {
		var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/customerperson/_addPerson",
			dataType: 'json',
			data: {
				idCustomer: buttonData.idCustomer,
				name: $('#txtName_' + buttonData.idCustomer).val(),
				surname: $('#txtSurName_' + buttonData.idCustomer).val(),
				nif: $('#txtNif_' + buttonData.idCustomer).val(),
				movil: $('#txtMovil_' + buttonData.idCustomer).val(),
				particular: $('#txtParticular_' + buttonData.idCustomer).val(),
				trabajo: $('#txtTrabajo_' + buttonData.idCustomer).val(),
				horario: $('#txtHorario_' + buttonData.idCustomer).val(),
				mail: $('#txtMail_' + buttonData.idCustomer).val(),
				idioma: $('#txtIdioma_' + buttonData.idCustomer).val(),
				trato: $('#txtTrato_' + buttonData.idCustomer).val()
			},
			success: function(data) {
				if (data.success == "OK") {
				    $("#personRowAvailable_" + data.idCustomer).detach();
					var t = template.filter('#person-row-selected').html();
					$('#persons_list').append(Mustache.render(t, data));
				} else {
					if (data.success == "FAIL") {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}
	
	function updatePerson() {
		var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/customerperson/_updatePerson",
			dataType: 'json',
			data: {
				id: buttonData.id,
				name: $('#txtName_' + buttonData.id).val(),
				surname: $('#txtSurName_' + buttonData.id).val(),
				nif: $('#txtNif_' + buttonData.id).val(),
				movil: $('#txtMovil_' + buttonData.id).val(),
				particular: $('#txtParticular_' + buttonData.id).val(),
				trabajo: $('#txtTrabajo_' + buttonData.id).val(),
				horario: $('#txtHorario_' + buttonData.id).val(),
				mail: $('#txtMail_' + buttonData.id).val(),
				idioma: $('#txtIdioma_' + buttonData.id).val(),
				trato: $('#txtTrato_' + buttonData.id).val()
			},
			success: function(data) {
				if (data.success == "OK") {
					alert('Datos guardados.');
				    
				} else {
					if (data.success == "FAIL") {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}

	function removePerson() {
	    var buttonData = $.parseJSON($(this).attr('data-button'));
		$.ajax({
			type: "POST",
			url: "${request.contextPath}/customerperson/_removePerson",
			dataType: 'json',
			data: {
				id: buttonData.id
			},
			success: function(data) {
				if (data.success == "OK") {
					$("#personRowSelected_" + data.id).detach();
				} else {
					if (data.errorMsg) {
						helper.showErrors(data.errorMsg, "#axMainMsg");
					}
				}
			},
			error: function(data) {
			    helper.showErrors(data.errorMsg, "#axMainMsg");
			},
			complete: function() {
			}
		});
		return false;
	}

    $(document).on("click", "#btnRenderRowPerson", renderPersonRow);
    $(document).on("click", ".btnSavePerson", addPerson);
    $(document).on("click", ".btnRemovePerson", removePerson);
    $(document).on("click", ".btnUpdatePerson", updatePerson);
</jq:jquery>